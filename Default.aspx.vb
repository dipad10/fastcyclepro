﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim message As String = Request.QueryString("m")
            Dim refid As String = Request.QueryString("ref")
            If refid <> "" Then
                Session("referral") = refid
            Else
                Session("referral") = "admin"
            End If
            If message <> "" Then
                Session.Clear()

            End If
          
            'calculate 0.01 interest
            Dim value1 As Double = txtbtcamount.Text

            Dim val1 = 3.7 / 100 * value1 / 24
            Dim val2 = 3.7 / 100 * value1
            dim val3 = 3.7 / 100 * value1 * 7
            lblhourlyInterest.Text = FormatNumber(CDbl(val1), 8)
            lbldailyInterest.Text = FormatNumber(CDbl(val2), 8)
            LblweeklyIntrest.Text = FormatNumber(CDbl(val3), 8)
            bindtotpayout()
            bindtotupgrade()
            bindtotcycle()
            bindtotdeposit()
        End If
    End Sub

    Protected Sub bindtotpayout()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotpayout"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totpayout.InnerHtml = dr("total") + 1.2
                        Catch ex As Exception

                        End Try

                    Else
                        totpayout.InnerHtml = "0.0000"


                    End If

                Else

                    totpayout.InnerHtml = "0.0000"


                End If

            End Using

        End Using
    End Sub
    Protected Sub bindtotdeposit()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotdeposit"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totdeposit.InnerHtml = dr("total") + 1.9
                        Catch ex As Exception

                        End Try

                    Else
                        totdeposit.InnerHtml = "0.0000"


                    End If

                Else

                    totdeposit.InnerHtml = "0.0000"


                End If

            End Using

        End Using
    End Sub
    Protected Sub bindtotupgrade()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotupgrade"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totupgrade.InnerHtml = dr("total") + 2419
                        Catch ex As Exception

                        End Try

                    Else
                        totupgrade.InnerHtml = 0


                    End If

                Else

                    totpayout.InnerHtml = 0


                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotcycle()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotcycled"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totcycled.InnerHtml = dr("total") + 1130
                        Catch ex As Exception

                        End Try

                    Else
                        totcycled.InnerHtml = 0


                    End If

                Else

                    totcycled.InnerHtml = 0


                End If

            End Using

        End Using
    End Sub
End Class
