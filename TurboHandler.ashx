﻿<%@ WebHandler Language="VB" Class="TurboHandler" %>

Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments

Public Class TurboHandler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "application/x-www-form-urlencoded"
        Dim cp_merchant_id As String = "126985dbd630b72ada3d6cf33e2df642"
        Dim cp_ipn_secret As String = "Prodigy1234"
        Dim cp_debug_email As String = "support@fastcyclepro.com"
        Dim order_total As String = "0.0102"
        Dim ipnmode = context.Request("ipn_mode")
        If ipnmode = "" Or ipnmode <> "hmac" Then
            context.Response.Write("Invalid")
            Exit Sub
            
        End If
        Dim merchant = context.Request("merchant")
        If merchant Is Nothing Then
            context.Response.Write("Error")
            Exit Sub
        End If
        If merchant <> cp_merchant_id Then
            context.Response.Write("Invalid")
            Exit Sub
        End If
        Dim ipntype = context.Request("ipn_type")
        Select Case ipntype
            Case "withdrawal"
                'process withdrawal return id.
                Dim withid = context.Request("id")
                Dim txnid = context.Request("txn_id")
                Dim btcaddress = context.Request("address")
                Dim amount = context.Request("amount")
                'update record with the txn id
                Dim B As New cls_turboTurboWallet
                Dim rec As fastcyclepro.TurboWallet = B.SelectThistransid(withid)
                rec.Field4 = txnid
                Dim res As ResponseInfo = B.Update(rec)
                If res.ErrorCode = 0 Then
                    Dim userec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(rec.Username)
                    Dim body As String = mod_main.PopulateBodywithdrawalprocessed(userec.Name, Date.Now.ToLongDateString, amount & " BTC" & "", "COINPAYMENTS", btcaddress, txnid)
                    Call mod_main.SendHtmlFormattedEmail(userec.Email, "", "Your Withdrawal was successful", body)
                End If
            Case Else
                'start deposit postings
                Dim itemname = context.Request("item_name")
                Dim txnid = context.Request("txn_id")
                Dim item_number = context.Request("item_number")
                Dim invoice = context.Request("invoice")
                Dim amount1 = context.Request("amount1") - invoice
                Dim amount2 = context.Request("amount2") - invoice
                Dim currency1 = context.Request("currency1")
                Dim currency2 = context.Request("currency2")
                Dim status = context.Request("status")
                Dim status_text = context.Request("status_text")
        
                Dim custom = context.Request("custom")
       
                If status >= 100 Then
                    'means payment is complete save data and send mail saying transaction successful
                    Dim A As New cls_turboTurboWallet
                    Dim rec As New fastcyclepro.TurboWallet
                    rec.TransactionID = txnid
                    rec.AccountType = "COINPAYMENTS"
                    rec.Deposit = amount1
                    rec.Withdrawal = 0
                    Dim curbal As Double = 3.7 / 100 * amount1 * 30
                    rec.CurrentBalance = curbal
                    rec.Type = "DEPOSIT"
                    rec.Username = custom
                    rec.Item_name = itemname
                    rec.Status = status
                    rec.Statustext = "COMPLETED"
                    rec.amount1 = amount1
                    rec.amount2 = amount1
                    rec.amount = amount1
                    rec.TransGUID = item_number
                    'this is to get the increament step for the counter per seconds(86400)
                    rec.Step = 3.7 / 100 * amount1 / 86400
                    rec.Lastupdate = Date.Now
                    rec.LastUpdatedBalance = 0.0
                    rec.Createdon = Date.Now
                    rec.Active = 1
                    Dim res As ResponseInfo = A.Insert(rec)
                    If res.ErrorCode = 0 Then
                        'get username and email address
                        Dim usedetail As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(rec.Username)

                        Dim body As String = mod_main.PopulateBodydepositcomplete(usedetail.Name, Date.Now.ToLongDateString, amount1 & " BTC", "Coinpayments")
                        Call mod_main.SendHtmlFormattedEmail(usedetail.Email, "", "Your FastcycleTurbo Deposit has been Completed!", body)
                        'context.Response.Write("Successful!")
                        'post withdrawal of transaction fee to admin wallet
                        Try
                            'save commission into adminprofit table
                            Dim P As New cls_adminprofit
                            Dim Prec As New fastcyclepro.AdminProfit
                            Prec.commission = invoice
                            Prec.createdon = Date.Now
                            Prec.Description = "Commission " & invoice & "BTC" & " Received from Deposit made by " & rec.Username & ""
                            Prec.Status = 1
                            'status 1 means hasnt been withdrawed, 0 means it has been withdrawed
                            Dim res2 As ResponseInfo = P.Insert(Prec)
                            If res2.ErrorCode = 0 Then
                                'send mail to admin saying commission received
                                Dim mailbody As String = mod_main.PopulateBodymastermail("Administrator", " Turbo Commission Payment Received!", Prec.Description)
                                Call mod_main.SendHtmlFormattedEmail(ConfigurationManager.AppSettings("AdminNotifications"), "", "Turbo Commission Payment Received!", mailbody)
                            End If
                            'Dim admindetail As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername("admin")
                            'POSTTURBOWITHDAWAL("create_withdrawal", invoice, "1", "BTC", admindetail.BitcoinwalletAddress, "1", "Transaction Commissions", ConfigurationManager.AppSettings("turbopublickey"))
                            ''send mail to admin saying commission received
                            'Dim description As String = "Commission " & invoice & "BTC" & " Received from Deposit made by " & rec.Username & ""
                            'Dim mailbody As String = mod_main.PopulateBodymastermail("Administrator", "Commission Payment Received!", description)
                            'Call mod_main.SendHtmlFormattedEmail(ConfigurationManager.AppSettings("AdminNotifications"), "", "Commission Payment Received!", mailbody)
                            'try get the referrer and commission to his current balance
                            
                            'calculate 3% of the deposit
                            
                            Dim com As Double = 3 / 100 * amount1
                            Dim ref As fastcyclepro.Referal = (New cls_referal).SelectThisreferee(rec.Username)
                            Dim refreg As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(ref.Referer)
                            Call mod_main.Saveturbowallet("COINPAYMENTS", 0, 0, com, 0, com, refreg.UserName, "COMMISSIONS", Guid.NewGuid.ToString(), "Commission payment received from " & rec.Username & "'s FastcycleTurbo Deposit", rec.Username)
                            'send mail to referrer
                            Dim body2 As String = mod_main.PopulateBodyreferralbonus(refreg.Name, rec.Username, com & "BTC", "Commission payment received from " & rec.Username & "'s FastcycleTurbo Deposit")
                            Call mod_main.SendHtmlFormattedEmail(refreg.Email, "", "You have just earned an Affiliate commission", body2)
                            context.Response.Write("Successfull!")
                        Catch ex As Exception
                            context.Response.Write(ex.Message)
                        End Try
              
                    Else
                        context.Response.Write(res.ErrorMessage & " " & res.ExtraMessage)
                    End If
                ElseIf status < 0 Then
                    'payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent 
                    context.Response.Write("Payment Error")
            
         
                Else
                    'payment is pending, you can optionally add a note to the order page 
                    'Dim usedetail As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(custom)

                    'Dim body As String = mod_main.PopulateBodydepositpending(usedetail.Name, Date.Now.ToLongDateString, amount1 & " BTC", "Coinpayments")
                    'Call mod_main.SendHtmlFormattedEmail(usedetail.Email, "", "Your Deposit is awaiting confirmations!", body)
                    context.Response.Write("Payment Pending")
                End If

        End Select
      
        
        
       
    End Sub
 
    
    
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class