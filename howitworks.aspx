﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="howitworks.aspx.vb" Inherits="howitworks" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | How it Works</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta name="theme-color" content="#3a526a" />
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
         <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			          How it works
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">How it Works</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<article class="page">
			
                <div class="container">
	          <h2 class="block-title text-dark margin-top-3x text-center">
	            How Fastcyclepro Works<small class="h5">In simple terms, you register, add money to your fastcyclepro account, Buy a position, relax and watch while you receive bitcoins in your wallet.</small>
	          </h2>

	          <div class="steps steps-dark">
	            <div class="inner">
	              <div class="step">
	                <h4 class="step-label">
	                  Step 1
	                </h4>

	                <div class="step-body">
	                  <div class="icon-box icon-box-bg-enabled hover-fade text-center">
	                    <span class="icon-box-backdrop" style="background-color: #000000;"></span>
                          <center>
                                <div class="icon-box-icon">
                            <i class="fa fa-user"></i>
	                    </div>
                          </center>
	                  

	                    <h3 class="icon-box-title">
	                      Signup
	                    </h3>

	                    <div class="icon-box-description">
	                      <p class="text-gray">
                              Signup for your account, entering your Personal Informations
	                      </p>
	                    </div>
	                  </div>
	                </div>
	              </div>

	              <div class="step">
	                <h4 class="step-label">
	                  Step 2
	                </h4>

	                <div class="step-body">
	                  <div class="icon-box icon-box-bg-enabled hover-fade text-center">
	                    <span class="icon-box-backdrop" style="background-color: #000000;"></span>
	                     <center>
                                <div class="icon-box-icon">
                            <i class="fa fa-money"></i>
	                    </div>
                          </center>
	                  
	                    <h3 class="icon-box-title">
	                     Add money to your FCP Account
	                    </h3>

	                    <div class="icon-box-description">
	                      <p class="text-gray">
	                    Update your wallet address and fund your account
	                      </p>
	                    </div>
	                  </div>
	                </div>
	              </div>

	              <div class="step">
	                <h4 class="step-label">
	                  Step 3
	                </h4>

	                <div class="step-body">
	                  <div class="icon-box icon-box-bg-enabled hover-fade text-center">
	                    <span class="icon-box-backdrop" style="background-color: #000000;"></span>
	                     <center>
                                <div class="icon-box-icon">
                            <i class="fa fa-child"></i>
	                    </div>
                          </center>
	                  

	                    <h3 class="icon-box-title">
	                    Buy a Position
	                    </h3>

	                    <div class="icon-box-description">
	                      <p class="text-gray">
	                     Here, you purchase a position and system assigns you a positionid
	                      </p>
	                    </div>
	                  </div>
	                </div>
	              </div>

	              <div class="step">
	                <h4 class="step-label">
	                 Step 4
	                </h4>

	                <div class="step-body">
	                  <div class="icon-box icon-box-bg-enabled hover-fade text-center">
	                    <span class="icon-box-backdrop" style="background-color: #000000;"></span>
	                     <center>
                                <div class="icon-box-icon">
                            <i class="fa fa-bitcoin"></i>
	                    </div>
                          </center>
	                  

	                    <h3 class="icon-box-title">
	                    Cycle Out and Receive Bitcoins
	                    </h3>

	                    <div class="icon-box-description">
	                      <p class="text-gray">
                              Buy a position and start Earning. Enjoy Automatic withdrawals!!!
	                      </p>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
                    <h3>How does it Work</h3>
                    <p>
                   Suppose 13 members joined and those members have joined sequentially 12345678910111213
Right now everyone is in level 0 and when the system start cyling here’s what with happen, 1 is at the top so 1 will be cycled out from level 0 then 2 then 3 and so on.

                    </p>
                  <p>
                      1 has three members placed after him which are 2 3 and 4 so the system will remove 1 from level 0 and move him to level 1 and while doing this the system will credit member 1 wallet with 0.005 bitcoins for cycling from level 0 to level 1.
                  </p>
                    <p>
                        Now in level 0 we are left with member 2,3,4 and the next person to be cycled out is member 2. The system will now wait for member 5 6 and 7 to buy position. By then 2 is at the top so system will remove number 2 from level 0 and move him to level 1.
                    </p>
                    <p> 
                        There are now two members in level 1 which is member 1 and 2. The next to be cycled out from level 0 is member 3 but for the system to be able to achieve this, member 8 9 and 10 will need to buy positions. The system will now remove member 3 from level 0 and move him to level 1. Now we have 3 members in level 1. System will now cycle out member 4 from Level 0 by member 11 12 and 13. 
                    </p>
                    <p>
                        Now Level 1 now as 4 members (1 2 3 4). In level 0 there are no members that can cycle out member 5 so the system will now wait for people to buy positions and we know that in Level 1 we have 4 members (1 2 3 4) then the system will again cycle out member 1 by 2 3 4 and member 1 will enter level 2 with 0.01 bitcoins sent to his wallet.
Now system will see there are no more members to recycle member 2 from level 1 so it will wait for members to cycle out level 0 and enter level 2. 

                    </p>

                    <p>
                      <a class="text-warning text-bold" href="/Aboutbitcoin.aspx">Want to know more about bitcoins and blockchain technology or how you can use it participate on Fastcyclepro?</a>  
                    </p>
	        </div>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-99037889-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>

</html>
