﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Error500.aspx.vb" Inherits="Accessdenied" %>

<!DOCTYPE html>

<html lang="en">
<head>
<title>An Error Occured!</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Access denied" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link href="Errordoc/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="Errordoc/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<!--//online-fonts -->
<body>
<div class="header">
	<h1>Oops Sorry!</h1>
</div>
<div class="w3-main">
	<div class="agile-info">
		<h2>An Error Occured.</h2>
		<h3></h3>
		<p>Sorry an Error occured please Go back and Try again.</p>
			
			
		<a onclick="history.go(-1)" href="#">Go back</a>
	</div>
</div>
<div class="footer-w3l">

</div>

</body>
</html>

