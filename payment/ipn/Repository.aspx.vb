﻿
Partial Class payment_ipn_Repository
    Inherits System.Web.UI.Page
    Private Shared cp_merchant_id As String = "e7204ccb734bac3fdf937124448acb6c"
    Private Shared cp_ipn_secret As String = "Prodigy1234"
    Private Shared cp_debug_email As String = "dipad10@rocketmail.com"
    Private Shared order_total As String = "0.0102"
    Private txn_id As String, item_name As String, item_number As String, amount1 As Double, amount As Double, amount2 As Double, currency1 As String, _
    currency2 As String, status As Integer, status_text As String, email As String, custom As String


   
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'validate if the malicious person doesnt know the ipnmode
        If Request.Form("ipn_mode") = "" Or Request.Form("ipn_mode") <> "hmac" Then
            Response.Write("IPN Mode is not HMAC")
            Exit Sub
        End If

        If Request.Form("merchant") = "" Or Request.Form("merchant") <> Trim(cp_merchant_id) Then

            Response.Write("No or incorrect Merchant ID passed")
            Exit Sub
        End If

        'this means either button,cart,deposit, and so on
        txn_id = Request.Form("txn_id")
        item_name = Request.Form("item_name")
        item_number = Request.Form("item_number")
        amount1 = Request.Form("amount1")
        amount2 = Request.Form("amount2")
        currency1 = Request.Form("currency1")
        currency2 = Request.Form("currency2")
        status = Request.Form("status")
        status_text = Request.Form("status_text")
        email = Request.Form("email")
        custom = Request.Form("custom")
        'check against order total

        'If amount1 < order_total Then
        '    Response.Write("Amount cannot be lesser than 0.01015 BTC")
        '    Exit Sub
        'End If
        Dim commission As Double = 0.00015

        'check status
        If status >= 100 Or status = 2 Then
            'means payment is complete save data and send mail saying transaction successful
            Dim A As New cls_wallet
            Dim rec As New Fastcyclepro.Wallet
            rec.TransactionID = txn_id
            rec.AccountType = "COINPAYMENTS"
            rec.Deposit = amount1 - commission
            rec.Withdrawal = 0
            rec.Total = amount1 - commission
            rec.Type = "DEPOSIT"
            rec.Username = custom
            rec.Item_name = item_name
            rec.Status = status
            rec.Statustext = "COMPLETED"
            rec.amount1 = amount1 - commission
            rec.amount2 = amount1 - commission
            rec.amount = amount1 - commission
            rec.TransGUID = item_number
            rec.Createdon = Date.Now
            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then
                'get username and email address
                Dim usedetail As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(rec.Username)

                Dim body As String = mod_main.PopulateBodydepositcomplete(usedetail.Name, Date.Now.ToLongDateString, rec.amount1 & " Btc", "Coinpayments")
                Call mod_main.SendHtmlFormattedEmail(usedetail.Email, "", "Your Deposit has been Completed!", body)
                Response.Write("Successful!")
                'save commission into adminprofit table
                Dim P As New cls_adminprofit
                Dim Prec As New Fastcyclepro.AdminProfit
                Prec.commission = commission
                Prec.createdon = Date.Now
                Prec.Description = "Commission " & commission & "BTC" & " Received from Deposit made by " & rec.Username & ""
                Prec.Status = 1
                'status 1 means hasnt been withdrawed, 0 means it has been withdrawed
                Dim res2 As ResponseInfo = P.Insert(Prec)
                If res2.ErrorCode = 0 Then
                    'send mail to admin saying commission received
                    Dim mailbody As String = mod_main.PopulateBodymastermail("Administrator", "Commission Payment Received!", Prec.Description)
                    Call mod_main.SendHtmlFormattedEmail(ConfigurationManager.AppSettings("AdminNotifications"), "", "Commission Payment Received!", mailbody)
                End If
            Else
                Response.Write(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        ElseIf status < 0 Then
            'payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent 
            Response.Write("Payment Error")
        Else
            'payment is pending, you can optionally add a note to the order page 
            Response.Write("Payment Pending")
        End If

        '' HMAC Signature verified at this point, load some variables. 
        'Dim ipn_type As String = Request.Form("ipn_type")
        ''check if theipn is button,withdrawal or deposit so we can do everything in same repositorypage
        'Select Case ipn_type
        '    Case "withdrawal"
        '        'perform withdrawal task here

        '    Case Else

        'End Select
       
    End Sub
End Class
