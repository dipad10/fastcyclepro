﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Plans.aspx.vb" Inherits="Plans" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | Donation Plans!</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
      <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			          Plans We Offer
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">Plans</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>

          <style>
              @media(min-width: 992px) {
                  .col5 {
                      width: 20%;
                      float: left;
                      position: relative;
                      min-height: 1px;
                      padding-right: 15px;
                      padding-left: 15px;
                  }
              }
          </style>
			<article class="page">
			
            <section class="fw-section padding-top-3x padding-bottom-4x">
			    <div class="container">
	          <h2 class="block-title text-dark text-center">
	           Our Donation Plans
	          </h2>

	          <div class="row">
                   <div data-aos="zoom-in-up" class="col5 col-sm-6 margin-bottom-2x">
                       <div class="pricing-plan pricing-plan-v3 text-center">
	                  <div class="inner">
	                    <div class="pricing-plan-image">
                            <center>
                                	                      <img style="width:50px; height:50px; text-align:center;" alt="" src="/assets/img/steps-icon-4.png">

                            </center>
	                    </div>

	                    <h3 class="pricing-plan-name">
	                      Alpha Plan
	                    </h3>
	                    <span class="pricing-plan-description">For Starters</span>
	                    <div class="pricing-plan-badge-wrap">
	                      <span class="pricing-plan-badge bg-default">Auto Matching</span>
	                    </div>

	                    <div class="pricing-plan-features">
	                      <h5 class="pricing-plan-feature-title">
	                        Features
	                      </h5>

	                      <ul class="text-gray">
	                     
                                        <li>1:1 <strong>Matrix</strong></li>
                                        <li><i class="fa fa-bitcoin"></i> <strong runat="server" id="alphabtc"></strong> Required</li>
                                        <li><strong runat="server" id="alpharate"></strong> Profit On Initial Investment</li>
                              <li><i class="fa fa-bitcoin"></i><strong runat="server" id="alphareturn"></strong> </li>
                                        <li>In<strong> 10</strong> Days</li>
                             
	                      </ul>
	                    </div>
                          
	                    <hr>

	                    <h4 class="pricing-plan-price padding-top-1x">
	                      <span runat="server" id="alphaprice"></span>
	                    </h4>
	                   
	                    <div class="padding-top-1x">
	                      <a class="btn btn-transparent btn-rounded btn-primary btn-default waves-effect waves-light" href="/Office/Signup.aspx?class=A">Subscribe Now</a>
	                    </div>
	                  </div>
	                </div>
                       </div>
	         <div data-aos="zoom-in-up" class="col5 col-sm-6 margin-bottom-2x">
                       <div class="pricing-plan pricing-plan-v3 text-center">
	                  <div class="inner">
	                   <div class="pricing-plan-image">
                            <center>
                                	                      <img style="width:50px; height:50px; text-align:center;" alt="" src="/assets/img/steps-icon-4.png">

                            </center>
	                    </div>

	                    <h3 class="pricing-plan-name">
	                      Beta Plan
	                    </h3>
	                    <span class="pricing-plan-description">For Students</span>
	                    <div class="pricing-plan-badge-wrap">
	                      <span class="pricing-plan-badge bg-default">Auto Matching</span>
	                    </div>

	                    <div class="pricing-plan-features">
	                      <h5 class="pricing-plan-feature-title">
	                        Features
	                      </h5>

	                      <ul class="text-gray">
	                       <li><strong>1:1</strong> Matrix</li>
                                   
                                       <li><i class="fa fa-bitcoin"></i> <strong runat="server" id="betabtc"></strong> Required</li>
                                        <li><strong runat="server" id="Betarate"></strong> Profit On Initial Investment</li>
                                        
                               <li><i class="fa fa-bitcoin"></i><strong runat="server" id="betareturn"></strong> </li>
                              <li>In<strong> 10</strong> Days</li>
	                      </ul>
	                    </div>

	                    <hr>

	                    <h4 class="pricing-plan-price padding-top-1x">
	                      <span runat="server" id="Betaprice"></span>
	                    </h4>
	                   
	                    <div class="padding-top-1x">
	                      <a class="btn btn-transparent btn-rounded btn-primary btn-default waves-effect waves-light" href="/Office/Signup.aspx?class=B">Subscribe Now</a>
	                    </div>
	                  </div>
	                </div>
                       </div>
                <div data-aos="zoom-in-up" class="col5 col-sm-6 margin-bottom-2x">
                       <div class="pricing-plan pricing-plan-v3 text-center">
	                  <div class="inner">
	                 <div class="pricing-plan-image">
                            <center>
                                	                      <img style="width:50px; height:50px; text-align:center;" alt="" src="/assets/img/steps-icon-4.png">

                            </center>
	                    </div>

	                    <h3 class="pricing-plan-name">
	                      Platinum Plan
	                    </h3>
	                    <span class="pricing-plan-description">For Average</span>
	                    <div class="pricing-plan-badge-wrap">
	                      <span class="pricing-plan-badge bg-default">Auto Matching</span>
	                    </div>

	                    <div class="pricing-plan-features">
	                      <h5 class="pricing-plan-feature-title">
	                        Features
	                      </h5>

	                      <ul class="text-gray">
	                       <li><strong>1:1</strong> Matrix</li>
                                       
                                        <li><i class="fa fa-bitcoin"></i> <strong runat="server" id="platinumbtc"></strong> Required</li>
                                        <li><strong runat="server" id="platinumrate"></strong> Profit On Initial Investment</li>
                               <li><i class="fa fa-bitcoin"></i><strong runat="server" id="platinumreturn"></strong> </li>
                                        <li>In<strong> 10</strong> Days</li>
	                      </ul>
	                    </div>

	                    <hr>

	                    <h4 class="pricing-plan-price padding-top-1x">
	                      <span runat="server" id="platinumprice"></span>
	                    </h4>
	                   
	                    <div class="padding-top-1x">
	                      <a class="btn btn-transparent btn-rounded btn-primary btn-default waves-effect waves-light" href="/Office/Signup.aspx?class=P">Subscribe Now</a>
	                    </div>
	                  </div>
	                </div>
                       </div>
	         <div data-aos="zoom-in-up" class="col5 col-sm-6 margin-bottom-2x">
                       <div class="pricing-plan pricing-plan-v3 text-center">
	                  <div class="inner">
	                  <div class="pricing-plan-image">
                            <center>
                                	                      <img style="width:50px; height:50px; text-align:center;" alt="" src="/assets/img/steps-icon-4.png">

                            </center>
	                    </div>

	                    <h3 class="pricing-plan-name">
	                      Silver Plan
	                    </h3>
	                    <span class="pricing-plan-description">For Workers</span>
	                    <div class="pricing-plan-badge-wrap">
	                      <span class="pricing-plan-badge bg-default">Auto Matching</span>
	                    </div>

	                    <div class="pricing-plan-features">
	                      <h5 class="pricing-plan-feature-title">
	                        Features
	                      </h5>

	                      <ul class="text-gray">
	                       <li><strong>1:1</strong> Matrix</li>
                                       
                                        <li><i class="fa fa-bitcoin"></i> <strong runat="server" id="silverbtc"></strong> Required</li>
                                        <li><strong runat="server" id="Silverrate"></strong> Profit On Initial Investment</li>
                               <li><i class="fa fa-bitcoin"></i><strong runat="server" id="silverreturn"></strong> </li>
                                        <li>In<strong> 18</strong> Days</li>
	                      </ul>
	                    </div>

	                    <hr>

	                    <h4 class="pricing-plan-price padding-top-1x">
	                      <span runat="server" id="Silverprice"></span>
	                    </h4>
	                   
	                    <div class="padding-top-1x">
	                      <a class="btn btn-transparent btn-rounded btn-primary btn-default waves-effect waves-light" href="/Office/Signup.aspx?class=S">Subscribe Now</a>
	                    </div>
	                  </div>
	                </div>
                       </div>
                  <div  data-aos="zoom-in-up" class="col5 col-sm-6 margin-bottom-2x">
                       <div class="pricing-plan pricing-plan-v3 text-center">
	                  <div class="inner">
	                 <div class="pricing-plan-image">
                            <center>
                                	                      <img style="width:50px; height:50px; text-align:center;" alt="" src="/assets/img/steps-icon-4.png">

                            </center>
	                    </div>

	                    <h3 class="pricing-plan-name">
	                      Gold Plan
	                    </h3>
	                    <span class="pricing-plan-description">ExtraOrdinary</span>
	                    <div class="pricing-plan-badge-wrap">
	                      <span class="pricing-plan-badge bg-default">Auto Matching</span>
	                    </div>

	                    <div class="pricing-plan-features">
	                      <h5 class="pricing-plan-feature-title">
	                        Features
	                      </h5>

	                      <ul class="text-gray">
	                       <li><strong>1:1</strong> Matrix</li>
                                       
                                      <li><i class="fa fa-bitcoin"></i> <strong runat="server" id="goldbtc"></strong> Required</li>
                                        <li><strong runat="server" id="goldrate"></strong> Profit On Initial Investment</li>
                              <li><i class="fa fa-bitcoin"></i><strong runat="server" id="goldreturn"></strong> </li>
                                        <li>In<strong> 25</strong> Days</li>
	                      </ul>
	                    </div>

	                    <hr>

	                    <h4 class="pricing-plan-price padding-top-1x">
	                      <span runat="server" id="goldprice"></span>
	                    </h4>
	                   
	                    <div class="padding-top-1x">
	                      <a class="btn btn-transparent btn-rounded btn-primary btn-default waves-effect waves-light" href="/Office/Signup.aspx?class=G">Subscribe Now</a>
	                    </div>
	                  </div>
	                </div>
                       </div>
	          </div>


	        
	        </div>
              
			  </section>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
</body>

</html>
