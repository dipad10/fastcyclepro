﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="getstarted.aspx.vb" Inherits="getstarted" %>
<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | Getting started</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta name="theme-color" content="#3a526a" />
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
      <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			          Getting Started
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">Getting Started</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<article class="page">

			<div class="container">
	          <h2 class="block-title text-dark margin-top-3x text-center">
	          Get Started<small class="h5">For FastcycleTurbo Participants(Cloud mining)</small>
	          </h2>
                    <center>
                        <h3 class="">STEP1: Add Your Bitcoin Wallet Address</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/cashsettings1.png" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">1. Go to My Account</span>
                      <p class="text-justify">
                          On the left hand side corner of the page, click my account dropdown and select cashout settings, when it loads, click 'Update' button at the right hand side of the grid.
                      </p>
                  </div>
	         </div>
                       
                           <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/cashsettings2.png" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">2. Enter Address and save</span>
                      <p class="text-justify">
                          Here, all you have to do is paste your wallet address inside the textbox and click save.
                      </p>
                  </div>
	         </div>
                      

                        <hr class="text-info" />
                         <h3 class="">STEP2: Deposit in Fastcycle Turbo</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/depo1.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">1. Deposit</span>
                      <p class="text-justify">
                          To deposit money to your fastcycle turbo account the first thing to do is click the 'Fastcycle Turbo' dropdown at the left hand side menu and click on 'Deposit'. Once the system display textbox for you, Input the amount in the textbox E.g 0.0102, 0.0204, 0.0306 and click 'Make a deposit'
                      </p>
                  </div>
	         </div>

                    
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/depo2.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">2. Enter amount and see Interest in hour, day, week.</span>
                      <p class="text-justify">
                          Here, you enter the amount you want to deposit, the system then calculate your interest per Hour, per day, and per week.
                      </p>
                  </div>
	         </div>

                          <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/depo3.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">3. Send bitcoins to address</span>
                      <p class="text-justify">
                          The system will generate an address for you to send your bitcoins to. You must send the exact amount so it can reflect on your account. I f you don't send it complete, the system will wait until you send the remaining before it now reflects on your account.
                      </p>
                  </div>
	         </div>

                        

                        
                        <hr class="text-info" />
                         <h3 class="">STEP3: How to withdraw in Fastcycle Turbo</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/withdraw1.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold"> Withdrawing Bitcoins</span>
                      <p class="text-justify">
                          To withdraw your bitcoins, simply click the withdraw button on the dashboard to process withdrawal. The minimum withdrawal is 0.0002 BTC.
                      </p>
                  </div>
	         </div>
                         <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/withdraw1.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold"> Withdrawing to address</span>
                      <p class="text-justify">
                          Here, you check correctly your address you have setup in the system for cashout. If its correct then go ahead and click withdraw. Then the system send you your bitcoins instantly.
                      </p>
                  </div>
	         </div>
                     
                    </center>
	         
	        </div>
                <br />
                <hr />
                <div class="container">
	          <h2 class="block-title text-dark margin-top-3x text-center">
	          Getting Started<small class="h5">For Matrix Cycler Users</small>
	          </h2>
                    <center>
                        <h3 class="">STEP1: Add Your Bitcoin Wallet Address</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/cashsettings1.png" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">1. Go to My Account</span>
                      <p class="text-justify">
                          On the left hand side corner of the page, click my account dropdown and select cashout settings, when it loads, click 'Update' button at the right hand side of the grid.
                      </p>
                  </div>
	         </div>
                       
                           <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/cashsettings2.png" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">2. Enter Address and save</span>
                      <p class="text-justify">
                          Here, all you have to do is paste your wallet address inside the textbox and click save.
                      </p>
                  </div>
	         </div>
                      

                        <hr class="text-info" />
                         <h3 class="">STEP2: Fund Your Account</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/fund1.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">1. Add Money</span>
                      <p class="text-justify">
                          To add money to your account the first thing to do is click the 'wallet' dropdown at the left hand side menu and click on 'Add money'. Once the system display textbox for you, Input the amount in the textbox E.g 0.0102, 0.0204, 0.0306 and click 'Fund account'
                      </p>
                  </div>
	         </div>

                    
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/fund2.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">2. Confirm and pay</span>
                      <p class="text-justify">
                          Here, verify the details of the payment before submitting. Once its correct then go ahead and click 'Make payment' which redirec you to the coinpayments page.
                      </p>
                  </div>
	         </div>

                          <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/fund3.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">3. Complete Your Checkout</span>
                      <p class="text-justify">
                          Once you are on the Coinpayments page, Enter your first and last name, your email flows there automatically and you can change it if you want to. Select your coin 'Bitcoin' and then finally click the 'Complete your checkout' button.
                      </p>
                  </div>
	         </div>

                           <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/fund4.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold">4. Sending Bitcoins</span>
                      <p class="text-justify">
                          After clicking the complete your checkout, you will be redirected to a page where you see the address you are to send your bitcoins to. Send the payment to the bitcoin address provided. Wait for the coinpayments for 2 confirmations on the bitcoin network. As soon as it confirms, your btc will be sent to your Fastcyclepro account.
                      </p>
                  </div>
	         </div>

                        
                        <hr class="text-info" />
                         <h3 class="">STEP3: Buy a Position</h3>
                        <div class="row">
                 <div class="col-md-6">
                     <img src="/assets/steps/purchse1.PNG" width="500" height="350" class="img-rounded img-responsive" />
                 </div>
                  <div class="col-md-6">
                      <span class="text-primary text-bold"> Buying a Position</span>
                      <p class="text-justify">
                          To buy a position, click on 'purchase' on the left hand side menu, once the page is loaded then enter '1' in the amount textbox and click buy.
                      </p>
                  </div>
	         </div>
                        <br />
                        <i class="fa fa-arrow-down fa-2x"></i>
                        <br />
                        <a href="/office/signup.aspx" class="btn btn-warning">Open Account</a>
                    </center>
	         
	        </div>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
</body>

</html>

