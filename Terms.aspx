﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Terms.aspx.vb" Inherits="Terms" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | Terms and Conditions!</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta name="theme-color" content="#3a526a" />
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
        <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			          Terms and Conditions
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">Terms & Conditions</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<article class="page">
			
                <section class="fw-section padding-top-3x">
			    
                    <div class="container">

					<div class="row">
    <h2>Terms of Use</h2>

    <p align="justify">

    This agreement defines the basic rules and terms of use (hereinafter, Terms of Use) for access to services and/or features on websites owned and managed by Fastcyclepro Solutions Limited (hereinafter, also Fastcyclepro), including https://Fastcyclepro.com/. The Company reserves the right to add new or amend existing provisions of the agreement, as the Company’s financial offers may be expanded. This agreement also applies to other websites owned by Fastcyclepro Solutions Limited.<br><br> 

    Also, we do not rule out the possibility of contacting you on third-party websites (social networks, investment forums, etc.) where we publish our content (e.g. FACEBOOK.COM, INSTAGRAM.COM, TWITTER.COM, VK.COM, MMGP.RU, MMG, DREAMTEAM, TALKGOLD). Beware: Fastcyclepro Solutions Limited has no control over third-party websites, and these Terms of Use do not cover websites which do not belong to us. Please read the terms of use published on third-party websites.<br><br>

    By using the features of https://Fastcyclepro.com/, you agree with these Terms of Use, the Confidentiality Policy, and the principles and rules of use, even if you have not read them. If you disagree with any of these agreements, you must not use our websites.<br><br>

    Fastcyclepro Solutions Limited reserves the right to amend and/or modify any of the terms and conditions of the Terms of Use. Fastcyclepro Solutions Limited reserves the right to amend this agreement from time to time, without prior notice and at its own discretion. Your subsequent use of the website signifies your acceptance of such amendments and/or modifications. If you disagree with such amendments and/or modifications, you must immediately stop using our web resources. It is recommended that you read the Terms of Use and Confidentiality Policy as frequently as possible.
    </p>

    <h2>Copyright</h2>

    <p align="justify">

    The design, text, graphics, logos, button icons, images, audio and video content, their selection and arrangement, and all software on the websites are protected by copyright (C) 1995-2016 Fastcyclepro Solutions Limited.<br><br> 
    The compilation (collection, arrangement and editing) of all content on the website is the exclusive property of Fastcyclepro (c) and is protected by international copyright law.

    </p>

    <h2>Content disclaimer</h2>

    <p align="justify">

    You may access any content on the website only if permitted by these Terms of Use and the Confidentiality Policy. By agreeing to the Terms of Use, you undertake the obligation not to take part in using, copying or distributing any content on the website, save for cases expressly stipulated in this document.
    </p>

    <h2>Survival of rights</h2>

    <p align="justify">
Fastcyclepro Solutions Limited reserves the right to terminate without prior notification and at its own discretion the rights to use interactive services on the website, or restrict or prevent your future access to the web resource.
Fastcyclepro Solutions Limited may disclose, save or transmit account and/or personal data if required by the law or if this action is required to:</p>
<ul>
<li>• comply with civil proceedings; </li>
<li>• comply with these Terms of Use; </li>
<li>• protect the legal title or the personal or technical security of Fastcyclepro (and its staff), its users, and general public.</li>
</ul>

<h2>Fraud protection</h2>

    <p align="justify">

    When accepting payments, our specialists monitor all transactions for possible fraud and any other unauthorized or illegal activities. We reserve the right to reject a payment in case of suspected fraud, unauthorized or illegal activities. These measures are aimed at protecting our clients and our company from fraud and any other illegal activity.<br>
	Each investor can register only one account, where he/she can create an unlimited number of deposits. Multi-accounting is not allowed. Multi-accounting it is registration in system with the aim of fraud on an affiliate program.<br>
	Accounts of users who have the same IP, payment details, secret question/answer can be suspended until circumstances are clarified. In case of suspected fraud or multi-accounting, Fastcyclepro  HealthCare Limited Company reserves the right to suspend or block account access for violating investors.
	<br><br>
Fastcyclepro Solutions Limited shall not be liable for any losses caused by sharing passwords or by personal data theft. We strongly recommend that you keep as secure as possible your data that is required for authentication and access to your personal profile and settings. Do not disclose your personal data to third parties.

    </p>

<h2>Refund policy</h2>

    <p align="justify">

    Despite the fact that all investments are  final, the company's return policy allows each user to apply for a full refund of the invested money less profits. All requests to return the funds are processed and scrutinized in strict terms of up to 30 calendar days. <br><br>
Fastcyclepro Solutions Limited reserves the right to reject any refund request not supported with evidence, based on a decision of the technical department, and in accordance with this agreement.<br><br>
Any unresolved complaints or disputes between you and Fastcyclepro shall be filed to and resolved by a competent court. If any provision of these Terms of Use is ruled invalid by a competent court, the invalidity of such a provision shall not affect the validity of the remaining provisions of these Terms of Use, which shall remain in full force and effect.
    </p>

<h2>General provisions</h2>

    <p align="justify">

    By agreeing with the Terms of Use, you automatically confirm that you are of legal age in the country of your citizenship and your use of this website does not violate the laws of your state.<br><br>
All materials and information provided by Fastcyclepro is intended only for personal, educational and informational purposes.<br><br>
You undertake to pay profit tax according to the current legislation of the United Kingdom. Profits earned in a given state are subject to taxation regardless of the official residence, actual residence, or the country in which the company is registered.<br><br>
Any financial transactions executed under this agreement shall be deemed final and irreversible. 
In order to prevent fraud and/or cheating, it is strictly prohibited to create multiple accounts and/or use shared payment systems for different users. If any such abuse is discovered, Fastcyclepro Solutions Limited reserves the right to cease all partner accruals and suspend such accounts.<br><br>

Any violation of this agreement or any of its clauses may entail a permanent account suspension.
    </p>


</div>
				</div>
			  </section>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
</body>

</html>
