﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Faq.aspx.vb" Inherits="Faq" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | Frequently Asked Questions!</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
     <meta name="theme-color" content="#3a526a" />
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
      <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			          Frequently Asked Questions
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">Payment proof</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<article class="page">
			
                <section class="fw-section padding-top-3x">
			    <div class="container">
	          <h2 class="block-title text-dark text-center">
	           Frequently asked questions from Visitors. Hope you find the answer to your Question here.
	          </h2>

	          <div class="row">
	          <div class="col-md-12">
                  <div class="panel-group panel-group-dark" id="tta-aa3efbcb">
			            

			            <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-1d435109" aria-expanded="false"><i class="material-icons help_outline"></i> What is Fastcyclepro?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-1d435109" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
                                   fastcyclepro.com is a Platform which is designed for the masses to get financial freedom through a fast single line cycler.
			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>
			            <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-2" aria-expanded="false"><i class="material-icons help_outline"></i> THE AIM OF Fastcyclepro?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-2" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                   									<p class="" style="height: 0px;">Fastcyclepro was created to promote the greatest good, with a particular emphasis on helping man. Its connect people from all over the world to join and earn everyday!</p>

			                  </div>
			                </div>
			              </div>
			            </div>
                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-3" aria-expanded="false"><i class="material-icons help_outline"></i> HOW MANY DIRECT REFERRALS DO I NEED TO HAVE?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-3" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                  									<p class="" style="height: 0px;">There aren’t any obligations to force members to join your downline. No Spillover function. The system is designed for automation.</p>

			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-4" aria-expanded="false"><i class="material-icons help_outline"></i> How do I make money with fastcyclepro.com?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-4" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                   									<p class="" style="height: 0px;">Purchase advertising on Fastcyclepro and earn from new customers when you get them or purchase positions in our unique Matrix/Cycler plans. You will earn everytime you cycled out from our plans. The earnings will be automatically withdrawn to your bitcoin wallet.</p>

			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-5" aria-expanded="false"><i class="material-icons help_outline"></i> WWHO IS ELIGIBLE TO JOIN Fastcyclepro?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-5" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                  									<p>There is no age and gender barrier in joining TwinKaS. Equal benefits and donations are assigned to all and sundry</p>

			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-6" aria-expanded="false"><i class="material-icons help_outline"></i> CAN I HAVE MULTIPLE ACCOUNT?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-6" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                   									<p>Yes. But the system will require additional username and email for verification and validation.</p>

			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-7" aria-expanded="false"><i class="material-icons help_outline"></i>Can I purchase multiple positions in the fastcyclepro.com?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-7" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                 <p>Yes, you can purchase multiple positions in the fastcyclepro.com , but only in the Bronze Line. The maximum you can buy has no limit but only 4 positions in 60 minutes timeframe.</p>
			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-8" aria-expanded="false"><i class="material-icons help_outline"></i> What is the minimum Payout</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-8" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
The minimum you earn from Bronze line is 0.01 BTC and goes further up to 1.20 BTC and all withdrawals will be paid to your BTC wallet automatically. You dont need to request for payout.			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>

                      <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-9" aria-expanded="false"><i class="material-icons help_outline"></i> Are withdrawals handled instantly?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-9" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
                                    Yes earnings accumulated when you cycles out every line goes to your btc wallet directly.
			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>
                         <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-9" aria-expanded="false"><i class="material-icons help_outline"></i> Do I have to refer members to earn?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-9" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
                                   No! It's not obligatory, it’s optional, but more active members will speed up the cycling and earn more commissions for you.
			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>

                         <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-9" aria-expanded="false"><i class="material-icons help_outline"></i> HOW WOULD I GET NOTIFIED?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-9" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
                                    Fastcyclepro would send you mails for each transaction you do on the platform
			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>
                          <div class="panel">
			              <h4 class="panel-title">
			                <a class="collapsed" data-parent="#tta-aa3efbcb" data-toggle="collapse" href="#panel-9" aria-expanded="false"><i class="material-icons help_outline"></i> WHAT DO I NEED TO START USING Fastcyclepro?</a>
			              </h4>

			              <div class="panel-collapse out collapse" id="panel-9" aria-expanded="false" style="height: 0px;">
			                <div class="panel-collapse-inner">
			                  <div class="text-block">
			                    <p>
                                   On Fastcyclepro you need a cooinpayment wallet to receive your bitcoins, your Personal Details. we only support coinpayments Wallets to avoid delayed Confirmations. To learn more about how blockchain, bitcoins <a href="Aboutbitcoin.aspx">click Here</a>
			                    </p>
			                  </div>
			                </div>
			              </div>
			            </div>

                      



			          </div>
	          </div>
	          </div>

	          
			    </div>
			  </section>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
</body>

</html>
