﻿Imports Microsoft.VisualBasic

Public Class cls_announcements
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Fastcyclepro.Announcement) As ResponseInfo
        Try
            DB.Announcements.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal G As Fastcyclepro.Announcement) As ResponseInfo
        Try
            DB.Announcements.DeleteOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As Fastcyclepro.Announcement) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal sn As String) As Fastcyclepro.Announcement
        Try
            Return (From R In DB.Announcements Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Announcement

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of Fastcyclepro.Announcement)
        Try
            Return (From U In DB.Announcements).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Announcement)
        End Try
    End Function
End Class
