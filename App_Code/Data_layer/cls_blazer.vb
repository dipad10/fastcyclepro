﻿Imports Microsoft.VisualBasic

Public Class cls_blazer
    Dim DB As fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As fastcyclepro.Blazer) As ResponseInfo
        Try
            DB.Blazers.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As fastcyclepro.Blazer) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThislevelid(ByVal levelid As String) As fastcyclepro.Blazer
        Try
            Return (From R In DB.Blazers Where R.LevelID = levelid And R.Status = "ACTIVE" Order By R.SN Ascending).ToList(0)
        Catch ex As Exception
            Return New Fastcyclepro.blazer

        End Try
    End Function
    Public Function SelectThisusername(ByVal username As String) As fastcyclepro.Blazer
        Try
            Return (From R In DB.Blazers Where R.Username = username And R.Status = "ACTIVE" Order By R.SN Descending).ToList(0)
        Catch ex As Exception
            Return New fastcyclepro.blazer

        End Try
    End Function
    Public Function SelectThispositionid(ByVal positionid As String) As fastcyclepro.Blazer
        Try
            Return (From R In DB.Blazers Where R.PositionID = positionid).ToList()(0)
        Catch ex As Exception
            Return New fastcyclepro.Blazer

        End Try
    End Function

    Public Function SelectThisblazerany(ByVal username As String) As Boolean
        Try
            Return (From R In DB.Blazers Where R.Username = username And R.Status = "ACTIVE").Take(1).Any
            Return True
        Catch ex As Exception
            Return False

        End Try
    End Function
    Public Function SelectAllblazer() As List(Of fastcyclepro.Blazer)
        Try
            Return (From R In DB.Blazers).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.blazer)
        End Try
    End Function

    Public Function Selectlevelposition(ByVal levelid As Integer) As List(Of fastcyclepro.Blazer)
        Try
            Return (From R In DB.Blazers Where R.LevelID = levelid And R.Status = "ACTIVE" Order By R.SN Ascending).Take(50).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Blazer)
        End Try
    End Function

    Public Function Selectuserposition(ByVal username As String, ByVal levelid As String) As List(Of fastcyclepro.Blazer)
        Try
            Return (From R In DB.Blazers Where R.Username = username And R.LevelID = levelid).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Blazer)
        End Try
    End Function

    Public Function Selectuserposition2(ByVal username As String) As List(Of fastcyclepro.Blazer)
        Try
            Return (From R In DB.Blazers Where R.Username = username Order By R.SN Descending).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Blazer)
        End Try
    End Function
End Class
