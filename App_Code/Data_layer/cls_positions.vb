﻿Imports Microsoft.VisualBasic

Public Class cls_position
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.position) As ResponseInfo
        Try
            DB.positions.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.position) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThislevelid(ByVal levelid As String) As Fastcyclepro.Position
        Try
            Return (From R In DB.Positions Where R.LevelID = levelid And R.Status = "ACTIVE" Order By R.SN Ascending).ToList(0)
        Catch ex As Exception
            Return New Fastcyclepro.Position

        End Try
    End Function
    Public Function SelectThisusername(ByVal username As String) As fastcyclepro.Position
        Try
            Return (From R In DB.Positions Where R.Username = username And R.Status = "ACTIVE" Order By R.SN Descending).ToList(0)
        Catch ex As Exception
            Return New fastcyclepro.Position

        End Try
    End Function

    Public Function SelectThispositionrany(ByVal username As String) As Boolean
        Try
            Return (From R In DB.Positions Where R.Username = username And R.Status = "ACTIVE").Take(1).Any
            Return True
        Catch ex As Exception
            Return False

        End Try
    End Function
    Public Function SelectThispositionid(ByVal positionid As String) As Fastcyclepro.Position
        Try
            Return (From R In DB.Positions Where R.PositionID = positionid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Position

        End Try
    End Function
   

    Public Function SelectAllpositions() As List(Of Fastcyclepro.position)
        Try
            Return (From R In DB.positions).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.position)
        End Try
    End Function

    Public Function Selectlevelpositions(ByVal levelid As Integer) As List(Of Fastcyclepro.Position)
        Try
            Return (From R In DB.Positions Where R.LevelID = levelid And R.Status = "ACTIVE" Order By R.SN Ascending).Take(50).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Position)
        End Try
    End Function

    Public Function Selectuserpositions(ByVal username As String, ByVal levelid As Integer) As List(Of Fastcyclepro.Position)
        Try
            Return (From R In DB.Positions Where R.Username = username And R.LevelID = levelid).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Position)
        End Try
    End Function

    Public Function Selectuserpositions2(ByVal username As String) As List(Of Fastcyclepro.Position)
        Try
            Return (From R In DB.Positions Where R.Username = username Order By R.SN Descending).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Position)
        End Try
    End Function
End Class
