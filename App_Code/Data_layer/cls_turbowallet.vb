﻿Imports Microsoft.VisualBasic

Public Class cls_turboTurboWallet
    Dim DB As fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As fastcyclepro.TurboWallet) As ResponseInfo
        Try
            DB.TurboWallets.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As fastcyclepro.TurboWallet) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThistransid(ByVal transactonid As String) As fastcyclepro.TurboWallet
        Try
            Return (From R In DB.TurboWallets Where R.TransactionID = transactonid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.TurboWallet

        End Try
    End Function
    Public Function SelectThisguid(ByVal itemnumber As String) As fastcyclepro.TurboWallet
        Try
            Return (From R In DB.TurboWallets Where R.TransGUID = itemnumber).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.TurboWallet

        End Try
    End Function

    Public Function SelectAllTurboWallets(ByVal username As String) As List(Of fastcyclepro.TurboWallet)
        Try
            Return (From R In DB.TurboWallets Where R.Username = username Order By R.Createdon Descending).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.TurboWallet)
        End Try
    End Function
    Public Function Selectuser(ByVal username As String) As List(Of fastcyclepro.TurboWallet)
        Try
            Return (From R In DB.TurboWallets Where R.Username = username And R.Active = 1).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.TurboWallet)
        End Try
    End Function

    Public Function Selectuserdeposit(ByVal username As String) As List(Of fastcyclepro.TurboWallet)
        Try
            Return (From R In DB.TurboWallets Where R.Username = username And R.Type = "DEPOSIT").ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.TurboWallet)
        End Try
    End Function
    Public Function Selectuserlastupdated(ByVal username As String) As fastcyclepro.TurboWallet
        Try
            Return (From R In DB.TurboWallets Where R.Username = username And R.Type = "DEPOSIT" And R.Active = 1 Order By R.SN Ascending).ToList(0)
        Catch ex As Exception
            Return New fastcyclepro.TurboWallet
        End Try
    End Function
End Class
