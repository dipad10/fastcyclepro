﻿Imports Microsoft.VisualBasic

Public Class cls_country
    Dim DB As fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As fastcyclepro.Country) As ResponseInfo
        Try
            DB.Countries.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As fastcyclepro.Country) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal id As String) As fastcyclepro.Country
        Try
            Return (From R In DB.Countries Where R.id = id).ToList()(0)
        Catch ex As Exception
            Return New fastcyclepro.Country

        End Try
    End Function
    Public Function SelectThisiso(ByVal iso As String) As fastcyclepro.Country
        Try
            Return (From R In DB.Countries Where R.iso = iso).ToList()(0)
        Catch ex As Exception
            Return New fastcyclepro.Country

        End Try
    End Function

    Public Function SelectAllcountries() As List(Of fastcyclepro.Country)
        Try
            Return (From R In DB.Countries).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Country)
        End Try
    End Function
End Class
