﻿Imports Microsoft.VisualBasic

Public Class cls_referal
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Referal) As ResponseInfo
        Try
            DB.Referals.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Referal) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal refid As String) As Fastcyclepro.Referal
        Try
            Return (From R In DB.Referals Where R.RefID = refid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Referal

        End Try
    End Function

    Public Function SelectThisreferee(ByVal referee As String) As Fastcyclepro.Referal
        Try
            Return (From R In DB.Referals Where R.Referee = referee And R.Status = "ACTIVE").ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Referal

        End Try
    End Function
    Public Function SelectThisreferer(ByVal referer As String) As List(Of fastcyclepro.Referal)
        Try
            Return (From R In DB.Referals Where R.Referer = referer And R.Status = "ACTIVE").ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Referal)

        End Try
    End Function

    Public Function SelectThisrefereelist(ByVal referee As String) As List(Of Fastcyclepro.Referal)
        Try
            Return (From R In DB.Referals Where R.Referee = referee And R.Status = "ACTIVE").ToList
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Referal)

        End Try
    End Function



    Public Function SelectAllReferals() As List(Of Fastcyclepro.Referal)
        Try
            Return (From U In DB.Referals).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Referal)
        End Try
    End Function
End Class
