﻿Imports Microsoft.VisualBasic

Public Class cls_matches
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Match) As ResponseInfo
        Try
            DB.matches.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Match) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal matchid As String) As Fastcyclepro.Match
        Try
            Return (From R In DB.matches Where R.matchID = matchid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.match

        End Try
    End Function

    Public Function SelectThisbydonationid(ByVal donationid As String) As Fastcyclepro.Match
        Try
            Return (From R In DB.Matches Where R.Donationid = donationid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Match

        End Try
    End Function

    Public Function SelectThisbydonationidlist(ByVal donationid As String) As List(Of Fastcyclepro.Match)
        Try
            Return (From R In DB.Matches Where R.Donationid = donationid).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Match)

        End Try
    End Function

    Public Function SelectThisbymatchidlist(ByVal matchid As String) As List(Of Fastcyclepro.Match)
        Try
            Return (From R In DB.Matches Where R.MatchID = matchid).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Match)

        End Try
    End Function

    Public Function SelectThisdonationlisttake2(ByVal donationid As String) As Boolean
        Try
            Return (From R In DB.Matches Where R.Donationid = donationid And R.Status = "CONFIRMED").Take(2).Any
            Return True
        Catch ex As Exception
            Return False

        End Try
    End Function

    Public Function SelectThisguid(ByVal guid As String) As Fastcyclepro.Match
        Try
            Return (From R In DB.matches Where R.TransGUID = guid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.match

        End Try
    End Function

    Public Function Selectsponsorusername(ByVal sponsorusername As String) As List(Of Fastcyclepro.Match)
        Try
            Return (From U In DB.Matches Where U.SponsorUsername = sponsorusername).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Match)
        End Try
    End Function



    Public Function SelectAllmatches() As List(Of Fastcyclepro.Match)
        Try
            Return (From U In DB.matches).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.match)
        End Try
    End Function
End Class
