﻿Imports Microsoft.VisualBasic
Imports System

Public Class Cls_Autosettings
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Autosetting) As ResponseInfo
        Try
            DB.Autosettings.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Autosetting) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal SN As String) As Fastcyclepro.Autosetting
        Try
            Return (From R In DB.Autosettings Where R.SN = SN).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Autosetting

        End Try
    End Function



    Public Function Getautonumbervalue(ByVal numbertype As String) As Fastcyclepro.Autosetting
        Try
            Return (From R In DB.Autosettings Where R.NumberType = numbertype).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Autosetting

        End Try
    End Function


    Public Function SelectAllsettings() As List(Of Fastcyclepro.Autosetting)
        Try
            Return (From U In DB.Autosettings).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Autosetting)
        End Try
    End Function
End Class
