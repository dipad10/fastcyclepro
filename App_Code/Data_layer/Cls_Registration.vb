﻿Imports Microsoft.VisualBasic
Imports System

Public Class Cls_Registration
    Dim DB As Fastcyclepro.FastcycleproDataContext

    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Registration) As ResponseInfo
        Try
            DB.Registrations.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As Fastcyclepro.Registration) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal R As Fastcyclepro.Registration) As ResponseInfo
        Try
            DB.Registrations.DeleteOnSubmit(R)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisUserID(ByVal ID As String) As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.UserID = ID).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function

    Public Function SelectThisUsername(ByVal username As String) As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.UserName = username).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function

    Public Function SelectThisemail(ByVal email As String) As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.Email = email).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function
    Public Function Authenticatelogin(ByVal username As String, ByVal password As String) As List(Of Fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.UserName = username And R.Password = password).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Registration)

        End Try
    End Function
    Public Function SelectAllUsers() As List(Of Fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.Active = 1 And R.Field9 = 1).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Registration)
        End Try
    End Function
    Public Function SelectAllUserscountry(ByVal country As String) As List(Of fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.Country = country).ToList()
        Catch ex As Exception
            Return New List(Of fastcyclepro.Registration)
        End Try
    End Function

    Public Function SelectThisemailist(ByVal email As String) As List(Of Fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.Email = email).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Registration)

        End Try
    End Function

    Public Function Selectalladmin() As List(Of Fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.Permission = "ADMIN").ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Registration)

        End Try
    End Function

    Public Function Selectallbasicusers() As List(Of Fastcyclepro.Registration)
        Try
            Return (From R In DB.Registrations Where R.Permission = "USER").ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Registration)

        End Try
    End Function

    Public Function Selecttop1admin() As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.Permission = "ADMIN").Take(1).ToList(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function
    Public Function SelectThisguid(ByVal guid As String) As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.Activationcode = guid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function

    Public Function Selectresetpwd(ByVal email As String, ByVal code As String) As Fastcyclepro.Registration
        Try
            Return (From R In DB.Registrations Where R.Email = email And R.Field10 = code).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Registration

        End Try
    End Function
End Class
