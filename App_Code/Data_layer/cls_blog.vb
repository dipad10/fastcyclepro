﻿Imports Microsoft.VisualBasic

Public Class cls_blog
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Fastcyclepro.Blog) As ResponseInfo
        Try
            DB.blogs.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal G As Fastcyclepro.Blog) As ResponseInfo
        Try
            DB.blogs.DeleteOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As Fastcyclepro.Blog) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal sn As String) As Fastcyclepro.Blog
        Try
            Return (From R In DB.blogs Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.blog

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of Fastcyclepro.Blog)
        Try
            Return (From U In DB.blogs).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.blog)
        End Try
    End Function

    Public Function Selecttop6() As List(Of Fastcyclepro.Blog)
        Try
            Return (From U In DB.Blogs Order By U.Submittedon Descending).Take(6).ToList
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Blog)
        End Try
    End Function
End Class
