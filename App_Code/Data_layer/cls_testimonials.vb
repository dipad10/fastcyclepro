﻿Imports Microsoft.VisualBasic

Public Class cls_testimonials
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Fastcyclepro.Testimonial) As ResponseInfo
        Try
            DB.testimonials.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Delete(ByVal G As Fastcyclepro.Testimonial) As ResponseInfo
        Try
            DB.testimonials.DeleteOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As Fastcyclepro.Testimonial) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal testimonialid As String) As Fastcyclepro.Testimonial
        Try
            Return (From R In DB.Testimonials Where R.Testimonyid = testimonialid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Testimonial

        End Try
    End Function

    Public Function SelectThisSN(ByVal sn As String) As Fastcyclepro.Testimonial
        Try
            Return (From R In DB.Testimonials Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Testimonial

        End Try
    End Function



    Public Function SelectAlltestimonies() As List(Of Fastcyclepro.Testimonial)
        Try
            Return (From U In DB.Testimonials Where U.field1 = 1 Order By U.submittedon Descending).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Testimonial)
        End Try
    End Function
End Class
