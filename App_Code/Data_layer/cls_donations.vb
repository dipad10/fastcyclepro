﻿Imports Microsoft.VisualBasic

Public Class cls_donations
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Donation) As ResponseInfo
        Try
            DB.Donations.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Donation) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal donationid As String) As Fastcyclepro.Donation
        Try
            Return (From R In DB.Donations Where R.DonationID = donationid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Donation

        End Try
    End Function

    Public Function SelectThisguid(ByVal guid As String) As Fastcyclepro.Donation
        Try
            Return (From R In DB.Donations Where R.TransGUID = guid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Donation

        End Try
    End Function

    Public Function SelectThisguidlist(ByVal guid As String) As List(Of Fastcyclepro.Donation)
        Try
            Return (From R In DB.Donations Where R.TransGUID = guid).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Donation)

        End Try
    End Function

    Public Function Checkdonation(ByVal username As String) As List(Of Fastcyclepro.Donation)
        Try
            Return (From R In DB.Donations Where R.Username = username And R.Active = 1).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Donation)

        End Try
    End Function
    Public Function Checkdonationstatus(ByVal username As String) As List(Of Fastcyclepro.Donation)
        Try
            Return (From R In DB.Donations Where R.Status = "NOT ASSIGNED" And R.Username = username And R.Active = 1).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Donation)

        End Try
    End Function


    Public Function SelectThisplanname(ByVal planname As String, ByVal username As String) As List(Of Fastcyclepro.Donation)
        Try
            Return (From R In DB.Donations Where R.Planname = planname And R.Username = username And R.Active = 1).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Donation)

        End Try
    End Function


    Public Function SelectAlldonations() As List(Of Fastcyclepro.Donation)
        Try
            Return (From U In DB.Donations).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Donation)
        End Try
    End Function
End Class
