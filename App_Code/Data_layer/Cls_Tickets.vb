﻿Imports Microsoft.VisualBasic
Imports System

Public Class Cls_Tickets
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Ticket) As ResponseInfo
        Try
            DB.Tickets.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Ticket) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal sn As String) As Fastcyclepro.Ticket
        Try
            Return (From R In DB.Tickets Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Ticket

        End Try
    End Function

    Public Function SelectAllTickets() As List(Of Fastcyclepro.Ticket)
        Try
            Return (From R In DB.Tickets).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Ticket)
        End Try
    End Function
End Class
