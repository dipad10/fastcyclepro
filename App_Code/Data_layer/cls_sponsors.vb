﻿Imports Microsoft.VisualBasic

Public Class cls_sponsors
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Sponsor) As ResponseInfo
        Try
            DB.Sponsors.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Sponsor) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal sponsorid As String) As Fastcyclepro.Sponsor
        Try
            Return (From R In DB.Sponsors Where R.Sponsorid = sponsorid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.sponsor

        End Try
    End Function

    Public Function SelectThisdonationid(ByVal donationid As String) As Fastcyclepro.Sponsor
        Try
            Return (From R In DB.Sponsors Where R.DonationID = donationid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Sponsor

        End Try
    End Function

    Public Function SelectThisguid(ByVal guid As String) As Fastcyclepro.Sponsor
        Try
            Return (From R In DB.sponsors Where R.TransGUID = guid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.sponsor

        End Try
    End Function



    Public Function get2sponsors(ByVal amount As Double, ByVal date1 As Date) As List(Of Fastcyclepro.Sponsor)
        Try
            Return (From U In DB.Sponsors Where U.Profitinreturn = amount And date1 >= U.Matchdate And U.Status = "NOT ASSIGNED" And U.Active = 1 Order By U.Submittedon Ascending).Take(2).ToList
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Sponsor)
        End Try
    End Function

    Public Function SelectAllsponsors() As List(Of Fastcyclepro.Sponsor)
        Try
            Return (From U In DB.sponsors).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.sponsor)
        End Try
    End Function
End Class
