﻿Imports Microsoft.VisualBasic

Public Class cls_wallet
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Wallet) As ResponseInfo
        Try
            DB.Wallets.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Wallet) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThistransid(ByVal transactonid As String) As Fastcyclepro.Wallet
        Try
            Return (From R In DB.Wallets Where R.TransactionID = transactonid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Wallet

        End Try
    End Function
    Public Function SelectThisguid(ByVal itemnumber As String) As Fastcyclepro.Wallet
        Try
            Return (From R In DB.Wallets Where R.TransGUID = itemnumber).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Wallet

        End Try
    End Function

    Public Function SelectAllWallets(ByVal username As String) As List(Of Fastcyclepro.Wallet)
        Try
            Return (From R In DB.Wallets Where R.Username = username Order By R.Createdon Descending).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Wallet)
        End Try
    End Function
End Class
