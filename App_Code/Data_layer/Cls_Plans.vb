﻿Imports Microsoft.VisualBasic
Imports System
Public Class Cls_Plans
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.Plan) As ResponseInfo
        Try
            DB.Plans.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.Plan) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal planid As String) As Fastcyclepro.Plan
        Try
            Return (From R In DB.Plans Where R.PlanID = planid).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.Plan

        End Try
    End Function





    Public Function SelectAllplans() As List(Of Fastcyclepro.Plan)
        Try
            Return (From U In DB.Plans).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.Plan)
        End Try
    End Function
End Class
