﻿Imports Microsoft.VisualBasic

Public Class cls_adminprofit
    Dim DB As Fastcyclepro.FastcycleproDataContext
    Public Sub New(Optional ByVal Context As Fastcyclepro.FastcycleproDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Fastcyclepro.FastcycleproDataContext
        Else
            DB = Context
        End If
    End Sub

    Public Function Insert(ByVal G As Fastcyclepro.AdminProfit) As ResponseInfo
        Try
            DB.adminprofits.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Fastcyclepro.AdminProfit) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisID(ByVal sn As String) As Fastcyclepro.AdminProfit
        Try
            Return (From R In DB.adminprofits Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New Fastcyclepro.adminprofit

        End Try
    End Function

    Public Function SelectAlladminprofits() As List(Of Fastcyclepro.AdminProfit)
        Try
            Return (From R In DB.adminprofits).ToList()
        Catch ex As Exception
            Return New List(Of Fastcyclepro.adminprofit)
        End Try
    End Function
End Class
