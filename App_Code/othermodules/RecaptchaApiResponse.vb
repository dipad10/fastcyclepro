﻿Imports Microsoft.VisualBasic

Imports System.Runtime.Serialization
Imports System.Text
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports System.IO
Imports Newtonsoft.Json

Public Class RecaptchaApiResponse
    Public Shared Function Validate(EncodedResponse As String) As String
        Dim client = New WebClient()

        Dim PrivateKey As String = "6LcflCcUAAAAABXYhqIwENkCFhOKV6o5rLdrxrMH"

        Dim req As WebRequest = WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=" & PrivateKey & "&response=" & EncodedResponse & "")
        Dim postData As String = "https://www.google.com/recaptcha/api/siteverify?secret=" & PrivateKey & "&response=" & EncodedResponse & ""

        Dim send As Byte() = Encoding.[Default].GetBytes(postData)
        req.Method = "POST"
        req.ContentType = "application/json; charset=utf-8"
        req.ContentLength = send.Length

        Dim sout As Stream = req.GetRequestStream()
        sout.Write(send, 0, send.Length)
        sout.Flush()
        sout.Close()

        Dim res As WebResponse = req.GetResponse()
        Dim sr As New StreamReader(res.GetResponseStream())
        Dim returnvalue As String = sr.ReadToEnd()

        Dim captchaResponse = JsonConvert.DeserializeObject(Of RecaptchaApiResponse)(returnvalue)

        Return captchaResponse.Success
    End Function

    <JsonProperty("success")> _
    Public Property Success() As String
        Get
            Return m_Success
        End Get
        Set(value As String)
            m_Success = value
        End Set
    End Property

    Private m_Success As String
    <JsonProperty("error-codes")> _
    Public Property ErrorCodes() As List(Of String)
        Get
            Return m_ErrorCodes
        End Get
        Set(value As List(Of String))
            m_ErrorCodes = value
        End Set
    End Property

    Private m_ErrorCodes As List(Of String)
  

End Class
