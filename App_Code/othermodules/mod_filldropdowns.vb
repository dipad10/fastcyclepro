﻿Imports Microsoft.VisualBasic
Imports DevExpress.Web
Public Module mod_filldropdowns

    Public Sub Filltitles(ByVal cb As DropDownList)
        cb.Items.Clear()
        cb.Items.Add(New ListItem("Mr.", "MR."))
        cb.Items.Add(New ListItem("Mrs.", "MRS."))
        cb.Items.Add(New ListItem("DR.", "DR"))
        cb.Items.Add(New ListItem("MST", "MST"))
        cb.Items.Add(New ListItem("MISS", "MISS"))
        cb.Items.Insert(0, New ListItem("- select -", "NULL"))
    End Sub

    'Public Sub FillTicketcategories(ByVal cb As ASPxComboBox)
    '    Dim obj As List(Of GHD5.TicketCategory) = (New cls_ticketcategories).SelectAllcategories
    '    cb.DataSource = obj
    '    cb.Columns.Add("CategoryID").Width = 30
    '    cb.Columns.Add("Categoryname").Width = 100

    '    cb.ValueField = "Categoryname"
    '    cb.TextField = "CategoryID"

    '    cb.TextFormatString = "{0},{1}"
    '    cb.IncrementalFilteringMode = IncrementalFilteringMode.Contains
    '    cb.DataBindItems()
    'End Sub

    'Public Sub FillTicketstatus(ByVal cb As ASPxComboBox)
    '    Dim obj As List(Of GHD5.StatusType) = (New cls_statustypes).SelectAllstatustypes
    '    cb.DataSource = obj

    '    cb.Columns.Add("Statustype").Width = 100

    '    cb.ValueField = "Tag"
    '    cb.TextField = "StatusType"

    '    cb.TextFormatString = "{0}"
    '    cb.IncrementalFilteringMode = IncrementalFilteringMode.Contains
    '    cb.DataBindItems()
    'End Sub

    'Public Sub FillTicketcategoriesddl(ByVal cb As DropDownList)
    '    Dim obj As List(Of GHD5.TicketCategory) = (New cls_ticketcategories).SelectAllcategories
    '    cb.DataSource = obj
    '    cb.DataTextField = "Categoryname"
    '    cb.DataValueField = "CategoryID"
    '    cb.DataBind()
    '    cb.Items.Insert(0, New ListItem("- select -", "NULL"))
    'End Sub

    'Public Sub Fillcalltypes(ByVal cb As DropDownList)
    '    Dim obj As List(Of GHD5.CallType) = (New Cls_calltypes).SelectAllcalltypes
    '    cb.DataSource = obj
    '    cb.DataTextField = "CallTypes"
    '    cb.DataValueField = "CallTypeID"
    '    cb.DataBind()
    '    cb.Items.Insert(0, New ListItem("- select -", "NULL"))
    'End Sub

    'Public Sub FillAssigneesbyID(ByVal cb As ASPxComboBox, ByVal categoryid As Integer)
    '    Dim obj As List(Of GHD5.User) = (New cls_users).SelectuserbycategoryID(categoryid)

    '    cb.DataSource = obj
    '    cb.Columns.Clear()
    '    cb.Columns.Add("UserID").Width = 50
    '    cb.Columns.Add("Username").Width = 100
    '    cb.Columns.Add("firstname").Width = 100
    '    cb.Columns.Add("Lastname").Width = 150
    '    cb.ValueField = "Username"
    '    cb.TextField = "UserID"
    '    cb.TextFormatString = "{0},{1}"
    '    cb.DataBind()

    'End Sub

    Public Sub fillusers(ByVal cb As ASPxComboBox)
        Dim obj As List(Of Fastcyclepro.Registration) = (New Cls_Registration).SelectAllUsers

        cb.DataSource = obj
        cb.Columns.Clear()
        cb.Columns.Add("UserID").Width = 60
        cb.Columns.Add("UserName").Width = 120
        cb.Columns.Add("Email").Width = 220
        cb.ValueField = "UserName"
        cb.TextField = "UserID"
        cb.TextFormatString = "{0},{1}"
        cb.DataBind()

    End Sub


End Module
