﻿Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Text

'Imports Newtonsoft.Json

Namespace coinpaymentspay
    Public Class payment

        'Private Shared Sub Main(args As String())
        '    PostJson("https://api.amplifypay.com/merchant/transact", New template() With { _
        '        .merchantId = "NJEUEGZ22UI5MI7MZ4FQA", _
        '        .apiKey = "46f46a54-6865-4073-9bd8-e9d68fee0c3e", _
        '        .Amount = "200", _
        '        .customerEmail = "dipad10@rocketmail.com", _
        '        .planId = "200", _
        '        .paymentDescription = "BtcEx Bitcoin Payment", _
        '        .redirectUrl = "http://localhost:1118/private/payment_successful.aspx", _
        '        .transID = Guid.NewGuid.ToString _
        '    })


        'End Sub

        Public Shared Function HttpPost(URI As String, Parameters As String) As String
            Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(URI)
            'req.Proxy = New System.Net.WebProxy(ProxyString, True)
            'Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded"
            req.Method = "POST"
            'We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            Dim bytes As Byte() = System.Text.Encoding.ASCII.GetBytes(Parameters)
            req.ContentLength = bytes.Length
            Dim os As System.IO.Stream = req.GetRequestStream()
            os.Write(bytes, 0, bytes.Length)
            'Push it out there
            os.Close()
            Dim resp As System.Net.WebResponse = req.GetResponse()
            If resp Is Nothing Then
                Return Nothing
            End If
            Dim sr As New System.IO.StreamReader(resp.GetResponseStream())
            Return sr.ReadToEnd().Trim()
        End Function
        'Public Shared Sub PostJson(uri As String, postParameters As template)
        '    Dim postData As String = JsonConvert.SerializeObject(postParameters)
        '    Dim bytes As Byte() = Encoding.UTF8.GetBytes(postData)
        '    Dim httpWebRequest = DirectCast(WebRequest.Create(uri), HttpWebRequest)
        '    httpWebRequest.Method = "POST"
        '    httpWebRequest.ContentLength = bytes.Length
        '    httpWebRequest.ContentType = "application/json"
        '    Using requestStream As Stream = httpWebRequest.GetRequestStream()
        '        requestStream.Write(bytes, 0, bytes.Count())
        '    End Using
        '    Dim httpWebResponse = DirectCast(httpWebRequest.GetResponse(), HttpWebResponse)
        '    If httpWebResponse.StatusCode <> HttpStatusCode.OK Then
        '        Dim message As String = [String].Format("POST failed. Received HTTP {0}", httpWebResponse.StatusCode)
        '        Throw New ApplicationException(message)
        '    End If
        'End Sub

    End Class

    Public Class template

        Public Property version() As String
            Get
                Return _version
            End Get
            Set(value As String)
                _version = value
            End Set
        End Property
        Private _version As String
        Public Property key() As String
            Get
                Return _key
            End Get
            Set(value As String)
                _key = value
            End Set
        End Property
        Private _key As String

        Public Property cmd() As String
            Get
                Return _cmd
            End Get
            Set(value As String)
                _cmd = value
            End Set
        End Property
        Private _cmd As String

        Public Property amount() As String
            Get
                Return _Amount
            End Get
            Set(value As String)
                _Amount = value
            End Set
        End Property
        Private _amount As String

        Public Property currency1() As String
            Get
                Return _currency1
            End Get
            Set(value As String)
                _currency1 = value
            End Set
        End Property
        Private _currency1 As String

        Public Property currency() As String
            Get
                Return _currency
            End Get
            Set(value As String)
                _currency = value
            End Set
        End Property
        Private _currency As String

        Public Property address() As String
            Get
                Return _address
            End Get
            Set(value As String)
                _address = value
            End Set
        End Property
        Private _address As String

        Public Property ipn_url() As String
            Get
                Return _ipn_url
            End Get
            Set(value As String)
                _ipn_url = value
            End Set
        End Property
        Private _ipn_url As String


        Public Property auto_confirm() As String
            Get
                Return _auto_confirm
            End Get
            Set(value As String)
                _auto_confirm = value
            End Set
        End Property
        Private _auto_confirm As String

    End Class
End Namespace