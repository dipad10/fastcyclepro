﻿Imports System.Net
Imports System.Text
Imports System.Web.Mail
Imports System.Configuration
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Threading

Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.IO.Ports
Imports System.Net.Mail
'Imports CrystalDecisions.Shared
Imports System.Security.Cryptography
Imports System.IO
Imports System.Web.HttpContext
Public Module mod_main

    'reference this m_strconnString anywhere u want to call ur connection into d DB cos its now made public
    Public m_strconnString As String = (New Fastcyclepro.FastcycleproDataContext).Connection.ConnectionString
    Public settings As schoolsettings

    Public Enum ErrCodeEnum
        NO_ERROR = 0
        INVALID_GUID = 200
        INVALID_PASSWORD = 200
        FORBIDDEN = 407
        INVALID_RANGE = 408
        ACCOUNT_LOCKED = 400
        ACCOUNT_EXPIRED = 400
        GENERIC_ERROR = 100
    End Enum

    Public Class ResponseInfo
        Public ErrorCode As Integer
        Public ErrorMessage As String
        Public ExtraMessage As String
        Public TotalSuccess As Integer
        Public TotalFailure As Integer
        Public TotalCharged As Integer
        Public CurrentBalance As Integer
    End Class

#Region "GET FUNCTIONS"

    Public Function getQueryString(Optional ByVal q As NameValueCollection = Nothing, Optional ByVal SkipParam As String = "") As String
        If q Is Nothing Then q = My.Request.QueryString
        Dim query As String = "", skips() As String = Split(SkipParam, ",")

        For p As Integer = 0 To q.Count - 1
            If Not skips.Contains(q.GetKey(p)) Then
                query &= "&" & q.GetKey(p) & "=" & String.Join("", q.GetValues(p))
            End If
        Next

        If query.StartsWith("&") Then query = query.Remove(0, 1)
        Return query
    End Function
    Public ReadOnly Property paymentamount() As String
        Get
            Return HttpContext.Current.Session("amount")
        End Get
    End Property
    Public Function IsValidEmail(ByVal strIn As String) As Boolean
        Try
            Return Regex.IsMatch(strIn, "^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IsValidNigerianNo(ByVal MobileNo As String) As Boolean
        Return (MobileNo Like "23480########") Or (MobileNo Like "23470########") Or (MobileNo Like "080########") Or (MobileNo Like "070########") Or (MobileNo Like "090########") Or (MobileNo Like "081########")

    End Function
    Public Function DecodeFromBase64String(ByVal inputStr As String) As String
        Try
            If Len(inputStr) > 0 And (Len(inputStr) Mod 4) > 0 Then
                inputStr = inputStr & Left("====", (4 - (Len(inputStr) Mod 4)))
            End If
            ' Convert the binary input into Base64 UUEncoded output.
            Dim binaryData() As Byte = System.Convert.FromBase64String(inputStr)
            Return (New System.Text.UnicodeEncoding).GetString(binaryData)
        Catch exp As System.ArgumentNullException
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Structure schoolsettings
        Dim id As Integer
        Dim name As String
        Dim logo As String
        Dim address As String
        Dim website As String

    End Structure

    Public Sub Messagebox(ByVal Page As UI.Page, ByVal message As String)
        Page.Response.Cookies.Add(New HttpCookie("msgbox", message))
    End Sub
    Public Function getAdhocTable(ByVal strSQL As String, ByVal con As Data.Common.DbConnection) As Data.DataTable
        Dim cmd As New SqlCommand(strSQL, con)
        With cmd
            .CommandType = CommandType.Text

            Dim rdr As New SqlDataAdapter(cmd)
            Dim tbl As New DataTable("Table1")
            rdr.Fill(tbl) : Return tbl
        End With
    End Function
#End Region

#Region "CORE SYSTEM FUNCTIONS"
    Public Function _GetResponseStruct(ByVal ErrCode As ErrCodeEnum, Optional ByVal TotalSuccess As Integer = 0, _
     Optional ByVal TotalFailure As Integer = 0, Optional ByVal ErrorMsg As String = "", Optional ByVal ExtraMsg As String = "", Optional ByVal currentBalance As Integer = 0) As ResponseInfo
        Dim res As New ResponseInfo
        With res
            .ErrorCode = ErrCode
            .ErrorMessage = ErrorMsg
            .ExtraMessage = ExtraMsg
            .TotalSuccess = TotalSuccess
            .TotalFailure = TotalFailure
            .CurrentBalance = currentBalance
        End With
        Return res
    End Function


    Public Function Encrypt(ByVal clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Public Function testbind(ByVal value As Object) As String
        If value = "null" Then
            Return "0.0000"

        End If

        Return value.ToString()
    End Function


    Public Function Decrypt(ByVal cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function

    Public Function converttobtc(amount As Integer)
        Try
            Dim uri = String.Format("https://blockchain.info/tobtc?currency=USD&value={0}", amount)

            Dim client As New WebClient()
            client.UseDefaultCredentials = True
            Dim data = client.DownloadString(uri)

            Dim result = Convert.ToDouble(data)
            Return result
        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Function PopulateBody(ByVal fullname As String, ByVal Username As String, ByVal Password As String, ByVal Email As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/WelcomeEmailTemplate.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Password}", Password)
        body = body.Replace("{Email}", Email)
        body = body.Replace("{fullname}", fullname)
        Return body
    End Function

    Public Function PopulateBodyreferralbonus(ByVal fullname As String, ByVal referral As String, ByVal amount As String, ByVal description As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateRefererbonus.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{referral}", referral)
        body = body.Replace("{amount}", amount)
        body = body.Replace("{description}", description)
        Return body
    End Function

    Public Function PopulateBodycycled(ByVal fullname As String, ByVal position As String, ByVal level As String, ByVal createdon As String, ByVal btcamount As String, ByVal walletaddress As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatecycled.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{position}", position)
        body = body.Replace("{level}", level)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{btcamount}", btcamount)
        body = body.Replace("{walletaddress}", walletaddress)
        Return body
    End Function

    Public Function PopulateBodynewposition(ByVal fullname As String, ByVal position As String, ByVal level As String, ByVal createdon As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatenewposition.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{position}", position)
        body = body.Replace("{level}", level)
        body = body.Replace("{createdon}", createdon)
      
        Return body
    End Function

    Public Function PopulateBodyactivation(ByVal Username As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateActivation.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Url}", Url)


        Return body


    End Function

    Public Function PopulateBodymastermail(ByVal Username As String, ByVal subject As String, ByVal message As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/mastermail.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Message}", message)
        body = body.Replace("{subject}", subject)

        Return body


    End Function

    Public Function PopulateBodyforgotpwd(ByVal Username As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateforgotpwd.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Url}", Url)

        Return body

    End Function
    'Public Function PopulateBodyorderplaced(ByVal type As String, ByVal guid As String, ByVal username As String, ByVal btcamt As Double, ByVal nairaamt As Double, ByVal createdon As Date) As String
    '    Dim body As String = String.Empty
    '    Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateorderplaced.html"))
    '    body = reader.ReadToEnd
    '    body = body.Replace("{type}", type)
    '    body = body.Replace("{guid}", guid)
    '    body = body.Replace("{btcamt}", btcamt)
    '    body = body.Replace("{nairaamt}", nairaamt)
    '    body = body.Replace("{createdon}", createdon)
    '    body = body.Replace("{username}", username)

    '    Return body
    'End Function

    'Public Function PopulateBodyorderplacedadmin(ByVal type As String, ByVal guid As String, ByVal orderid As String, ByVal username As String, ByVal btcamt As Double, ByVal nairaamt As Double, ByVal createdon As Date) As String
    '    Dim body As String = String.Empty
    '    Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateorderplacedadmin.html"))
    '    body = reader.ReadToEnd
    '    body = body.Replace("{type}", type)
    '    body = body.Replace("{guid}", guid)
    '    body = body.Replace("{orderid}", orderid)
    '    body = body.Replace("{btcamt}", btcamt)
    '    body = body.Replace("{nairaamt}", nairaamt)
    '    body = body.Replace("{createdon}", createdon)
    '    body = body.Replace("{username}", username)

    '    Return body
    'End Function

    'Public Function PopulateBodyreplyadmin(ByVal TicketID As String, ByVal Creator As String, ByVal summary As String, ByVal message As String, ByVal priority As String, ByVal assignee As String, ByVal url As String, ByVal submittedon As String) As String
    '    Dim body As String = String.Empty
    '    Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatereplyadmin.html"))
    '    body = reader.ReadToEnd
    '    body = body.Replace("{TicketID}", TicketID)
    '    body = body.Replace("{Creator}", Creator)
    '    body = body.Replace("{summary}", summary)
    '    body = body.Replace("{message}", message)
    '    body = body.Replace("{Priority}", priority)
    '    body = body.Replace("{Assignee}", assignee)
    '    body = body.Replace("{Url}", url)
    '    body = body.Replace("{submittedon}", submittedon)
    '    Return body
    'End Function

    Public Function PopulateBodysponsoralert(ByVal Username As String, ByVal Planname As String, ByVal Donationamount As String, ByVal Bitcoinamount As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateSponsoralert.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Planname}", Planname)
        body = body.Replace("{Donationamount}", Donationamount)
        body = body.Replace("{BitcoinAmount}", Bitcoinamount)
        body = body.Replace("{Url}", Url)

        Return body
    End Function

    Public Function PopulateBodydepositcomplete(ByVal fullname As String, ByVal createdon As String, ByVal amount As String, ByVal paymentmode As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatedepositcomplete.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{amount}", amount)
        body = body.Replace("{paymentmode}", paymentmode)

        Return body
    End Function

    Public Function PopulateBodydepositpending(ByVal fullname As String, ByVal createdon As String, ByVal amount As String, ByVal paymentmode As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatedepositpending.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{amount}", amount)
        body = body.Replace("{paymentmode}", paymentmode)

        Return body
    End Function
    Public Function PopulateBodywithdrawalprocessed(ByVal fullname As String, ByVal createdon As String, ByVal amount As String, ByVal paymentmode As String, ByVal address As String, ByVal txnid As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplatewithdrawalprocessed.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{amount}", amount)
        body = body.Replace("{address}", address)
        body = body.Replace("{txn_id}", txnid)
        body = body.Replace("{paymentmode}", paymentmode)


        Return body
    End Function

    Public Function PopulateBodyprocessed(ByVal fullname As String, ByVal createdon As String, ByVal amount As String, ByVal address As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateprocessed.html"))
        body = reader.ReadToEnd
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{createdon}", createdon)
        body = body.Replace("{amount}", amount)
        body = body.Replace("{address}", address)


        Return body
    End Function

    Public Function PopulateBodysponsoralertsuccess(ByVal Username As String, ByVal Planname As String, ByVal Donationamount As String, ByVal Bitcoinamount As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateSponsoralertsuccess.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Planname}", Planname)
        body = body.Replace("{Donationamount}", Donationamount)
        body = body.Replace("{BitcoinAmount}", Bitcoinamount)
        body = body.Replace("{Url}", Url)

        Return body
    End Function

    Public Function PopulateBodyreferrer(ByVal Username As String, ByVal fullname As String, ByVal createdon As Date) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateReferer.html"))
        body = reader.ReadToEnd
        body = body.Replace("{username}", Username)
        body = body.Replace("{fullname}", fullname)
        body = body.Replace("{createdon}", createdon)



        Return body
    End Function

    Public Function PopulateBodysponsoralertadmin(ByVal Username As String, ByVal donationid As String, ByVal Donationamount As String, ByVal Bitcoinamount As String, ByVal Url As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateSponsoralertAdmin.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{donationid}", donationid)
        body = body.Replace("{Donationamount}", Donationamount)
        body = body.Replace("{BitcoinAmount}", Bitcoinamount)
        body = body.Replace("{Url}", Url)

        Return body
    End Function

    Public Function PopulateBodydonoralert(ByVal Username As String, ByVal Planname As String, ByVal Donationamount As String, ByVal Bitcoinamount As String, ByVal Duecollectiondate As Date, ByVal profitinreturn As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateDonoralert.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Planname}", Planname)
        body = body.Replace("{Donationamount}", Donationamount)
        body = body.Replace("{BitcoinAmount}", Bitcoinamount)
        body = body.Replace("{Duecollectiondate}", Duecollectiondate)
        body = body.Replace("{profitinreturn}", profitinreturn)
        Return body
    End Function

    Public Function PopulateBodydonoralert2confirmation(ByVal Username As String, ByVal sponsorname As String, ByVal Planname As String, ByVal Bitcoinamount As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateDonoralert2confirmations.html"))
        body = reader.ReadToEnd
        body = body.Replace("{Username}", Username)
        body = body.Replace("{Planname}", Planname)

        body = body.Replace("{BitcoinAmount}", Bitcoinamount)
        body = body.Replace("{sponsorname}", sponsorname)

        Return body
    End Function

    Public Function PopulateBodydonoralertadmin(ByVal sponsorname As String, ByVal Donationamount As String, ByVal Bitcoinamount As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateDonoralertAdmin.html"))
        body = reader.ReadToEnd
        body = body.Replace("{sponsorname}", sponsorname)

        body = body.Replace("{Donationamount}", Donationamount)
        body = body.Replace("{BitcoinAmount}", Bitcoinamount)

        Return body
    End Function
    Public Function SendHtmlFormattedEmail(ByVal recepientEmail As String, ByVal cc As String, ByVal subject As String, ByVal body As String) As Boolean
        Try
            Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage
            mailMessage.From = New MailAddress(ConfigurationManager.AppSettings("UserName"), "Fastcyclepro.com")
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail))
            If cc <> "" Then
                Dim CCId As String() = cc.Split(","c)

                For Each CCEmail As String In CCId
                    'Adding Multiple CC email Id
                    mailMessage.CC.Add(New MailAddress(CCEmail))


                Next
            Else
                'dont send cc email
            End If

            Dim smtp As SmtpClient = New SmtpClient
            smtp.Host = ConfigurationManager.AppSettings("Host")
            smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings("EnableSsl"))
            Dim NetworkCred As System.Net.NetworkCredential = New System.Net.NetworkCredential
            NetworkCred.UserName = ConfigurationManager.AppSettings("UserName")
            NetworkCred.Password = ConfigurationManager.AppSettings("Password")
            smtp.UseDefaultCredentials = True
            smtp.Credentials = NetworkCred
            smtp.Port = Integer.Parse(ConfigurationManager.AppSettings("Port"))
            smtp.Send(mailMessage)

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    'Public Function InsertActivity(Optional ByVal LogType As String = "", Optional ByVal Source As String = "", Optional ByVal Categories As String = "", Optional ByVal LogDetails As String = "", Optional ByVal SubmittedBy As String = "", Optional ByVal ticketid As String = "", Optional ByVal filepath As String = "", Optional ByVal filename As String = "") As Boolean

    '    Dim Rec As New GHD5.Activity

    '    Rec.LogType = LogType
    '    Rec.Source = Source
    '    Rec.Category = Categories
    '    Rec.Description = LogDetails
    '    Rec.SubmittedBy = SubmittedBy
    '    Rec.SubmittedOn = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
    '    Rec.UserID = ticketid
    '    Rec.FIlePath = filepath
    '    Rec.FileName = filename
    '    Dim res As ResponseInfo = (New cls_activity).Insert(Rec)
    '    If res.ErrorCode = 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If

    'End Function
    Public Function updateMatchstatus(ByVal matchid As String, ByVal status As String) As Boolean
        Try
            Dim A As New cls_matches
            Dim rec As Fastcyclepro.Match = A.SelectThisID(matchid)

            rec.Status = status
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function updateposition(ByVal positionid As String, ByVal earned As Double, ByVal status As String, ByVal downlines As Integer) As Boolean
        Try
            Dim A As New cls_position
            Dim rec As Fastcyclepro.Position = A.SelectThispositionid(positionid)

            rec.Status = status
            rec.Earned = earned
            rec.Downlines = 3
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function updatepositionblazer(ByVal positionid As String, ByVal earned As Double, ByVal status As String, ByVal downlines As Integer) As Boolean
        Try
            Dim A As New cls_blazer
            Dim rec As fastcyclepro.Blazer = A.SelectThispositionid(positionid)

            rec.Status = status
            rec.Earned = earned
            rec.Downlines = 3
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function


    Public Function updateMatchstatusbyID(ByVal ID As String, ByVal status As String) As Boolean
        Try
            Dim A As New cls_matches
            Dim rec As Fastcyclepro.Match = A.SelectThisID(ID)

            rec.Status = status
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function updatesponsoractive(ByVal guid As String, ByVal active As Integer) As Boolean
        Try
            Dim A As New cls_sponsors
            Dim rec As Fastcyclepro.Sponsor = A.SelectThisdonationid(guid)

            rec.Active = active
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function updatedonationstatus(ByVal guid As String, ByVal status As String) As Boolean
        Try
            Dim A As New cls_donations
            Dim rec As Fastcyclepro.Donation = A.SelectThisguid(guid)

            rec.Status = status
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function updatesponsorstatus(ByVal guid As String, ByVal status As String) As Boolean
        Try
            Dim A As New cls_sponsors
            Dim rec As Fastcyclepro.Sponsor = A.SelectThisdonationid(guid)

            rec.Status = status
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function updatesponsoractive(ByVal guid As String, ByVal active As String) As Boolean
        Try
            Dim A As New cls_sponsors
            Dim rec As Fastcyclepro.Sponsor = A.SelectThisdonationid(guid)

            rec.Active = active
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function updatedonationsactive(ByVal guid As String, ByVal active As String) As Boolean
        Try
            Dim A As New cls_donations
            Dim rec As Fastcyclepro.Donation = A.SelectThisguid(guid)

            rec.Active = active
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function Updateautonumber(ByVal numbertype As String) As Boolean
        Try
            Dim A As New Cls_Autosettings
            Dim rec As Fastcyclepro.Autosetting = A.Getautonumbervalue(numbertype)
            rec.Nextvalue = rec.Nextvalue + 1
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function Getautonumber(ByVal numbertype As String) As String
        Dim value As String = ""
        Try
            Dim A As New Cls_Autosettings
            Dim rec As Fastcyclepro.Autosetting = A.Getautonumbervalue(numbertype)

            value = rec.Nextvalue
        Catch ex As Exception
            Return ex.Message
        End Try
        Return value

    End Function
    Public Function Resetautonumber(ByVal numbertype As String) As Boolean
        Try
            Dim A As New Cls_Autosettings
            Dim rec As Fastcyclepro.Autosetting = A.Getautonumbervalue(numbertype)
            rec.Nextvalue = 1
            Dim res As ResponseInfo = A.Update(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function


    Public Function Savenewposition(ByVal username As String, ByVal positionid As String, ByVal levelid As Integer, ByVal downlines As Integer, ByVal earned As Double, ByVal purchaseamt As Double, ByVal status As String, ByVal createdon As Date, ByVal guid As String) As Boolean
        Try
            Dim A As New cls_position
            Dim rec As New Fastcyclepro.Position
            rec.Username = username
            rec.PositionID = positionid
            rec.LevelID = levelid
            rec.Downlines = downlines
            rec.Earned = earned
            rec.Purchased = purchaseamt
            rec.TransGUID = guid
            rec.createdon = createdon
            rec.Status = status
            rec.RepurchaseTime = DateTime.Now.AddHours(1)
            Dim res As ResponseInfo = A.Insert(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SavenewpositionBlazer(ByVal username As String, ByVal positionid As String, ByVal levelid As String, ByVal downlines As Integer, ByVal earned As Double, ByVal purchaseamt As Double, ByVal status As String, ByVal createdon As Date, ByVal guid As String) As Boolean
        Try
            Dim A As New cls_blazer
            Dim rec As New fastcyclepro.Blazer
            rec.Username = username
            rec.PositionID = positionid
            rec.LevelID = levelid
            rec.Downlines = downlines
            rec.Earned = earned
            rec.Purchased = purchaseamt
            rec.TransGUID = guid
            rec.createdon = createdon
            rec.Status = status
            rec.RepurchaseTime = DateTime.Now.AddMinutes(5)
            Dim res As ResponseInfo = A.Insert(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function Savewallet(ByVal Accountype As String, ByVal deposit As Double, ByVal withdrawal As Double, ByVal amount As Double, ByVal username As String, ByVal type As String, ByVal transactionid As String, ByVal description As String, ByVal downline As String) As Boolean
        Try
            Dim A As New cls_wallet
            Dim rec As New Fastcyclepro.Wallet
            rec.AccountType = Accountype
            rec.Item_name = description
            rec.Deposit = deposit
            rec.Withdrawal = withdrawal
            rec.amount = amount
            rec.amount1 = amount
            rec.amount2 = amount
            rec.Total = amount
            rec.TransGUID = Guid.NewGuid.ToString
            rec.Type = type
            rec.Username = username
            rec.TransactionID = transactionid
            rec.Createdon = Date.Now
            rec.Status = "100"
            rec.Statustext = "COMPLETED"
            rec.Field3 = downline
            Dim res As ResponseInfo = A.Insert(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function Saveturbowallet(ByVal Accountype As String, ByVal deposit As Double, ByVal withdrawal As Double, ByVal currentbalance As Double, ByVal speedstep As Double, ByVal amount As Double, ByVal username As String, ByVal type As String, ByVal transactionid As String, ByVal description As String, ByVal downline As String) As Boolean
        Try
            Dim A As New cls_turboTurboWallet
            Dim rec As New fastcyclepro.TurboWallet
            rec.AccountType = Accountype
            rec.Item_name = description
            rec.Deposit = deposit
            rec.Withdrawal = withdrawal
            rec.amount = amount
            rec.amount1 = amount
            rec.amount2 = amount
            rec.CurrentBalance = currentbalance
            rec.Step = speedstep
            rec.TransGUID = Guid.NewGuid.ToString
            rec.Type = type
            rec.Username = username
            rec.TransactionID = transactionid
            rec.Createdon = Date.Now
            rec.Status = "100"
            rec.Statustext = "COMPLETED"
            rec.Field3 = downline
            rec.Active = 1
            Dim res As ResponseInfo = A.Insert(rec)
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function savetestimonial(ByVal userid As String, ByVal userfullname As String, ByVal usercountry As String, ByVal planname As String, ByVal donationamt As String, ByVal startdate As Date, ByVal matchdate As Date, ByVal btcreceived As String, ByVal sponsordonationid As String) As Boolean
        Dim A As New cls_testimonials
        Dim rec As New Fastcyclepro.Testimonial
        Dim numbertype As String = "testimonialid"
        Dim auto As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype)
        Dim testid As String = String.Format("TEST/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)

        rec.Testimonyid = testid
        rec.submittedon = Date.Now
        rec.Sponsorid = userid
        rec.Sponsordonationid = sponsordonationid
        rec.Testimony = String.Format("Hello, i am {0} from {1} and a participant of Fastcyclepro.com, On {2} i subscribed to the {3} PLAN and donated {4} and on {5} i was matched to get paid for the sum of {6} which was sent to my blockchain wallet. I'm so glad, join Fastcyclepro.com today because it pays!!!.", userfullname, usercountry, startdate, planname, donationamt, matchdate, btcreceived)
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            Return True
        Else
            Return False

        End If
    End Function

#End Region
End Module
