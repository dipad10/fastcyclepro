﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports block_io_sharp

Namespace libCoinPaymentsNET
    Public Class CoinPayments
        Private Shared s_privkey As String = ConfigurationManager.AppSettings("privatekey")
        Private Shared s_pubkey As String = ConfigurationManager.AppSettings("publickey")
      
        Private Shared ReadOnly encoding As Encoding = encoding.UTF8
        Public Shared transid As String = ""

        'Public Sub New(privkey As String, pubkey As String)
        '    s_privkey = privkey
        '    s_pubkey = pubkey
        '    If s_privkey.Length = 0 OrElse s_pubkey.Length = 0 Then
        '        Throw New ArgumentException("Private or Public Key is empty")
        '    End If
        'End Sub

        Public Shared Function Getcallbackaddress(ByVal cmd As String, ByVal currency As String, ByVal version As String, ByVal key As String) As String

            Dim post_data As String = ""
            post_data = "currency=" & currency & "&cmd=" & cmd & "&version=" & version & "&key=" & key & ""


            Dim keyBytes As Byte() = encoding.GetBytes(s_privkey)
            Dim postBytes As Byte() = encoding.GetBytes(post_data)
            Dim hmacsha512 = New System.Security.Cryptography.HMACSHA512(keyBytes)
            Dim hmac As String = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", String.Empty)

            ' do the post:
            Dim cl As New System.Net.WebClient()
            cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
            cl.Headers.Add("HMAC", hmac)
            cl.Encoding = encoding

            Dim ret = New Dictionary(Of String, Object)()
            Dim resultDictionary As New Dictionary(Of String, Object)
            Try
                Dim resp As String = cl.UploadString("https://www.coinpayments.net/api.php", post_data)
                Dim decoder = New System.Web.Script.Serialization.JavaScriptSerializer()
                ret = decoder.Deserialize(Of Dictionary(Of String, Object))(resp)

            Catch e As System.Net.WebException
                ret("error") = "Exception while contacting CoinPayments.net: " + e.Message
            Catch e As Exception
                ret("error") = "Unknown exception: " + e.Message
            End Try
            If ret.ContainsKey("result") Then


                resultDictionary = DirectCast(ret("result"), Dictionary(Of String, Object))
                transid = resultDictionary("address")

            End If
            Return transid


        End Function

        Public Shared Function createtransaction(ByVal cmd As String, ByVal version As String, ByVal amount As String, ByVal currency As String, ByVal currency2 As String, ByVal buyer_email As String, ByVal buyer_name As String, ByVal item_name As String, ByVal item_number As String, ByVal invoice As String, ByVal custom As String, ByVal key As String) As String

            Dim post_data As String = ""
            post_data = String.Format("cmd={0}&version={1}&amount={2}&currency1={3}&buyer_email={4}&buyer_name={5}&item_name={6}&item_number={7}&invoice={8}&custom={9}&key={10}&currency2={11}", cmd, version, amount, currency, buyer_email, buyer_name, item_name, item_number, invoice, custom, key, currency2)


            Dim keyBytes As Byte() = encoding.GetBytes(ConfigurationManager.AppSettings("turboprivatekey"))
            Dim postBytes As Byte() = encoding.GetBytes(post_data)
            Dim hmacsha512 = New System.Security.Cryptography.HMACSHA512(keyBytes)
            Dim hmac As String = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", String.Empty)

            ' do the post:
            Dim cl As New System.Net.WebClient()
            cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
            cl.Headers.Add("HMAC", hmac)
            cl.Encoding = encoding

            Dim ret = New Dictionary(Of String, Object)()
            Dim resultDictionary As New Dictionary(Of String, Object)
            Try
                Dim resp As String = cl.UploadString("https://www.coinpayments.net/api.php", post_data)
                Dim decoder = New System.Web.Script.Serialization.JavaScriptSerializer()
                ret = decoder.Deserialize(Of Dictionary(Of String, Object))(resp)

            Catch e As System.Net.WebException
                ret("error") = "Exception while contacting CoinPayments.net: " + e.Message
            Catch e As Exception
                ret("error") = "Unknown exception: " + e.Message
            End Try
            If ret.ContainsKey("result") Then


                resultDictionary = DirectCast(ret("result"), Dictionary(Of String, Object))
                transid = resultDictionary("address")

            End If
            Return transid


        End Function
        Public Shared Function createtransactionfcp(ByVal cmd As String, ByVal version As String, ByVal amount As String, ByVal currency As String, ByVal currency2 As String, ByVal buyer_email As String, ByVal buyer_name As String, ByVal item_name As String, ByVal item_number As String, ByVal invoice As String, ByVal custom As String, ByVal key As String) As String

            Dim post_data As String = ""
            post_data = String.Format("cmd={0}&version={1}&amount={2}&currency1={3}&buyer_email={4}&buyer_name={5}&item_name={6}&item_number={7}&invoice={8}&custom={9}&key={10}&currency2={11}", cmd, version, amount, currency, buyer_email, buyer_name, item_name, item_number, invoice, custom, key, currency2)


            Dim keyBytes As Byte() = encoding.GetBytes(ConfigurationManager.AppSettings("privatekey"))
            Dim postBytes As Byte() = encoding.GetBytes(post_data)
            Dim hmacsha512 = New System.Security.Cryptography.HMACSHA512(keyBytes)
            Dim hmac As String = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", String.Empty)

            ' do the post:
            Dim cl As New System.Net.WebClient()
            cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
            cl.Headers.Add("HMAC", hmac)
            cl.Encoding = encoding

            Dim ret = New Dictionary(Of String, Object)()
            Dim resultDictionary As New Dictionary(Of String, Object)
            Try
                Dim resp As String = cl.UploadString("https://www.coinpayments.net/api.php", post_data)
                Dim decoder = New System.Web.Script.Serialization.JavaScriptSerializer()
                ret = decoder.Deserialize(Of Dictionary(Of String, Object))(resp)

            Catch e As System.Net.WebException
                ret("error") = "Exception while contacting CoinPayments.net: " + e.Message
            Catch e As Exception
                ret("error") = "Unknown exception: " + e.Message
            End Try
            If ret.ContainsKey("result") Then


                resultDictionary = DirectCast(ret("result"), Dictionary(Of String, Object))
                transid = resultDictionary("address")

            End If
            Return transid


        End Function
        Public Shared Function POSTWITHDAWAL(ByVal cmd As String, ByVal amount As String, ByVal version As String, ByVal currency As String, ByVal address As String, ByVal confirm As String, ByVal note As String, ByVal key As String) As String
            Dim post_data As String = ""
            post_data = "currency=" & currency & "&cmd=" & cmd & "&version=" & version & "&key=" & key & "&amount=" & amount & "&address=" & address & "&auto_confirm=" & confirm & "&note=" & note & ""


            Dim keyBytes As Byte() = encoding.GetBytes(s_privkey)
            Dim postBytes As Byte() = encoding.GetBytes(post_data)
            Dim hmacsha512 = New System.Security.Cryptography.HMACSHA512(keyBytes)
            Dim hmac As String = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", String.Empty)

            ' do the post:
            Dim cl As New System.Net.WebClient()
            cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
            cl.Headers.Add("HMAC", hmac)
            cl.Encoding = encoding

            Dim ret = New Dictionary(Of String, Object)()
            Dim resultDictionary As New Dictionary(Of String, Object)
            Try
                Dim resp As String = cl.UploadString("https://www.coinpayments.net/api.php", post_data)
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim decoder = New System.Web.Script.Serialization.JavaScriptSerializer()
                ret = decoder.Deserialize(Of Dictionary(Of String, Object))(resp)

            Catch e As System.Net.WebException
                ret("error") = "Exception while contacting CoinPayments.net: " + e.Message
            Catch e As Exception
                ret("error") = "Unknown exception: " + e.Message
            End Try
            If ret.ContainsKey("result") Then


                resultDictionary = DirectCast(ret("result"), Dictionary(Of String, Object))
                transid = resultDictionary("id")

            End If
            Return transid

        End Function

        Public Shared Function POSTTURBOWITHDAWAL(ByVal cmd As String, ByVal amount As String, ByVal version As String, ByVal currency As String, ByVal address As String, ByVal confirm As String, ByVal note As String, ByVal key As String) As String
            Dim post_data As String = ""
            post_data = "currency=" & currency & "&cmd=" & cmd & "&version=" & version & "&key=" & key & "&amount=" & amount & "&address=" & address & "&auto_confirm=" & confirm & "&note=" & note & ""


            Dim keyBytes As Byte() = encoding.GetBytes(ConfigurationManager.AppSettings("turboprivatekey"))
            Dim postBytes As Byte() = encoding.GetBytes(post_data)
            Dim hmacsha512 = New System.Security.Cryptography.HMACSHA512(keyBytes)
            Dim hmac As String = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", String.Empty)

            ' do the post:
            Dim cl As New System.Net.WebClient()
            cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
            cl.Headers.Add("HMAC", hmac)
            cl.Encoding = encoding

            Dim ret = New Dictionary(Of String, Object)()
            Dim resultDictionary As New Dictionary(Of String, Object)
            Try
                Dim resp As String = cl.UploadString("https://www.coinpayments.net/api.php", post_data)
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim decoder = New System.Web.Script.Serialization.JavaScriptSerializer()
                ret = decoder.Deserialize(Of Dictionary(Of String, Object))(resp)

            Catch e As System.Net.WebException
                ret("error") = "Exception while contacting CoinPayments.net: " + e.Message
            Catch e As Exception
                ret("error") = "Unknown exception: " + e.Message
            End Try
            If ret.ContainsKey("result") Then


                resultDictionary = DirectCast(ret("result"), Dictionary(Of String, Object))
                transid = resultDictionary("id")

            End If
            Return transid

        End Function

        
    End Class
End Namespace

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
