﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Aboutbitcoin.aspx.vb" Inherits="Aboutbitcoin" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Fastcyclepro | About Bitcoin!</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#3a526a" />
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>
</head>


<body>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
         <div class="page-title title-size-lg text-light" style="background-color: #3a526a;">
			  <div class="container">
			    <div class="inner">
			      <div class="column">
			        <h1>
			         About Bitcoin
			        </h1>
			      </div>

			      <div class="column">
			        <div class="breadcrumbs">
			          <span>
			          	<a href="/default.aspx">
			          		<span property="name">Home</span>
			          	</a>
			          </span>

		          	<i class="material-icons keyboard_arrow_right"></i>

		          	<span property="itemListElement" typeof="ListItem">
		          		<span property="name">About Bitcoins</span>
		          	</span>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<article class="page">
			
                <section class="fw-section padding-top-3x">
			    
                        <style>
                            #btc_video.playback {
                                border-radius: 15px;
                                -webkit-border-radius: 15px;
                                -moz-border-radius: 15px;
                            }

                            #btc_video {
                                width: 560px;
                                height: 315px;
                                position: relative;
                                border-radius: 30px;
                                -webkit-border-radius: 30px;
                                -moz-border-radius: 30px;
                                overflow: hidden;
                                -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                                -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                                box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.4);
                                /* cursor: pointer; */
                            }

                            #btc_bonus {
                                margin-top: 50px;
                                background: #fbf6e8;
                                padding: 40px 0 30px;
                            }

                            .btc-medal {
                                margin-top: -75px;
                            }
                        </style>
                <div class="container">

                    <h2>Bitcoin is the <strong>Future</strong> of Monetary Transactions</h2>

                   <div class="row">
                <div class="col-xs-3">
                    <p class="btc-black-header"><strong>WHAT IS BITCOIN AND HOW TO PARTICIPATE IN FASTCYCLEPRO USING BITCOINS?</strong></p>
                </div>
                <div class="col-xs-6">
                    <div id="btc_video" data-player="<iframe width=&quot;560&quot; height=&quot;315&quot;  data-cke-saved-src=&quot;https://www.youtube.com/embed/Gc2en3nHxA4?autoplay=1&quot; src=&quot;https://www.youtube.com/embed/Gc2en3nHxA4?autoplay=1&quot; frameborder=&quot;0&quot; allowfullscreen></iframe>" class="playback">                    
                        <iframe width="560" height="315" data-cke-saved-src="https://www.youtube.com/embed/Gc2en3nHxA4" src="https://www.youtube.com/embed/Gc2en3nHxA4?autoplay=1" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="col-xs-3">
                    <p class="btc-text">It's more profitable, faster, and safer to provide help using bitcoins rather than traditional currencies.<br><br>
                        Watch the video about Bitcoin's key advantages and find out why it's beneficial to use it while participating in <a href="https://Fastcyclepro.com/" target="_blank">Fastcyclepro</a></p>
                </div>
            </div>
                    <hr>



                </div>


                <div id="btc_bonus">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img class="btc-medal" src="/img/logos/bitcoin-icon.png" width="200" height="200">
                </div>
                <div class="col-xs-9">
                    <br />
                    <h1><strong>Bitcoin is the future of monetary transactions</strong></h1>
                </div>
            </div>
        </div>
    </div>
                <hr />
                <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <h3><strong>Bitcoin advantages</strong></h3>
                    <p class="btc-text">
                        Bitcoin is an international electronic currency. This is a new form of money created on the basis of the program code.<br><br>
                        Bitcoin is not owned by any state, companies or individuals.Due to this, Bitcoin has become the world's first decentralized means of payment.<br><br>
                        Money transfers are made on the peer-to-peer principle — without intermediaries between the sender and the recipient.
                    </p>
                </div>
                <div class="col-xs-8 col-xs-offset-1">
                    <div class="row">
                        <div class="col-xs-6 advantage-block">
                            <img src="img/logos/btc-icon-1.png">
                            <p class="lead text-color-dark">High safety level</p>
                            <p class="">No one can block a Bitcoin wallet. Bitcoin does not depend on central banks, local and international laws, religion and other things.<br><br>
                                Banks and the government do not control your bitcoins — they are only available to you. The wallet is created anonymously. Cryptocurrency cannot be faked, and payments cannot be cancelled.</p>
                        </div>
                        <div class="col-xs-6 advantage-block">
                            <img src="img/logos/btc-icon-2.png">
                            <p class="lead text-color-dark">Cheap transactions anywhere<br> in the world</p>
                            <p class="">Transfer fees do not depend on the amount or location. They make up less than US$1 (regardless of the amount).<br><br> You can transfer money to anyone from anywhere in the world.</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-6 advantage-block">
                            <img src="img/logos/btc-icon-3.png">
                            <p class="lead text-color-dark">Convenience</p>
                            <p class="">Unlike bank transactions, bitcoin transactions may be carried out on a 24/7 basis. You can decide independently when to transfer money.</p>
                        </div>
                        <div class="col-xs-6 advantage-block">
                            <img src="img/logos/btc-icon-4.png">
                            <p class="lead text-color-dark">Instant transfers</p>
                            <p class="">Bank transfers take 3–5 working days on average. The speed of transfers via Bitcoin is a few seconds, or, when you are dealing with large amounts of money, — 10–60 minutes.</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 advantage-block">
                            <img src="img/logos/btc-icon-5.png">
                            <p class="lead text-color-dark">Protection against inflation</p>
                            <p class="">National currencies depreciate annually by 1–20 % (sometimes more). Bitcoin is backed by a limited number of units (21 million) and protected against inflation.<br><br>
                                Cryptocurrency is becoming more and more popular, since people do not spend money on fees and instantly exchange money.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <hr />
                <section class="call-to-action call-to-action-default mb-xl">
                    <div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="call-to-action-content">
									<h3> How to create a Bitcoin wallet on <strong>Coinpayments.net?</strong></h3>
									<p>You are 1 <strong>Step</strong> away from creating your own wallet.</p>
								</div>
								<div class="call-to-action-btn">
									<a href="http://Coinpayments.net" target="_blank" class="btn btn-lg btn-default">Get Started Here!</a>
								</div>
							</div>
						</div>
					</div>
                </section>

                <div class="section section-with-divider">
					
					<div class="container">
					
                            <div class="row">
                                <div class="col-xs-4">
                                    <p>It’s very easy and simple to create and use<br>
                                        wallets on Coinpayments.net:<br><br>
                                        Follow the link <a href="http://Coinpayments.net" target="_blank">https://Coinpayments.net/</a>
                                    </p>
                                    <ol>
                                        <li>Click on "Signup" button</li>
                                        
                                    </ol>
                                </div>
                                <div class="col-xs-8">
                                    <img class="img-responsive" src="/coinpayment/capture.png">
                                </div>
                                <div class="col-xs-12">
                                    <div class="clear-hr"></div>
                                </div>
                            </div>
                        <hr />
                            <div class="row">
                                <div class="col-xs-4">
                                    <ol>
                                        <li>Enter your Username.</li>
                                        <li>Enter Your Email.</li>
                                        <li>Confirm the password.</li>
                                          <li>Confirm the Two checkboxes.</li>
                                        <li>Click on "Signup" button.</li>
                                    </ol>
                                </div>
                                <div class="col-xs-8">
                                    <img class="img-responsive" src="/coinpayment/2.png">
                                </div>
                                <div class="col-xs-12">
                                    <div class="clear-hr"></div>
                                </div>
                            </div>
                        <hr />
                         
                            <div class="row">
                                <div class="col-xs-4">
                                    <ol>
                                         <li>Enter your Username or Email to login.</li>
                                        <li>Enter your password.</li>
                                        <li>Click on "Login" button.</li>
                                    </ol>
                                    <p>(copy & paste the verification code sent to your mail into the provided textbox)
                                    </p>
                                </div>
                                <div class="col-xs-8">
                                   <img class="img-responsive" src="/coinpayment/3.png">
                                </div>
                                <div class="col-xs-12">
                                    <div class="clear-hr"></div>
                                </div>
                            </div>
                        <hr />
                          
                     
					</div>
				</div>

                  <section class="call-to-action call-to-action-default mb-xl">
                    <div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="call-to-action-content">
									<h3> How to transfer Bitcoins or receive Bitcoins on <strong>Coinpayments.net?</strong></h3>
							
								</div>
								<div class="call-to-action-btn">
									<a href="http://Coinpayments.net" target="_blank" class="btn btn-lg btn-default">Get Started Here!</a>
								</div>
							</div>
						</div>
					</div>
                </section>
                <div class="section section-with-divider">
                    <div class="container">
                     
                           <div class="row">
                                <div class="col-xs-4">
                                    <p class="faq-medium-header">To receive Bitcoins, Login to Coinpayments </p>
                                      <ol>
                                         <li>Click on "Your wallet".</li>
                                        <li>Under commands click the dropdown "BTC Options".</li>
                                       <li>Click on "Deposit/Receive".</li>
                                      
                                    </ol>
                                </div>
                                <div class="col-xs-8">
                                  <img class="img-responsive" src="/coinpayment/4b.png">
                                </div>

                            </div>
                       
                            <div class="row">
                                <div class="col-xs-4">
                                    <p class="faq-medium-header">Done!<br>
                                    Now you can see your<br>
                                    Waallet address here:</p>
                                </div>
                                <div class="col-xs-8">
                                     <img class="img-responsive" src="/coinpayment/5.png">
                                </div>
                            </div>
                        <hr />
                           <div class="row">
                                <div class="col-xs-4">
                                    <p class="faq-medium-header">To Transfer Bitcoins to People, Login to Coinpayments </p>
                                      <ol>
                                         <li>Click on "Your wallet".</li>
                                        <li>Under commands click the dropdown "BTC Options".</li>
                                       <li>Click on "Send/Withdraw".</li>
                                       <li>Fill in your amount and the receiver wallet address to send to.</li>
                                    </ol>
                                </div>
                                <div class="col-xs-8">
                                  <img class="img-responsive" src="/coinpayment/4.png">
                                </div>

                            </div>
                       
                            <div class="row">
                                <div class="col-xs-4">
                                    <p class="faq-medium-header">Done!<br>
                                    Now click "Request Withdrawal/Send"</p>
                                </div>
                                <div class="col-xs-8">
                                     <img class="img-responsive" src="/coinpayment/6.png">
                                </div>
                            </div>
                    </div>
                </div>

			  </section>
			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
</body>
</html>
