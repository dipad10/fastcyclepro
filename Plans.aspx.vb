﻿
Partial Class Plans
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
          

            'Bind Alphaplan percentage
            Dim A As Fastcyclepro.Rate = (New Cls_rates).SelectThisID("A")
            alpharate.InnerHtml = String.Format("{0}{1}", A.Percentage, "%")
            alphaprice.InnerHtml = String.Format("${0}", FormatNumber(A.Price, 0))
            alphabtc.InnerHtml = mod_main.converttobtc(A.Price)
            alphareturn.InnerHtml = String.Format("{0} Return on Investment", mod_main.converttobtc(A.Price) * 2)
            'beta
            Dim B As Fastcyclepro.Rate = (New Cls_rates).SelectThisID("B")
            Betarate.InnerHtml = String.Format("{0}{1}", B.Percentage, "%")
            Betaprice.InnerHtml = String.Format("${0}", FormatNumber(B.Price, 0))
            betabtc.InnerHtml = mod_main.converttobtc(B.Price)
            betareturn.InnerHtml = String.Format("{0} Return on Investment", mod_main.converttobtc(B.Price) * 2)
            'platinum
            Dim P As Fastcyclepro.Rate = (New Cls_rates).SelectThisID("P")
            platinumrate.InnerHtml = String.Format("{0}{1}", P.Percentage, "%")
            platinumprice.InnerHtml = String.Format("${0}", FormatNumber(P.Price, 0))
            platinumbtc.InnerHtml = mod_main.converttobtc(P.Price)
            platinumreturn.InnerHtml = String.Format("{0} Return on Investment", mod_main.converttobtc(P.Price) * 2)
            'silver
            Dim S As Fastcyclepro.Rate = (New Cls_rates).SelectThisID("S")
            Silverrate.InnerHtml = String.Format("{0}{1}", S.Percentage, "%")
            Silverprice.InnerHtml = String.Format("${0}", FormatNumber(S.Price, 0))
            silverbtc.InnerHtml = mod_main.converttobtc(S.Price)
            silverreturn.InnerHtml = String.Format("{0} Return on Investment", mod_main.converttobtc(S.Price) * 2)
            'gold
            Dim G As Fastcyclepro.Rate = (New Cls_rates).SelectThisID("G")
            goldrate.InnerHtml = String.Format("{0}{1}", G.Percentage, "%")
            goldprice.InnerHtml = String.Format("${0}", FormatNumber(G.Price, 0))
            goldbtc.InnerHtml = mod_main.converttobtc(G.Price)
            goldreturn.InnerHtml = String.Format("{0} Return on Investment", mod_main.converttobtc(G.Price) * 2)

        


        End If
    End Sub
End Class
