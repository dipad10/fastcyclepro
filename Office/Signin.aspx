﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Signin.aspx.vb" Inherits="Office_Signin" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<!DOCTYPE html>

<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/vendor.min.css">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/plugins.min.css">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/pratham.min.css">
    <title>Fastcyclepro | Earn with Pride</title>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body style=" background: url(/assets/business1.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
    <form runat="server" id="form1">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="prtm-wrapper">
            <div class="prtm-main">
              <%--  <div class="login-banner">
                    <img src="/office/assets/img/login-banner.jpg" alt="login banner" class="img-responsive" width="1920" height="246">
                </div>--%>
                <div class="login-form-wrapper mrgn-b-lg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-5 center-block">
                                <div class="prtm-form-block prtm-full-block overflow-wrappper">
                                    <div class="login-bar">
                                        <img src="/office/assets/img/login-bars.png" class="img-responsive" alt="login bar" width="743" height="7">
                                    </div>
                                    <div class="prtm-block-title text-center">
                                        <div class="mrgn-b-lg">
                                            <a href="javascript:;">
                                                <img src="/assets/img/Fastcyclepro.png" alt="login logo" class="img-responsive display-ib" width="218" height="23">
                                            </a>
                                        </div>
                                        <div class="login-top mrgn-b-lg">
                                            <div class="mrgn-b-md">
                                                <h2 class="text-capitalize base-dark font-2x fw-normal">Login</h2>
                                            </div>
                                            <p>Sign In and see what we have for you Today.</p>
                                        </div>
                                    </div>
                                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <uc1:msgbox1 ID="msgbox1" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="prtm-block-content">
                                       
                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Your Username" CssClass="form-control" ID="txtusername" runat="server"></asp:TextBox>
                                                
                                                <span class="glyphicon glyphicon-user form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                             <asp:TextBox placeholder="Your Password" aria-describedby="user-pwd" CssClass="form-control" TextMode="Password" ID="txtpassword" runat="server"></asp:TextBox>

                                               
                                                <span class="glyphicon glyphicon-lock form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                          <div class="form-group has-feedback">
                                              <p class="font-xs fw-semibold">Select Login Mode:</p>
                                              <asp:RadioButton ID="RadioButton1" Checked="true" CssClass="font-xs" TextAlign="Right" Text="Matrix Cycler" GroupName="select" runat="server" />
                                               <asp:RadioButton ID="RadioButton2" TextAlign="Right" CssClass="font-xs" Text="Bitcoin Cloud Mining" GroupName="select" runat="server" />
                                            </div>
                                          <div class="login-meta mrgn-b-lg">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                  <div class="g-recaptcha" data-sitekey="6LcflCcUAAAAABU6bkwRlNIeZZiBpbTrVOJSdTKP"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="login-meta mrgn-b-lg">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"><span class="text-capitalize">Remember me</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 text-right"><a href="/office/forgot_pwd.aspx" class="text-primary font-xs password-style">Forget Password ?</a> </div>
                                                </div>
                                            </div>
                                            <div class="mrgn-b-lg">
                                         
                                                <asp:Button ID="btnsignin" OnClick="btnsignin_Click" runat="server" CssClass="btn btn-success btn-block font-2x" Text="Sign In" />
                                             
                                            </div>
                                            <div class="text-center">
                                                <h5 class="base-dark">Don't have an account ? <a href="/office/signup.aspx" class="text-primary">Sign Up</a></h5>
                                            </div>
                                            <div class="text-center"><a class="back-home-btn" href="/default.aspx"><i class="fa fa-long-arrow-left mrgn-r-xs"></i>Back To Home</a> </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="/office/assets/js/vendor.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/plugins.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/pratham.min.js" type="text/javascript"></script>

    
</body>

</html>
