﻿
Partial Class Office_Activation
    Inherits System.Web.UI.Page

    Protected Sub btnlogin_Click(sender As Object, e As EventArgs)
        Response.Redirect("http://fastcyclepro.com/office/signin.aspx")

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim code As String = Request.QueryString("Acode")

        If code <> "" Then


            Try
                'if activation code is not empty update active to 1
                Dim A As New Cls_Registration
                Dim rec As Fastcyclepro.Registration = A.SelectThisguid(code)
                'check if the user is not actvated before so it wont keep send mails over and over again wen d idiot refresh d page
                If rec.Active = 0 Then
                    rec.Active = 1

                    Dim res As ResponseInfo = A.Update(rec)
                    If res.ErrorCode = 0 Then
                        Dim body As String = mod_main.PopulateBody(rec.Name, rec.UserName, rec.Password, rec.Email)
                        Call mod_main.SendHtmlFormattedEmail(rec.Email, "", "Welcome To Fastcyclepro", body)
                        panelconfirmation.Visible = True

                    Else
                        msgbox1.ShowError(res.ErrorCode & " " & res.ExtraMessage)
                    End If



                    'session the donation id of the donation so he/she can go straight to the dashboard to get a sponsor

                Else
                    'tell the user ur account has been activated already. dont send any mail again
                    msgbox1.ShowHelp("Your Account has been Activated Already, Please Login http://fastcyclepro.com/office/signin")
                End If


            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
                Exit Sub
            End Try

        Else
            Response.Redirect("/office/signin.aspx")

            Exit Sub

        End If


    End Sub
End Class
