﻿
Partial Class Office_Web_Admin_blog_edit
    Inherits System.Web.UI.Page

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        If txtfdesc.Text = "" Then
            msgbox1.ShowError("description is required")
            Exit Sub

        End If

        If txttitle.Text = "" Then
            msgbox1.ShowError("Title is required")
            Exit Sub

        End If

        Dim serial As String = Request.QueryString("edit-id")
        Dim a As New cls_blog
        If serial <> "" Then
            'update

            Dim rec As Fastcyclepro.Blog = a.SelectThisID(serial)
            rec.Title = txttitle.Text
            rec.Fdesc = txtfdesc.Text
            Dim res As ResponseInfo = a.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Updated Successfully');window.location='blog.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage)
            End If

        Else
            'create new instance
            Dim rec As New Fastcyclepro.Blog
            rec.Title = txttitle.Text
            rec.Fdesc = txtfdesc.Text
            rec.Submittedon = Date.Now
            Dim res As ResponseInfo = a.Insert(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Saved Successfully');window.location='blog.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage)
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim serial As String = Request.QueryString("edit-id")
            If serial <> "" Then
                Dim rec As Fastcyclepro.Blog = (New cls_blog).SelectThisID(serial)
                txttitle.Text = rec.Title
                txtfdesc.Text = rec.Fdesc

            End If
        End If
    End Sub
End Class
