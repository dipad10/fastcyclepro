﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="tickets_edit.aspx.vb" Inherits="Office_Web_Admin_tickets_edit" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>View Ticket</h3>

        </header>

        <div class="mail-style prtm-block">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="compose">
                        <div class="compose-header">
                        
                            <div class="compose-field">
                                <div class="compose-field-body">
                                     <div class="selectbox">
                                           <asp:DropDownList ID="ddltitle" CssClass="compose-input" runat="server">
                                        <asp:ListItem Selected="True" Value="null">Add a Subject</asp:ListItem>
                                        <asp:ListItem>Irregular Activities</asp:ListItem>
                                         <asp:ListItem>Account Issue</asp:ListItem>
                                         <asp:ListItem>Report A Member</asp:ListItem>
                                         <asp:ListItem>Suggestions to make Fastcyclepro Better</asp:ListItem>
                                    </asp:DropDownList>
                                     </div>
                                  
                                   
                                </div>
                            </div>
                             <div class="compose-field">
                                <div class="compose-field-body">
                                    <p>User Email to reply-to</p>
                                    <asp:TextBox ID="txtemail" CssClass="form-control" runat="server"></asp:TextBox>
                                   
                                </div>
                            </div>
                            <div class="compose-field">
                                <div class="compose-field-body">
                                    <asp:Image ID="Image1" Width="600" Height="400" runat="server" />
                                     
                                   
                                </div>
                            </div>
                        </div>
                        <div class="compose-body">
                            <div class="compose-message pad-all-xs">
                                <div id="alerts"></div>
                               
                                
                                    <asp:TextBox ID="editor" placeholder="Enter Message" Width="100%" Height="400px" TextMode="MultiLine" CssClass="prtm-editor" runat="server"></asp:TextBox>
                               
                            </div>
                            <div class="compose-actions">
                                <button runat="server" id="btndiscard" class="btn btn-inverse btn-lg" OnClientClick="history.go(-1)" type="button"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Discard</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

