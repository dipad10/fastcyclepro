﻿
Partial Class Office_Web_Admin_Rates
    Inherits System.Web.UI.Page

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim planid As String = Request.QueryString("edit-id")
        If planid <> "" Then
            Dim a As New Cls_rates
            Dim rec As Fastcyclepro.Rate = a.SelectThisID(planid)
            rec.Percentage = ratepercentage.Text
            rec.Price = rateprice.Text
            Dim res As ResponseInfo = a.Update(rec)
            If res.ErrorCode = 0 Then
                Dim b As New Cls_Plans
                Dim record As Fastcyclepro.Plan = b.SelectThisID(planid)
                record.PlanAmt = rateprice.Text
                Dim res2 As ResponseInfo = b.Update(record)
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Rate Updated Successfully');window.location='rates.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage)
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim planid As String = Request.QueryString("edit-id")
            If planid <> "" Then
                Dim rec As Fastcyclepro.Rate = (New Cls_rates).SelectThisID(planid)
                ratepercentage.Text = rec.Percentage
                rateprice.Text = rec.Price
                Select Case planid
                    Case "A"
                        planname.InnerHtml = "Update Alpha Plan Rate"
                    Case "B"
                        planname.InnerHtml = "Update Beta Plan Rate"

                    Case "P"
                        planname.InnerHtml = "Update Platinum Plan Rate"

                    Case "S"
                        planname.InnerHtml = "Update Silver Plan Rate"

                    Case "G"
                        planname.InnerHtml = "Update Gold Plan Rate"

                End Select
            End If
        End If
    End Sub
End Class
