﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Tickets.aspx.vb" Inherits="Office_Web_Admin_Tickets" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Tickets</h3>

        </header>

        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">My Tickets</h4>
                <hr />

                <dx:ASPxGridView ID="ASPxGridView1" Styles-Cell-HorizontalAlign="Center" Styles-Header-HorizontalAlign="Center" Width="100%" Styles-Header-ForeColor="White" Styles-Header-Font-Bold="true" Styles-SelectedRow-CssClass="bg-danger" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" DataSourceID="SqlDataSource1">


                    <SettingsPager PageSize="7"></SettingsPager>

                    <Settings ShowFilterBar="Auto" />
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Visible="true" FieldName="SN" Caption="#TicketID" ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="UserName" Caption="Creator" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="TicketTitle" Caption="Summary" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Message" Caption="Description" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                       
                        
                         <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Status" Caption="Status" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>

                       <dx:GridViewDataTextColumn CellStyle-CssClass="font-sm text-secondary" Caption="Action" VisibleIndex="7">
                            <CellStyle CssClass="font-sm"></CellStyle>
                            <DataItemTemplate>
                            <a href="/office/web/admin/tickets_edit.aspx?edit-id=<%#Eval("SN")%>" class=""><i class="fa fa-eye"></i></a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy HH:mm:ss" CellStyle-CssClass="font-sm" Caption="Date Registered" FieldName="Submittedon" VisibleIndex="4">


                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataDateColumn>
                       

                           

                    </Columns>

                    <Styles>
                        <Header Font-Bold="True"></Header>

                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                    </Styles>
                </dx:ASPxGridView>



                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="GetUserTicketsadmin" SelectCommandType="StoredProcedure">
                  
                </asp:SqlDataSource>



                <br />
                <asp:Button ID="btnapprove" OnClick="btnapprove_Click" CssClass="btn btn-primary" runat="server" Text="Approve" />

            </div>
        </section>
    </form>
</asp:Content>

