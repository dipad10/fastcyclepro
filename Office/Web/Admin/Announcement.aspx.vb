﻿
Partial Class Office_Web_Admin_Announcement
    Inherits System.Web.UI.Page

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim sn As String = Request.QueryString("edit-id")
        If sn <> "" Then
            'update
            Dim A As New cls_announcements
            Dim rec As Fastcyclepro.Announcement = A.SelectThisID(sn)

            rec.Announcement = txtmessage.Text
            rec.submittedby = Session("username")
            rec.submittedon = Date.Now
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Saved Successfully');window.location='Announcement.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If

        Else
            'create
            Dim A As New cls_announcements
            Dim rec As New Fastcyclepro.Announcement
            rec.Announcement = txtmessage.Text
            rec.submittedby = Session("username")
            rec.submittedon = Date.Now
            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Saved Successfully');window.location='Announcement.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sn As String = Request.QueryString("edit-id")
            If sn <> "" Then
                Dim rec As Fastcyclepro.Announcement = (New cls_announcements).SelectThisID(sn)
                txtmessage.Text = rec.Announcement

            End If

        End If
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("SN")(i).ToString
                Dim A As New cls_announcements
                Dim rec As Fastcyclepro.Announcement = A.SelectThisID(sn)

                Dim res As ResponseInfo = A.Delete(rec)

                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Deleted Successfully');window.location='Announcement.aspx';", True)

                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub
End Class
