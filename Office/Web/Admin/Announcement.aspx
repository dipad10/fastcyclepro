﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Announcement.aspx.vb" Inherits="Office_Web_Admin_Announcement" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Annoucements</h3>

        </header>


        <section class="panel">
            <div class="panel-body">
              

                <div class="row">
                    <div class="col-md-12">
                        <div class="prtm-block">
                            <div class="prtm-block-title mrgn-b-lg">
                                <h3 class="text-capitalize">Add Information</h3>
                            </div>
                            <div class="form-elments1">
                                <div class="form-group">
                                    <label for="auto-field">SN</label>
                                    <input class="form-control" id="auto-field" placeholder="AUTO" disabled="" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="msg">Announcement</label>

                                    <asp:TextBox ID="txtmessage" CssClass="form-control"  TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                               <br />
                                   <asp:Button ID="btnsave" OnClick="btnsave_Click" CssClass="btn btn-primary" runat="server" Text="Save" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </section>

         <section class="panel">
            <div class="panel-body">
              

                <div class="row">
                    <div class="col-md-12">
                        <div class="prtm-block">
                        <dx:ASPxGridView ID="ASPxGridView1" Styles-Cell-HorizontalAlign="Center" Styles-Header-HorizontalAlign="Center" Width="100%" Styles-Header-ForeColor="White" Styles-Header-Font-Bold="true" Styles-SelectedRow-CssClass="bg-danger" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" DataSourceID="SqlDataSource1">


                    <SettingsPager PageSize="7"></SettingsPager>

                    <Settings ShowFilterBar="Auto" />
                    <Columns>
                           <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="SN" ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Announcement" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy HH:mm:ss" CellStyle-CssClass="font-sm" FieldName="submittedon" VisibleIndex="2">


                        </dx:GridViewDataDateColumn>
                      

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-sm text-secondary" VisibleIndex="3" FieldName="submittedby">
                        </dx:GridViewDataTextColumn>

                           <dx:GridViewDataTextColumn CellStyle-CssClass="font-sm text-secondary" Caption="Action" VisibleIndex="8">
                            <CellStyle CssClass="font-sm"></CellStyle>
                            <DataItemTemplate>
                                <a href="/office/web/Admin/Announcement.aspx?edit-id=<%#Eval("SN")%>" class=""><i class="fa fa-eye"></i></a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>

                    <Styles>
                        <Header Font-Bold="True"></Header>

                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                        <Cell HorizontalAlign="Center"></Cell>

                    </Styles>
                </dx:ASPxGridView>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="SELECT * FROM [Announcement] ORDER BY [SN] DESC"></asp:SqlDataSource>

                            <br />
                             <asp:Button ID="btndelete" OnClick="btndelete_Click" CssClass="btn btn-primary" runat="server" Text="Delete" />
                        </div>
                    </div>
                </div>

            </div>


        </section>
    </form>


</asp:Content>

