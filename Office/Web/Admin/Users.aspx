﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Users.aspx.vb" Inherits="Office_Web_Admin_Users" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Users</h3>

        </header>

        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">All Users</h4>
                <hr />
                <asp:Panel runat="server" ID="panel1" ScrollBars="Horizontal" Width="100%">
                    <dx:ASPxGridView ID="ASPxGridView1" CssClass="font-xs" Styles-Cell-HorizontalAlign="Center" Styles-Header-HorizontalAlign="Center" Width="100%" Styles-Header-ForeColor="White" Styles-Header-Font-Bold="true" Styles-SelectedRow-CssClass="bg-danger" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" DataSourceID="SqlDataSource1">


                    <SettingsPager PageSize="50"></SettingsPager>

                    <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Visible="false" FieldName="SN" ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="UserID" Caption="#UserID" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="UserName" Caption="Username" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Name" Caption="Fullname" VisibleIndex="2">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Gender" Caption="Gender" VisibleIndex="3">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>

                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Country" Caption="Country" VisibleIndex="4">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Email" Caption="Email" VisibleIndex="5">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Sponsor" Caption="Sponsor" VisibleIndex="5">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>

                       
                       
                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Permission" Caption="Permission" VisibleIndex="6">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy HH:mm:ss" CellStyle-CssClass="font-sm" Caption="SubmittedOn" FieldName="CreatedOn" VisibleIndex="7">


                            <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy HH:mm:ss"></PropertiesDateEdit>


                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Active" Caption="Active" VisibleIndex="8">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-sm text-secondary" Caption="Action" VisibleIndex="9">
                            <CellStyle CssClass="font-sm"></CellStyle>
                            <DataItemTemplate>
                                <a href="/office/web/Admin/users_edit?edit-id=<%#Eval("UserID")%>" class=""><i class="fa fa-eye"></i></a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>


                    </Columns>

                    <Styles>
                        <Header Font-Bold="True"></Header>

                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                        <Cell HorizontalAlign="Center"></Cell>

                    </Styles>
                </dx:ASPxGridView>
                </asp:Panel>
                

                  <dx:ASPxGridViewExporter ExportedRowType="Selected"  GridViewID="ASPxGridView1" ID="exportGrid" runat="server">
  </dx:ASPxGridViewExporter>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="GetallUsers" SelectCommandType="StoredProcedure">
                  
                </asp:SqlDataSource>



                <br />
                                  <asp:Button ID="btnnew" OnClick="btnnew_Click" CssClass="btn btn-primary" runat="server" Text="New User" />
                                                  <asp:Button ID="btnsuspend" OnClick="btnsuspend_Click" CssClass="btn btn-primary" runat="server" Text="Suspend User" />
                                                                  <asp:Button ID="btnunsuspend" OnClick="btnunsuspend_Click" CssClass="btn btn-primary" runat="server" Text="Unsuspend User" />

                                         <asp:Button ID="btnmail" OnClick="btnmail_Click" CssClass="btn btn-primary" runat="server" Text="Send User personal mail" />
                                                                  <asp:Button ID="btnexport" OnClick="btnexport_Click" CssClass="btn btn-primary" runat="server" Text="Export to Excel" />

                      <asp:Button ID="btndelete" OnClick="btndelete_Click" CssClass="btn btn-primary" runat="server" Text="Delete User" />


            </div>
        </section>
    </form>
</asp:Content>

