﻿
Partial Class Office_Web_Admin_Tickets
    Inherits System.Web.UI.Page

    Protected Sub btnapprove_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1

                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("SN")(i).ToString
                Dim A As New Cls_Tickets
                Dim rec As Fastcyclepro.Ticket = A.SelectThisID(sn)

                rec.Status = "APPROVED"

                Dim res As ResponseInfo = A.Update(rec)
                If res.ErrorCode = 0 Then
                    'send mail to referer stating that he has been paid
                    'Dim refusername As String = ASPxGridView1.GetSelectedFieldValues("Referer")(i).ToString
                    'Dim ref As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(refusername)
                    'Dim body As String = PopulateBodyreferrerbonusalert(ref.UserName, "$" & FormatNumber(rec.Bonus, 0), ref.BitcoinwalletAddress, rec.Referee)
                    'mod_main.SendHtmlFormattedEmail(ref.Email, "", "Fastcyclepro Referral Bonus!", body)
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Ticket Approved Successfully');window.location='tickets.aspx';", True)
                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub
End Class
