﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="blog_edit.aspx.vb" Inherits="Office_Web_Admin_blog_edit" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Add posts</h3>

        </header>


        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">Post Edit</h4>
                <hr />


                <div class="row">
                    <div class="col-md-12">
                        <div class="prtm-block">
                            <div class="horizontal-form-style">

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <label for="inputname3" class="col-sm-2 control-label">Post Title</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox ID="txttitle" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Full Description</label>
                                            <div class="col-sm-10">
                                                <asp:TextBox placeholder="description" TextMode="MultiLine" CssClass="form-control" ID="txtfdesc" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                  

                                    <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <asp:Button ID="btnsave" OnClick="btnsave_Click" CssClass="btn btn-rounded btn-primary" runat="server" Text="Save" />
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </form>
</asp:Content>

