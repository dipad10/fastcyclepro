﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Blog.aspx.vb" Inherits="Office_Web_Admin_Blog" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Blog Posts</h3>

        </header>

        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">All Posts</h4>
                <hr />
                <asp:Panel runat="server" ID="panel1" ScrollBars="Horizontal" Width="100%">
                    <dx:ASPxGridView ID="ASPxGridView1" Styles-Cell-HorizontalAlign="Center" Styles-Header-HorizontalAlign="Center" Width="100%" Styles-Header-ForeColor="White" Styles-Header-Font-Bold="true" Styles-SelectedRow-CssClass="bg-danger" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" DataSourceID="SqlDataSource1">


                    <SettingsPager PageSize="7"></SettingsPager>

                    <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Visible="false" FieldName="SN" ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Title" Caption="Title" VisibleIndex="1">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Fdesc" Caption="Full Description" VisibleIndex="2">
                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataTextColumn>
                       
                    
                       
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy HH:mm:ss" CellStyle-CssClass="font-sm" Caption="SubmittedOn" FieldName="Submittedon" VisibleIndex="4">


                            <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy HH:mm:ss"></PropertiesDateEdit>


                            <CellStyle CssClass="font-sm"></CellStyle>
                        </dx:GridViewDataDateColumn>
                      

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-sm text-secondary" Caption="Action" VisibleIndex="8">
                            <CellStyle CssClass="font-sm"></CellStyle>
                            <DataItemTemplate>
                                <a href="/office/web/Admin/blog_edit?edit-id=<%#Eval("SN")%>" class=""><i class="fa fa-eye"></i></a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>


                    </Columns>

                    <Styles>
                        <Header Font-Bold="True"></Header>

                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                        <Cell HorizontalAlign="Center"></Cell>

                    </Styles>
                </dx:ASPxGridView>
                </asp:Panel>
                



                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="Getblog" SelectCommandType="StoredProcedure">
                  
                </asp:SqlDataSource>



                <br />
                                  <asp:Button ID="btnnew" OnClick="btnnew_Click" CssClass="btn btn-primary" runat="server" Text="New Post" />
                                                                  <asp:Button ID="btndelete" OnClick="btndelete_Click" CssClass="btn btn-primary" runat="server" Text="Delete User" />


            </div>
        </section>
    </form>
</asp:Content>

