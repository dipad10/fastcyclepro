﻿
Partial Class Office_Web_Admin_MasterMail
    Inherits System.Web.UI.Page

    Protected Sub btnsend_Click(sender As Object, e As EventArgs)
        If txtmailmessage.Text = "" Then
            msgbox1.ShowError("insert mail message")
            Exit Sub
        End If

        If txtsubject.Text = "" Then
            msgbox1.ShowError("insert mail subject")
            Exit Sub
        End If
        'this checks if the A query string is not empty. if its not empty, it means the mail is to be sent as a reminder to one particular user.
 
        Dim type As String = Request.QueryString("type")
        Select Case type
            Case "PM"
                Dim count As Integer = 0
                'means send PM to invite new people by mail. here you will load all mails seperated by comma
                Dim CustDetailsList = Split(txtemails.Text, ",").ToList
                For Each email As String In CustDetailsList
                    count += 1
                    Dim body As String = mod_main.PopulateBodymastermail("There", txtsubject.Text, txtmailmessage.Text)
                    If mod_main.SendHtmlFormattedEmail(email, "", txtsubject.Text, body) = True Then
                        msgbox1.Showsuccess("Mail sent successfully to " & count & " People")
                    Else
                        msgbox1.ShowError("An error occured, mail not sent")
                    End If
                Next
            Case "Direct"
                'send personal mail
                Dim user As String = Request.QueryString("username")
                Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(user)
                Dim body As String = mod_main.PopulateBodymastermail(rec.Name, txtsubject.Text, txtmailmessage.Text)
                If mod_main.SendHtmlFormattedEmail(rec.Email, "", txtsubject.Text, body) = True Then
                    msgbox1.Showsuccess("Mail sent successfully")
                Else
                    msgbox1.ShowError("An error occured, mail not sent")
                End If
            Case Else
                'send global message to all fcp paticipants
                Select Case ddlcountry.SelectedValue
                    Case "all"
                        'send mail accross all countries
                        Dim count As Integer = 0
                        Dim rec As List(Of fastcyclepro.Registration) = (New Cls_Registration).SelectAllUsers
                        For Each userseen In rec
                            count += 1
                            Dim body As String = mod_main.PopulateBodymastermail(userseen.Name, txtsubject.Text, txtmailmessage.Text)
                            If mod_main.SendHtmlFormattedEmail(userseen.Email, "", txtsubject.Text, body) = True Then
                                msgbox1.Showsuccess("Mail sent successfully to " & count & " People")
                            Else
                                msgbox1.ShowError("An error occured, mail not sent")
                            End If

                        Next
                    Case Else
                        'send the mil to the target country only
                        Dim count As Integer = 0
                        Dim rec As List(Of fastcyclepro.Registration) = (New Cls_Registration).SelectAllUserscountry(ddlcountry.SelectedItem.Text)
                        For Each userseen In rec
                            count += 1
                            Dim body As String = mod_main.PopulateBodymastermail(userseen.Name, txtsubject.Text, txtmailmessage.Text)
                            If mod_main.SendHtmlFormattedEmail(userseen.Email, "", txtsubject.Text, body) = True Then
                                msgbox1.Showsuccess("Mail sent successfully to " & count & " People")
                            Else
                                msgbox1.ShowError("An error occured, mail not sent")
                            End If

                        Next
                End Select
              
        End Select
          



    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
           
            Dim type As String = Request.QueryString("type")
            Select Case type
                Case "PM"
                    'means its PM personal mail message to invite new people
                    topic.InnerHtml = "Send Personal messages to invite new people. insert all email addresses below"
                    lbladdemails.Visible = True
                    txtemails.Visible = True
                    txtsubject.Visible = True
                Case "Direct"
                    Dim user As String = Request.QueryString("username")
                    topic.InnerHtml = "Send direct message to " & user & ""
                    lbladdemails.Visible = True

                    txtsubject.Visible = True
                    txtemails.Visible = False
                Case Else
                    topic.InnerHtml = "Send global message to all Fastcyclepro members."
                    ddlcountry.Visible = True
            End Select
             

            End If

    End Sub
End Class
