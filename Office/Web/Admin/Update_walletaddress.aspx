﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Update_walletaddress.aspx.vb" Inherits="Office_Web_Admin_Update_walletaddress" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Update Wallet Address</h3>

        </header>


        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">Bitcoin address</h4>
                <hr />


                <div class="row">
                    <div class="col-md-12">
                        <div class="prtm-block">
                            <div class="horizontal-form-style">

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="row mrgn-all-none">
                                         
                                            <div class="col-sm-10">
                                                   <span class="">Bitcoin Address: <asp:TextBox ID="txtbitcoinaddress" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox></span> 

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <div class="col-sm-10">
                                                <asp:Button ID="btnupdate" OnClick="btnupdate_Click" Enabled="false" CssClass="btn btn-rounded btn-primary" runat="server" Text="Update Bitcoin Address" />
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <div class="col-sm-5">
                                                <asp:TextBox placeholder="Enter Pin" ID="txtpin" Enabled="true" TextMode="Password" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="row mrgn-all-none">
                                            <div class="col-sm-5">
                                                <asp:Button ID="btnenterpin" OnClick="btnenterpin_Click" CssClass="btn btn-rounded btn-primary" runat="server" Text="Enter" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </form>
</asp:Content>


