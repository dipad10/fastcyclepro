﻿Imports DevExpress.Web

Partial Class Office_Web_Admin_Users_edit
    Inherits System.Web.UI.Page

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            mod_filldropdowns.fillusers(Me.ddlsponsor)
            Dim userid As String = Request.QueryString("edit-id")
            If userid <> "" Then
                Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUserID(userid)
                txtfullname.Text = rec.Name
                txtemail.Text = rec.Email
                txtpassword.Text = rec.Password
                txtphonenumber.Text = rec.MobileNo1
                txtusername.Text = rec.UserName
                txtwalletaddress.Text = rec.BitcoinwalletAddress
                ddlgender.SelectedValue = rec.Gender
                ddlcountry.SelectedItem.Text = rec.Country
                ddlpermission.SelectedValue = rec.Permission
                Image1.ImageUrl = rec.Profilepicpath
                Image1.Visible = True
                ddlactive.SelectedValue = rec.Active

                ddlsponsor.Text = rec.Sponsor
            End If
        End If
    End Sub

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim use As String = Request.QueryString("edit-id")
        Dim A As New Cls_Registration
        If use <> "" Then
            'update
            Dim rec As Fastcyclepro.Registration = A.SelectThisUserID(use)
            rec.Name = txtfullname.Text
            rec.Gender = ddlgender.SelectedValue
            rec.Country = ddlcountry.SelectedItem.Text
            rec.BitcoinwalletAddress = txtwalletaddress.Text
            rec.MobileNo1 = txtphonenumber.Text
            rec.UserName = txtusername.Text
            rec.Password = txtpassword.Text
            rec.Permission = ddlpermission.SelectedValue
            rec.Email = txtemail.Text
            rec.Active = ddlactive.SelectedValue
            rec.Sponsor = ddlsponsor.Value
            rec.State = String.Format("/countryflag/{0}.png", ddlcountry.SelectedValue)
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(Request.PhysicalApplicationPath + "/Office/assets/avatars/" + file.FileName)
                    Dim bb As String = "/Office/assets/avatars/" + file.FileName
                    rec.Profilepicpath = bb
                    rec.Profilepic = file.FileName


                Next


            End If

            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                'update referra record too
                Dim b As New cls_referal
                Dim record As Fastcyclepro.Referal = b.SelectThisreferee(rec.UserName)
                record.Referer = rec.Sponsor
                Dim res2 As ResponseInfo = b.Update(record)
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='users.aspx';", True)

            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If

        Else
            'create new instance
            'validate

            Dim user As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(txtusername.Text)
            If user.UserName Is Nothing Then

            Else
                msgbox1.ShowError("This User already exists. Please Login or Use another username")
                Exit Sub

            End If


            Dim numbertype As String = "UserID"
            Dim auto As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype)
            Dim userid As String = String.Format("USER/{0}/{1}", Date.Now.Year, auto.Nextvalue)
            Call mod_main.Updateautonumber(numbertype)
            Dim rec As New Fastcyclepro.Registration
            rec.UserID = userid
            rec.Name = txtfullname.Text
            rec.Gender = ddlgender.SelectedValue
            If ddlgender.SelectedValue = "Male" Then
                rec.Profilepicpath = "/office/assets/avatars/maleavatar.png"
                rec.Profilepic = "maleavatar.png"
            Else
                rec.Profilepicpath = "/office/assets/avatars/femaleavatar.png"
                rec.Profilepic = "femaleavatar.png"
            End If
            rec.Country = ddlcountry.SelectedItem.Text
            rec.State = String.Format("/countryflag/{0}.png", ddlcountry.SelectedValue)
            rec.BitcoinwalletAddress = txtwalletaddress.Text
            rec.MobileNo1 = txtphonenumber.Text
            rec.UserName = txtusername.Text
            rec.Password = txtpassword.Text
            rec.Active = 1
            rec.Activationcode = Guid.NewGuid.ToString
            rec.CreatedOn = DateTime.Now.ToString
            rec.Field9 = 1
            rec.Active = ddlactive.SelectedValue
            rec.Email = txtemail.Text
            rec.Permission = ddlpermission.SelectedValue
            rec.Sponsor = ddlsponsor.Value
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(Request.PhysicalApplicationPath + "/Office/assets/avatars/" + file.FileName)
                    Dim bb As String = "/Office/assets/avatars/" + file.FileName
                    rec.Profilepicpath = bb
                    rec.Profilepic = file.FileName

                Next

            End If

            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then
                Dim b As New cls_referal
                Dim record As New Fastcyclepro.Referal
                Dim numbertype2 As String = "refid"
                Dim auto2 As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype2)
                Dim refid As String = String.Format("REF/{0}/{1}", Date.Now.Year, auto2.Nextvalue)
                Call mod_main.Updateautonumber(numbertype2)

                record.RefID = refid
                record.Referer = rec.Sponsor
                record.Referee = rec.UserName
                record.Active = 1
                record.Status = "ACTIVE"
                record.Bitcoinamount = 0
                record.Bonus = 0
                record.submittedon = rec.CreatedOn
                record.Field2 = rec.Email
                Dim res2 As ResponseInfo = b.Insert(record)
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved Successfully');window.location='users.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        End If
    End Sub
End Class
