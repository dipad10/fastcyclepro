﻿Imports DevExpress.Web
Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports DevExpress.Web.Export
Partial Class Office_Web_Admin_Users
    Inherits System.Web.UI.Page

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/office/web/Admin/users_edit.aspx")
    End Sub

    Protected Sub btnsuspend_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1

                Dim userid As String = ASPxGridView1.GetSelectedFieldValues("UserID")(i).ToString
                Dim A As New Cls_Registration
                Dim rec As fastcyclepro.Registration = A.SelectThisUserID(userid)

                rec.Field9 = 0

                Dim res As ResponseInfo = A.Update(rec)
                If res.ErrorCode = 0 Then
                    'send mail to referer stating that he has been paid
                    'Dim refusername As String = ASPxGridView1.GetSelectedFieldValues("Referer")(i).ToString
                    'Dim ref As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(refusername)
                    'Dim body As String = PopulateBodyreferrerbonusalert(ref.UserName, "$" & FormatNumber(rec.Bonus, 0), ref.BitcoinwalletAddress, rec.Referee)
                    'mod_main.SendHtmlFormattedEmail(ref.Email, "", "Fastcyclepro Referral Bonus!", body)
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Account Suspended Successfully');window.location='users.aspx';", True)
                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("UserID")(i).ToString
                Dim A As New Cls_Registration
                Dim rec As fastcyclepro.Registration = A.SelectThisUserID(sn)

                Dim res As ResponseInfo = A.Delete(rec)

                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('User Deleted Successfully');window.location='users.aspx';", True)

                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub

    Protected Sub btnmail_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("UserName")(i).ToString

                Response.Redirect("/office/web/Admin/Mastermail?type=Direct&username=" & sn & "")

            Next

        End If


    End Sub

    Protected Sub btnunsuspend_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1

                Dim userid As String = ASPxGridView1.GetSelectedFieldValues("UserID")(i).ToString
                Dim A As New Cls_Registration
                Dim rec As fastcyclepro.Registration = A.SelectThisUserID(userid)

                rec.Field9 = 1

                Dim res As ResponseInfo = A.Update(rec)
                If res.ErrorCode = 0 Then
                    'send mail to referer stating that he has been paid
                    'Dim refusername As String = ASPxGridView1.GetSelectedFieldValues("Referer")(i).ToString
                    'Dim ref As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(refusername)
                    'Dim body As String = PopulateBodyreferrerbonusalert(ref.UserName, "$" & FormatNumber(rec.Bonus, 0), ref.BitcoinwalletAddress, rec.Referee)
                    'mod_main.SendHtmlFormattedEmail(ref.Email, "", "Fastcyclepro Referral Bonus!", body)
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Account UnSuspended Successfully');window.location='users.aspx';", True)
                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub
    Protected Sub btnexport_Click(sender As Object, e As EventArgs)
        If ASPxGridView1.Selection.Count = 0 Then

            exportGrid.ExportedRowType = GridViewExportedRowType.All
        End If
        exportGrid.WriteXlsxToResponse()
    End Sub
End Class
