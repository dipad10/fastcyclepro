﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="All_Transactions.aspx.vb" Inherits="Office_Web_Admin_All_Transactions" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>All Transactions</h3>

        </header>

        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">All Transactions</h4>
                <hr />
                <asp:Panel runat="server" ID="panel1" ScrollBars="Horizontal" Width="100%">
                    <dx:ASPxGridView ID="ASPxGridView1" Styles-Cell-HorizontalAlign="Center" Styles-Header-HorizontalAlign="Center" Width="100%" Styles-Header-ForeColor="White" Styles-Header-Font-Bold="true" Styles-SelectedRow-CssClass="bg-danger" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" DataSourceID="SqlDataSource1">


                    <SettingsPager PageSize="50"></SettingsPager>

                    <Settings ShowFilterBar="Auto" ShowFilterRow="True" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Visible="false" FieldName="SN" ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="TransactionID" Caption="TransactionID" VisibleIndex="1">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Username" Caption="Username" VisibleIndex="1">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="AccountType" Caption="AccountType" VisibleIndex="2">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Item_name" Caption="Description" VisibleIndex="3">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>

                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Deposit" Caption="Deposit(BTC)" VisibleIndex="4">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Withdrawal" Caption="Withdrawal(BTC)" VisibleIndex="5">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>

                       
                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Total" Caption="Total(BTC)" VisibleIndex="6">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Type" Caption="Type" VisibleIndex="6">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>

                           <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs" FieldName="Statustext" Caption="Status" VisibleIndex="6">
                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy HH:mm:ss" CellStyle-CssClass="font-xs" Caption="Createdon" FieldName="Createdon" VisibleIndex="7">


                            <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy HH:mm:ss"></PropertiesDateEdit>


                            <CellStyle CssClass="font-xs"></CellStyle>
                        </dx:GridViewDataDateColumn>
                   

                        <dx:GridViewDataTextColumn CellStyle-CssClass="font-xs text-secondary" Caption="View" VisibleIndex="9">
                            <CellStyle CssClass="font-xs"></CellStyle>
                            <DataItemTemplate>
                                <a href="/office/web/Admin/all_transactions?edit-id=<%#Eval("SN")%>" class=""><i class="fa fa-eye"></i></a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>


                    </Columns>

                    <Styles>
                        <Header Font-Bold="True"></Header>

                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                        <Cell HorizontalAlign="Center"></Cell>

                    </Styles>
                </dx:ASPxGridView>
                </asp:Panel>
                



                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="Getwallettransactions" SelectCommandType="StoredProcedure">
                  
                </asp:SqlDataSource>



            </div>
        </section>
    </form>
</asp:Content>

