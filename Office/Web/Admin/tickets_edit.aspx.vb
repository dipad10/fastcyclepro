﻿
Partial Class Office_Web_Admin_tickets_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sn As String = Request.QueryString("edit-id")
            If sn <> "" Then
                Dim rec As Fastcyclepro.Ticket = (New Cls_Tickets).SelectThisID(sn)
                editor.Text = rec.Message
                ddltitle.SelectedItem.Text = rec.TicketTitle
                Image1.ImageUrl = rec.Picpath
                Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUserID(rec.UserID)
                txtemail.Text = use.Email

            End If
        End If
    End Sub
End Class
