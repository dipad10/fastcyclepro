﻿
Partial Class Office_Web_Admin_Testimonials
    Inherits System.Web.UI.Page

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("Testimonyid")(i).ToString
                Dim A As New cls_testimonials
                Dim rec As Fastcyclepro.Testimonial = A.SelectThisID(sn)

                Dim res As ResponseInfo = A.Delete(rec)

                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Testimony Deleted Successfully');window.location='Testimonials.aspx';", True)

                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("Testimonyid")(i).ToString
                Dim A As New cls_testimonials
                Dim rec As Fastcyclepro.Testimonial = A.SelectThisID(sn)
                rec.field1 = 1
                Dim res As ResponseInfo = A.Update(rec)

                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Testimony approved Successfully');window.location='Testimonials.aspx';", True)

                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub
End Class
