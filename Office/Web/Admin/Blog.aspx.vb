﻿
Partial Class Office_Web_Admin_Blog
    Inherits System.Web.UI.Page

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/office/web/admin/blog_edit.aspx")
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.msgbox1.ShowError("Please select an item")
            Exit Sub
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim sn As String = ASPxGridView1.GetSelectedFieldValues("SN")(i).ToString
                Dim A As New cls_blog
                Dim rec As Fastcyclepro.Blog = A.SelectThisID(sn)

                Dim res As ResponseInfo = A.Delete(rec)

                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data Deleted Successfully');window.location='blog.aspx';", True)

                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

                End If



            Next

        End If
    End Sub
End Class
