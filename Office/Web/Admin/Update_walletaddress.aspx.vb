﻿
Partial Class Office_Web_Admin_Update_walletaddress
    Inherits System.Web.UI.Page

    Protected Sub btnupdate_Click(sender As Object, e As EventArgs)
        Dim a As New Cls_Registration
        Dim rec As List(Of Fastcyclepro.Registration) = a.Selectalladmin
        For Each record In rec
            record.BitcoinwalletAddress = txtbitcoinaddress.Text
            Dim res As ResponseInfo = a.Update(record)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Wallet address Updated Successfully');window.location='update_walletaddress.aspx';", True)
            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        Next
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).Selecttop1admin
            txtbitcoinaddress.Text = rec.BitcoinwalletAddress
        End If
    End Sub

    Protected Sub btnenterpin_Click(sender As Object, e As EventArgs)
        If txtpin.Text = "" Then
            msgbox1.ShowError("Pls Enter pin")
            Exit Sub
        End If

        If txtpin.Text <> "Balikis1234" Then
            btnupdate.Enabled = False
            txtbitcoinaddress.Enabled = False
            msgbox1.ShowError("Invalid Pin! You don't have the priviledge to do this. Gerrout of Here!")
            Exit Sub
        Else
            btnupdate.Enabled = True
            txtbitcoinaddress.Enabled = True

        End If
    End Sub
End Class
