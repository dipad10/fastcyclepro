﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments
Partial Class Office_Web_Wallet_Add_money
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtpayment.Enabled = False
            'If Session("username") <> "admin" Then
            '    btnfund.Enabled = False

            'End If
        End If
    End Sub

    Protected Sub btnfund_Click(sender As Object, e As EventArgs)
      
        Try

            'If txtamount.Text < 0.005 Then
            '    msgbox1.ShowError("Please Bitcoin Amount cannot be lesser than 0.005 BTC")
            '    Exit Sub

            'End If
            If txtamount.Text = "" Then
                msgbox1.ShowError("Please Enter Bitcoin Amount")
                Exit Sub

            End If
        Catch ex As Exception
            msgbox1.ShowError("Please Insert Figures")
            Exit Sub
        End Try
       
        Dim bitrec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
        If bitrec.BitcoinwalletAddress = "N/A" Then
            msgbox1.ShowError("Please Update your Bitcoin Wallet address before you proceed")
            Exit Sub
        End If

        Session("amount") = txtamount.Text

        Panel1.Visible = False
        Panelconfirm.Visible = True
        'btnconfirm.Visible = True
        Dim amt As Double = Session("amount")

        lblamt.Text = FormatNumber(CDbl(amt), 4)
        lblitem.Text = "Account Funding by " & Session("username") & ""
        lblfee.Text = "0.0000"
        lbltotal.Text = lblamt.Text
        ''To bind confirm payment button to coinpayments.net
        'Dim value As Double = lblamt.Text
        'Me.btcamount = value
        'Me.itemname = lblitem.Text
        'Me.itemnumber = Guid.NewGuid.ToString
        'Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
        'Me.useremail = rec.Email
        ''a malicious user might use a different mail which by the time we validate it it would be wrong from the one in db so we have to use the userid
        'Me.userid = Session("username")
    End Sub

    Protected Property btcamount() As String
        Get
            Return m_InputValue
        End Get
        Set(value As String)
            m_InputValue = value
        End Set
    End Property
    Private m_InputValue As String
    Protected Property itemname() As String
        Get
            Return m_itemname
        End Get
        Set(value As String)
            m_itemname = value
        End Set
    End Property
    Private m_itemname As String

    Protected Property itemnumber() As String
        Get
            Return m_itemnumber
        End Get
        Set(value As String)
            m_itemnumber = value
        End Set
    End Property
    Private m_itemnumber As String
    Protected Property useremail() As String
        Get
            Return m_useremail
        End Get
        Set(value As String)
            m_useremail = value
        End Set
    End Property
    Private m_useremail As String

    Protected Property userid() As String
        Get
            Return m_userid
        End Get
        Set(value As String)
            m_userid = value
        End Set
    End Property
    Private m_userid As String

    Protected Sub btnpay_Click(sender As Object, e As EventArgs)
        Try
            Dim value As Double = lblamt.Text
            lblsendamt.Text = value
            Dim use As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim address As String = createtransactionfcp("create_transaction", "1", value, "BTC", "BTC", use.Email, use.Name, lblitem.Text, Guid.NewGuid.ToString(), "", use.UserName, ConfigurationManager.AppSettings("publickey"))
            lbladdress.Text = address
            Panel1.Visible = False
            Panelconfirm.Visible = False
            Panelpay.Visible = True
        Catch ex As Exception
            msgbox1.ShowError(ex.Message)
        End Try
    End Sub
End Class
