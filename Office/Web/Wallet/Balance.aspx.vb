﻿
Partial Class Office_Web_Wallet_Balance
    Inherits System.Web.UI.Page


    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
    End Sub
    Private totalvalue As Integer = 0
    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        ''Check if the current row is datarow or not
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    'Add the value of column
        '    totalvalue += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Total"))
        'End If
        'If e.Row.RowType = DataControlRowType.Footer Then
        '    'Find the control label in footer 
        '    Dim lblamount As Label = DirectCast(e.Row.FindControl("lblTotalValue"), Label)
        '    'Assign the total value to footer label control
        '    lblamount.Text = totalvalue.ToString()
        'End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'display mesage after payment that he or she will receive a mail soon
            Dim message As String = Request.QueryString("msg")
            If message <> "" Then
                msgbox1.Showsuccess("You will Get a feedback shortly once your payment has been completed!")
            End If
        End If
    End Sub
End Class
