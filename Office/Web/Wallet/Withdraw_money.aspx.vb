﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments
Partial Class Office_Web_Wallet_Withdraw_money
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            bindwalletbalance(Session("username"))
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            txtbitcoinaddress.Text = rec.BitcoinwalletAddress
            If txtbitcoinaddress.Text = "" Or txtbitcoinaddress.Text = "N/A" Then
                btnadd.Visible = True
                txtbitcoinaddress.Visible = False
            End If
            txtbitcoinaddress.Enabled = False
        End If
    End Sub

    Protected Sub bindwalletbalance(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "GetSumWalletbalance"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("totalBAL") IsNot DBNull.Value Then
                        txtbalance.Text = dr("totalBAL")
                        txtbalance.Enabled = False

                    Else
                        txtbalance.Text = "0.0000"


                    End If

                Else

                    txtbalance.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub

   
    Protected Sub btnwithdraw_Click(sender As Object, e As EventArgs)
        If txtamount.Text = "" Then
            msgbox1.ShowError("Please Enter an amount")
            Exit Sub
        End If
        
        If btnadd.Visible = True Then
            'means btcaddress is empty he should go and add it
            msgbox1.ShowError("Please Add a Wallet Address")
            Exit Sub
        End If
        If txtamount.Text > txtbalance.Text Then
            msgbox1.ShowError("Insufficient Balance")
            Exit Sub
        End If
        If txtamount.Text < 0.0002 Then
            msgbox1.ShowError("You can't withdraw bitcoin less than 0.0002 BTC")
            Exit Sub
        End If
        'check if he/she have refered at least 1 person
        Dim A As New fastcyclepro.FastcycleproDataContext
        Dim rec = A.checkrefererblazer(Session("username")).ToList
        If rec.Count = 0 Then
            'check main fcp
            Dim B As New fastcyclepro.FastcycleproDataContext
            Dim rec2 = A.checkrefererfcp(Session("username")).ToList
            If rec2.Count = 0 Then
                msgbox1.ShowError("Make sure you have at least one Referral in Fastcyclepro and has purchased a position before you can withdraw.")
                Exit Sub

            Else
                lbladdress.Text = txtbitcoinaddress.Text
                lblamt.Text = txtamount.Text

                Panel1.Visible = False
                Panelconfirm.Visible = True
            End If
        Else
            lbladdress.Text = txtbitcoinaddress.Text
            lblamt.Text = txtamount.Text

            Panel1.Visible = False
            Panelconfirm.Visible = True
        End If
        'Dim referer As List(Of fastcyclepro.Referal) = (New cls_referal).SelectThisreferer(Session("username"))
        'If referer.Count = 0 Then

        'Else
        '    'now he has refered but check if referee has bought position in blazer
        '    For Each rec In referer


        '        Dim checkblazer As Boolean = (New cls_blazer).SelectThisblazerany(rec.Referee)
        '        Dim checkfcp As Boolean = (New cls_position).SelectThispositionrany(rec.Referee)
        '        If checkblazer = False And checkfcp = False Then

        '            msgbox1.ShowError("Make sure at least one of your downline has purchase a position.")
        '            Exit Sub

        '            'check in main matrix


        '        Else


        '        End If
        '    Next


        'End If



    End Sub

    Protected Sub btnconfirm_Click(sender As Object, e As EventArgs)
        'Dim openWith As New SortedList(Of String, String)

        '' Add some elements to the list. There are no 
        '' duplicate keys, but some of the values are duplicates.
        'openWith.Add("version", "1")
        'openWith.Add("key", ConfigurationManager.AppSettings("publickey"))
        'openWith.Add("cmd", "create_withdrawal")
        'openWith.Add("amount", lblamt.Text)
        'openWith.Add("currency", "BTC")
        'openWith.Add("address", lbladdress.Text)
        'openWith.Add("auto_confirm", "1")
        'openWith.Add("note", txtcomments.Text)



        'commit to database here into wallet table
        Dim A As New cls_wallet
        Dim wallrec As New Fastcyclepro.Wallet
        wallrec.AccountType = "COINPAYMENTS"
        wallrec.amount = lblamt.Text
        wallrec.amount1 = lblamt.Text
        wallrec.amount2 = lblamt.Text
        wallrec.Createdon = Date.Now
        wallrec.Deposit = 0
        wallrec.Withdrawal = lblamt.Text
        wallrec.Username = Session("username")
        wallrec.Type = "WITHDRAWAL"
        wallrec.TransGUID = Guid.NewGuid.ToString
        wallrec.Total = lblamt.Text
        wallrec.Status = "COMPLETED"
        wallrec.TransactionID = POSTWITHDAWAL("create_withdrawal", lblamt.Text, "1", "BTC", lbladdress.Text, "1", txtcomments.Text, ConfigurationManager.AppSettings("publickey"))
        wallrec.Item_name = "Withdrawal by " & Session("username") & ""
        Dim res As ResponseInfo = A.Insert(wallrec)
        If res.ErrorCode = 0 Then

            'send mail to user saying withdraw has been processed
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = PopulateBodyprocessed(rec.Name, Date.Now.ToLongDateString, lblamt.Text & " Btc", lbladdress.Text)
            Call SendHtmlFormattedEmail(rec.Email, "", "Your Withdrawal is being Processed!", body)
            Panel1.Visible = True
            Panelconfirm.Visible = False
            msgbox1.Showsuccess("Withdrawal being Processed shortly...")
        Else
            msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
        End If

    End Sub
End Class
