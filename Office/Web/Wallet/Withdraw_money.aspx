﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Withdraw_money.aspx.vb" Inherits="Office_Web_Wallet_Withdraw_money" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Withdraw Money</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Wallet</a></li>

                <li class="breadcrumb-item active"><a href="#">Withdraw Money</a></li>
            </ul>
        </div>
        
        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Withdraw Money</h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Withdraw Money</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td width="20%">From Balance</td>
                                                <td><span>
                                                    <asp:TextBox ID="txtbalance" CssClass="form-control" runat="server"></asp:TextBox>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Amount to Withdraw</td>
                                                <td><span>
                                                    <asp:TextBox ID="txtamount" onkeypress="return numbersonly(this, event)" CssClass="form-control" Text="" runat="server"></asp:TextBox></span></td>
                                            </tr>
                                             <tr>

                                                <td>Bitcoin Address</td>
                                                <td><span>
                                                    <asp:TextBox ID="txtbitcoinaddress" CssClass="form-control" Text="" runat="server"></asp:TextBox></span> <span><a class="" runat="server" id="btnadd" visible="false" href="/office/web/MemberAccount/Cashout_settings.aspx"><i class="fa fa-plus-square"></i> Add WalletAddress</a> </span></td>
                                            </tr>
                                             <tr>

                                                <td>Add Comments</td>
                                                <td><span>
                                                    <asp:TextBox ID="txtcomments" TextMode="MultiLine" CssClass="form-control" Text="" runat="server"></asp:TextBox></span></td>
                                            </tr>




                                        </tbody>
                                    </table>
                                    <div class="form-group center">
                                        <asp:Button ID="btnwithdraw"  OnClick="btnwithdraw_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Withdraw" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panelconfirm" Visible="false" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Confirm Withdrawal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td width="20%">To Bitcoin Address</td>
                                                <td><span class="fw-bold">
                                                 <asp:Label ID="lbladdress" runat="server" Text="Label"></asp:Label>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Amount</td>
                                                <td><span class="fw-bold">
                                                   <i class="fa fa-bitcoin"></i> <asp:Label ID="lblamt" CssClass="" runat="server" Text="Label"></asp:Label>
                                                </span></td>
                                            </tr>
                                          

                                        </tbody>
                                    </table>
                                     <div class="form-group center">
                                        <asp:Button ID="btnconfirm" OnClick="btnconfirm_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Proceed Withdrawal" />
                                    </div>
                                </div>
                            </asp:Panel>
                            
                            </div>
                    </div>
                </div>

            </div>
        </div>
         </form>
     <script>
         function numbersonly(myfield, e) {
             var key;
             var keychar;

             if (window.event)
                 key = window.event.keyCode;
             else if (e)
                 key = e.which;
             else
                 return true;

             keychar = String.fromCharCode(key);

             // control keys
             if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                 return true;

                 // numbers
             else if ((("0123456789").indexOf(keychar) > -1))
                 return true;

                 // only one decimal point
             else if ((keychar == ".")) {
                 if (myfield.value.indexOf(keychar) > -1)
                     return false;
             }
             else
                 return false;
         }
    </script>

</asp:Content>

