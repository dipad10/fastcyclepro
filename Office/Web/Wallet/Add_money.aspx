﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Add_money.aspx.vb" Inherits="Office_Web_Wallet_Add_money" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Add Money</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Wallet</a></li>

                <li class="breadcrumb-item active"><a href="#">Add Money</a></li>
            </ul>
        </div>
        
        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Fund Account</h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Add Money</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td width="20%">Amount</td>
                                                <td><span>
                                                    <asp:TextBox placeholder="Ex: 0.005, 0.015, 0.030" data-toggle="tooltip" data-placement="top"  data-original-title="Enter the amount you wish to deposit E.g 0.005, 0.015, 0.030." onkeypress="return numberOnly(this, event)" ID="txtamount" CssClass="form-control" runat="server"></asp:TextBox>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Payment Option</td>
                                                <td><span>
                                                    <asp:TextBox ID="txtpayment" CssClass="form-control" Text="Coinpayments" runat="server"></asp:TextBox></span></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <div class="form-group center">
                                        <asp:Button ID="btnfund" OnClick="btnfund_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Fund Account" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panelconfirm" Visible="false" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Confirm Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td width="20%">Item</td>
                                                <td><span>
                                                 <asp:Label ID="lblitem" runat="server" Text="Label"></asp:Label>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Amount</td>
                                                <td><span class="fw-bold">
                                                   <i class="fa fa-bitcoin"></i> <asp:Label ID="lblamt" runat="server" Text="Label"></asp:Label>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Fee</td>
                                                <td><span class="fw-bold">
                                                   <i class="fa fa-bitcoin"></i> <asp:Label ID="lblfee" runat="server" Text="Label"></asp:Label>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td>Total</td>
                                                <td><span class="fw-bold">
                                                   <i class="fa fa-bitcoin"></i> <asp:Label ID="lbltotal" runat="server" Text="Label"></asp:Label></span></td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                      <div class="form-group center">
                                        <asp:Button ID="btnpay" OnClick="btnpay_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Make Payment" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panelpay" Visible="false" runat="server">
                         <span class="font-xs"> <i class="fa fa-info-circle"></i> Send the BTC amount to the address shown below. Once its completed you will get a mail from us regarding this deposit.</span>

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Make Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td class="fw-bold" style="color:#758494;" width="20%">Please Send: </td>
                                                <td class="fw-bold font-2x" style="color:#758494;"><span>
                                                    <asp:label ID="lblsendamt" style="color:#758494;" runat="server"></asp:label> BTC</span></td>
                                            </tr>
                                            <tr>

                                                <td style="color:#758494;" class="fw-bold"><span class="font-xs fw-bold"></span>To address:</td>
                                                <td style="color:#758494;" class="fw-bold"><span> <asp:label ID="lbladdress" CssClass="fw-bold font-2x" runat="server"></asp:label></span>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                  
                                </div>
                                    <div style="text-align:center;" class="form-group center">
                                         <a href="Add_money.aspx" class="btn btn-outline-primary btn-rounded">Mark Payment as complete</a>
                                      
                                    </div>
                            </asp:Panel>
                            </div>
                    </div>
                </div>

            </div>
        </div>
         </form>
    
         <%--<div runat="server" id="btnconfirm" visible="false" style="margin-bottom:40px !important; z-index:9999; text-align:center !Important;" class="form-group">
                               <form action="https://www.coinpayments.net/index.php" method="post">
                <input type="hidden" name="cmd" value="_pay">
                <input type="hidden" name="reset" value="1">
                <input type="hidden" name="merchant" value="f39ec1421ad91f82aade6898a3089c59">
                <input type="hidden" name="want_shipping" value="0">
                <input type="hidden" name="currency" value="BTC">
                <input type="hidden" name="amountf" value="<%= Me.btcamount%>">
                <input type="hidden" name="item_name" value="<%= Me.itemname%>">
                                    <input type="hidden" name="custom" value="<%= Me.userid%>">
                <input type="hidden" name="item_number" value="<%= Me.itemnumber%>">
                                   <input type="hidden" name="email" value="<%= Me.useremail%>">
                <input type="hidden" name="ipn_url" value="http://fastcyclepro.com/Handler.ashx">
                <input type="hidden" name="success_url" value="http://fastcyclepro.com/office/web/wallet/balance.aspx?msg=yes">
                <input type="hidden" name="cancel_url" value="http://fastcyclepro.com/office/web/wallet/Add_money.aspx"><input type="submit" name="submit" class="btn btn-lg btn-primary btn-rounded" value="Make Payment">
                                                        </form>
                                                    </div>--%>
      
                             
       

                       
      <%--<script>
          function numbersonly(myfield, e) {
              var key;
              var keychar;

              if (window.event)
                  key = window.event.keyCode;
              else if (e)
                  key = e.which;
              else
                  return true;

              keychar = String.fromCharCode(key);

              // control keys
              if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                  return true;

                  // numbers
              else if ((("0123456789").indexOf(keychar) > -1))
                  return true;

                  // only one decimal point
              else if ((keychar == ".")) {
                  if (myfield.value.indexOf(keychar) > -1)
                      return false;
              }
              else
                  return false;
          }
    </script>--%>


     <script type="text/javascript">
         function numberOnly(txt, e) {
             var arr = "0123456789.";
             var code;
             if (window.event)
                 code = e.keyCode;
             else
                 code = e.which;
             var char = keychar = String.fromCharCode(code);
             if (arr.indexOf(char) == -1)
                 return false;
             else if (char == ".")
                 if (txt.value.indexOf(".") > -1)
                     return false;
         }
    </script>

</asp:Content>

