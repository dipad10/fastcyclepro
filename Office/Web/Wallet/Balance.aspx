﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Balance.aspx.vb" Inherits="Office_Web_Wallet_Balance" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Wallet</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">My Wallet</a></li>

                <li class="breadcrumb-item active"><a href="#">Wallet Summary</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Wallet Summary</h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">


                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Account Type">
                                            <ItemTemplate>
                                                <span class="">Coinpayments </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Total Balance">
                                            <ItemTemplate>
                                                <span class="fw-bold"><i class="fa fa-bitcoin"></i><%#Eval("totalBAL")%> </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>





                                        <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                                    </Columns>
                                </asp:GridView>
                                
                            </div>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="GetSumWalletbalance" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="username" SessionField="username" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button" aria-expanded="false">Select action <i class="fa fa-sort-desc" aria-hidden="true"></i></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="/office/web/Wallet/Add_money">Add Money </a></li>
                                        <li><a href="/office/web/Wallet/Withdraw_money">Withdraw Money </a></li>
                                        <li class="divider"></li>
                                     
                                    </ul>
                                </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</asp:Content>

