﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Tickets_new.aspx.vb" Inherits="Office_Web_Tickets_new" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <header class="page-header col-md-12">
            <h3>Create a New Support Ticket</h3>

        </header>

        <div class="mail-style prtm-block">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="compose">
                        <div class="compose-header">
                        
                            <div class="compose-field">
                                <div class="compose-field-body">
                                     <div class="form-group">
                                  <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                  <div class="selectbox">
                                           <asp:DropDownList ID="ddltitle" CssClass="compose-input" runat="server">
                                        <asp:ListItem Selected="True" Value="null">Add a Subject</asp:ListItem>
                                        <asp:ListItem>Irregular Activities</asp:ListItem>
                                         <asp:ListItem>Account Issue</asp:ListItem>
                                         <asp:ListItem>Report A Member</asp:ListItem>
                                         <asp:ListItem>Suggestions to make Fastcyclepro Better</asp:ListItem>
                                    </asp:DropDownList>
                                     </div>
                                  
                                </div>
                                      </div>
                                                        


                                                    </div>
                         
                                  
                                   
                                </div>
                            </div>
                            <div class="compose-field">
                                <div class="compose-field-body">
                                       <dx:ASPxUploadControl ID="ASPxUploadControl1" Width="320px" NullText="Upload file" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                                    <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg" MaxFileSize="1000000">
                                    </ValidationSettings>
                                </dx:ASPxUploadControl>
                                    <asp:Image ID="Image1" Visible="false" Width="300" Height="300" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="compose-body">
                            <div class="compose-message pad-all-xs">
                                <div id="alerts"></div>
                               
                                
                                    <asp:TextBox ID="editor" placeholder="Enter Message" Width="100%" Height="400px" TextMode="MultiLine" CssClass="prtm-editor" runat="server"></asp:TextBox>
                               
                            </div>
                            <div class="compose-actions">
                                <asp:Button ID="btnsumit" OnClick="btnsumit_Click" CssClass="btn btn-primary btn-lg" runat="server" Text="Submit" />
                                <button runat="server" id="btndiscard" class="btn btn-inverse btn-lg" OnClientClick="history.go(-1)" type="button"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Discard</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

