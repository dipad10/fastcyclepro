﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Office_Web_Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <header class="page-header col-md-12">
            <h3>Fastcyclepro Dashboard</h3>

        </header>
        <%-- announcement section --%>




        <section>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <h3 class="panel-title text-capitalize"><i class="fa fa-bell"></i>Latest Announcements</h3>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <a href="#panel-drop-5" class="pull-right" data-toggle="collapse"><i class="fa fa-angle-down fa-lg fa-inverse"></i></a>
                        </div>
                    </div>
                </div>
                <div id="panel-drop-5" class="collapse in">
                    <div class="panel-body">
                        <p runat="server" id="announce" class="mrgn-all-none fw-bold"></p>
                    </div>
                </div>
            </div>
        </section>
        <br />
        <br />
        <div class="modal" style="margin-top: 23px;" aria-hidden="true" id="popup">
            <div class="modal-backdrop fade in" style="z-index: 1050; background: #000 !important;"></div>
            <div style="z-index: 1060;" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title center text-center">
                            <img src="/assets/icon.png" style="height: 40px; width: 40px;" class="img-circle" />
                            Hi There!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12">
                                <ul class="unordered-list">
                                    <li class="font-sm fw-bold text-secondary">I'm Martin, I will guide you on how to use Fastcyclepro platform. You are one-step away from becoming a millionaire through the World's fastest Straight Line Cycler.
                                    </li>
                                    <li class="font-sm text-left">We've Made all controls available on your dashboard.</li>
                                    <li class="font-sm text-left">First thing to do is to update your Bitcoin wallet address.</li>
                                    <li class="font-sm text-left">Credit your Fastcyclepro account with the amount of positions you want to buy E.g 0.0102, 0.0204 BTC</li>
                                    <li class="font-sm text-left">Last step is to buy a position and relax while receiving bitcoins in your wallet</li>
                                    <li class="font-sm text-left">You can create a support ticket if you have a suggestion or request. To Create a Support Ticket, Go the side menu and click 'Tickets' and you can see the 'create a ticket' button
                                    </li>
                                    
                                    <li class="font-sm text-left">If you need anything else just send us a message using our online chat located at the right hand bottom corner of the pages. Keep Promoting FCP and keep Earning!
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>


                    </div>
                </div>
            </div>
        </div>


        <%-- CARDS SECTION --%>
        <div class="row prtm-card-wrapper">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box graph bg-primary pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Paid Withdrawal</span></div>
                            <%-- <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblindollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">

                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lblinbitcoin" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>


            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box order bg-success pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Purchase</span> </div>
                            <%--  <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblprofitdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lblprofitbitcoin" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>


            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box invoices bg-info pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Balance</span> </div>
                            <%--   <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblrefdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lblrefbitcoin" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box tickets bg-secondary pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Referrals</span> </div>
                            <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-user-circle-o"></i>
                                    <asp:Label ID="lbltotalref" runat="server" Text=""></asp:Label>
                                </span>
                            </div>
                        </div>
                        <div class="prtm-card-progress">

                            <asp:Label ID="lblreflink" CssClass="font-xs" runat="server" Text=""></asp:Label>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%-- MY dashboard SEction --%>


        <section class="panel">
            <div class="panel-body">
                <h4 class="text-info">My Dashboard</h4>
                <span runat="server" id="showplanname" class="font-sm text-warning fw-bold"></span>
                <hr />
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <center>
<div class="profile_img">
<div id="crop-avatar">
 
<img runat="server" id="profileuserpicture" class="img-responsive img-circle display-ib" width="200" height="200" src="#" alt="Avatar" title="Change the avatar">
</div>
</div>
<h3 runat="server" id="profilefullname"></h3>
<ul class="list-unstyled user_data">
<%--<li class="m-top-xs"><i class="fa fa-phone user-profile-icon"></i> <asp:Label ID="profileuserphone" runat="server" Text=""></asp:Label> </li>--%>
 <li><i class="fa fa-user user-profile-icon"></i> Username: <asp:Label ID="profileusername" runat="server" Text=""></asp:Label> </li>
<li><i class="fa fa-envelope user-profile-icon"></i> <asp:Label ID="profileemail" runat="server" Text=""></asp:Label>  </li>
</ul>
<a href="/office/web/MemberAccount/Profile_edit.aspx" class="btn btn-base btn-primary btn-rounded btn-sm"><i class="fa fa-edit m-right-xs"></i> Edit my Basic Info</a>
 <a href="#responsive" class="btn btn-base btn-rounded btn-sm" role="button" data-toggle="modal">Update Wallet Address</a>
                       
<div id="responsive" class="modal fade" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Bitcoin Wallet Address</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        
                                            <div class="col-md-12">
                                                <h4>Current Wallet Address</h4>
                                                <div class="form-group">
                                                
                                              <input type="text" id="txtwalletaddress" runat="server" form="form1" name="txtwalletaddress" class="form-control fw-bold">
                                                </div>
                                                
                                            </div>
                                            
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>
                                    <button type="button" runat="server" id="wall" onserverclick="btnupdatewallet_Click" class="btn btn-success">Save changes</button>

                                    
                                </div>
                            </div>
                        </div>
                    </div>
</center>
                </div>
                <div class="col-md-6 col-sm-9 col-xs-12 profile_details">
                    <div>
                        <div class="caption">
                            <h4>Fastcyclepro Positions</h4>
                            <hr />
                        </div>
                        <div class="table-responsive">

                            <asp:GridView ID="GridView1" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" runat="server" AllowPaging="True">
                                <Columns>
                                    <asp:BoundField ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="bg-primary font-sm" DataField="PositionID" HeaderStyle-ForeColor="White" HeaderText="#PID" SortExpression="PositionID" />
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Method">
                                        <ItemTemplate>
                                            <span class="font-sm">CoinPayments</span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Line">
                                        <ItemTemplate>
                                            <span class="font-sm"><%#Eval("LevelID")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Downlines">
                                        <ItemTemplate>
                                            <span class="font-sm"><%#Eval("Downlines")%> / 3</span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Purchase">
                                        <ItemTemplate>
                                            <span class="font-sm"><i class="fa fa-bitcoin"></i><%#Eval("Purchased")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Earned">
                                        <ItemTemplate>
                                            <span class="font-sm"><i class="fa fa-bitcoin"></i><%#Eval("Earned")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:BoundField ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="bg-primary" DataField="Status" HeaderStyle-ForeColor="White" HeaderText="Status" SortExpression="Status" />


                                    <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                                </Columns>
                            </asp:GridView>
                            <div style="text-align: center;" class="center">
                                <asp:Label ID="lblnoposition" CssClass="text-center text-warning" Visible="false" runat="server" Text="- no positions -"></asp:Label>

                            </div>
                        </div>
                        <br />
                         <div class="caption">
                            <h4>FastCycle Blazer Positions</h4>
                            <hr />
                        </div>
                       
                        <div class="table-responsive">

                            <asp:GridView ID="GridView2" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" runat="server" AllowPaging="True">
                                <Columns>
                                    <asp:BoundField ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="bg-primary font-sm" DataField="PositionID" HeaderStyle-ForeColor="White" HeaderText="#PID" SortExpression="PositionID" />
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Method">
                                        <ItemTemplate>
                                            <span class="font-sm">CoinPayments</span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Line">
                                        <ItemTemplate>
                                            <span class="font-sm">Fastcycle Blazer</span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Downlines">
                                        <ItemTemplate>
                                            <span class="font-sm"><%#Eval("Downlines")%> / 3</span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Purchase">
                                        <ItemTemplate>
                                            <span class="font-sm"><i class="fa fa-bitcoin"></i><%#Eval("Purchased")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Earned">
                                        <ItemTemplate>
                                            <span class="font-sm"><i class="fa fa-bitcoin"></i><%#Eval("Earned")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:BoundField ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="bg-primary" DataField="Status" HeaderStyle-ForeColor="White" HeaderText="Status" SortExpression="Status" />


                                    <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                                </Columns>
                            </asp:GridView>
                            <div style="text-align: center;" class="center">
                                <asp:Label ID="Label1" CssClass="text-center text-warning" Visible="false" runat="server" Text="- no positions -"></asp:Label>

                            </div>
                        </div>
                    </div>


                </div>
                <style>
                 
                </style>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div>
                        <div class="caption">
                            <h4>Quick Links</h4>
                            <hr />
                       
                        </div>
                        <ul class="list-unstyled top_profiles scroll-view">
                            <li>
                                <a style="background:#5e6db3;" href="https://www.facebook.com/groups/2339699896255343/" class="btn btn-warning"><i class="fa fa-facebook"></i>  Join Facebook Group</a>
                            </li>
                            <br />
                              <li>
                                <a style="background:#21eb00;" href="https://chat.whatsapp.com/7VmohiPJQUf9s9mKoArYwm" class="btn btn-warning"><i class="fa fa-whatsapp"></i>  Join Whatsapp Group</a>
                            </li>
                            <br />
                              <li>
                                <a style="background:#4e86ab;" href="https://www.instagram.com/fastcycleproofficial/" class="btn btn-warning"><i class="fa fa-instagram"></i>  Follow FCP</a>
                            </li>
                            <br />
                            <li>
                                <a href="/office/web/Wallet/Add_money" class="btn btn-warning"><i class="fa fa-money"></i>Fund Account</a>
                            </li>
                            <br />
                            <li>
                                <a href="/office/web/MatrixCycler/buy_position" class="btn btn-base"><i class="fa fa-cart-plus"></i>Buy position</a>
                            </li>
                            <br />
                            <li>
                                <a href="/office/web/referralprogram/Referrals" class="btn btn-success"><i class="fa fa-child"></i>Referrals</a>
                            </li>
                            <br />
                            <li>
                                <a href="/office/web/MatrixCycler/Lines" class="btn btn-secondary"><i class="fa fa-sort-amount-asc"></i>Next in Line</a>
                            </li>


                        </ul>
                    </div>
                </div>

                <hr />


            </div>
        </section>
        <%-- STart Gridiews --%>
        <br />
    </form>
    <script>
        $(document).ready(function () {
            if (localStorage.getItem('popState') != 'shown') {
                $("#popup").delay(2000).fadeIn();
                localStorage.setItem('popState', 'shown')
            }

            $('#popup-close').click(function (e) // You are clicking the close button
            {
                $('#popup').fadeOut(); // Now the pop up is hiden.
            });
            $('#popup').click(function (e) {
                $('#popup').fadeOut();
            });
        });
    </script>
</asp:Content>

