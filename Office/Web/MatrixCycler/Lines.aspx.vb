﻿
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Partial Class Office_Web_MatrixCycler_Lines
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            bindphase0(0)
            bindphase1(1)
            bindphase2(2)
            bindphaseblazer()
            bindphaseblazerB()
            'bindphase3()
            'bindphase4()
            'bindphase5()
        End If
    End Sub

    Private Sub bindphase0(levelid As Integer)
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try       
            cmd = New SqlCommand("Selectlevelpositions", _Connection)
            cmd.Parameters.AddWithValue("@levelid", levelid)
            cmd.CommandType = CommandType.StoredProcedure
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeaterlevel0.DataSource = dt
                Repeaterlevel0.DataBind()
            Else
                lblnolevel0.Visible = True

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
      
        End Try
    End Sub
  
    'Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectlevelpositions(0)
    'GridViewlevel0.DataSource = rec
    'GridViewlevel0.DataBind()
    'If GridViewlevel0.Rows.Count = 0 Then
    '    lblnolevel0.Visible = True
    'End If

  
    Private Sub bindphase1(levelid As Integer)
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try
            cmd = New SqlCommand("Selectlevelpositions", _Connection)
            cmd.Parameters.AddWithValue("@levelid", levelid)
            cmd.CommandType = CommandType.StoredProcedure
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeaterlevel1.DataSource = dt
                Repeaterlevel1.DataBind()
            Else
                lblnolevel1.Visible = True

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)

        End Try

    End Sub

    Private Sub bindphaseblazer()
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try
            cmd = New SqlCommand("Selectlevelblazer", _Connection)
            'cmd.Parameters.AddWithValue("@levelid", levelid)
            cmd.CommandType = CommandType.StoredProcedure
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeaterblazer.DataSource = dt
                Repeaterblazer.DataBind()
            Else
                lblnoblazer.Visible = True

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)

        End Try

    End Sub

    Private Sub bindphaseblazerB()
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try
            cmd = New SqlCommand("SelectlevelblazerB", _Connection)
            'cmd.Parameters.AddWithValue("@levelid", levelid)
            cmd.CommandType = CommandType.StoredProcedure
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                RepeaterblazerB.DataSource = dt
                RepeaterblazerB.DataBind()
            Else
                lblnoblazer.Visible = True

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)

        End Try

    End Sub

    Private Sub bindphase2(levelid As Integer)
        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try
            cmd = New SqlCommand("Selectlevelpositions", _Connection)
            cmd.Parameters.AddWithValue("@levelid", levelid)
            cmd.CommandType = CommandType.StoredProcedure
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeaterlevel2.DataSource = dt
                Repeaterlevel2.DataBind()
            Else
                Lblnolevel2.Visible = True

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)

        End Try

    End Sub

    'Private Sub bindphase3()
    '    Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectlevelpositions(3)
    '    GridViewlevel3.DataSource = rec
    '    GridViewlevel3.DataBind()
    '    If GridViewlevel3.Rows.Count = 0 Then
    '        lblnolevel3.Visible = True
    '    End If

    'End Sub

    'Private Sub bindphase4()
    '    Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectlevelpositions(4)
    '    GridViewlevel4.DataSource = rec
    '    GridViewlevel4.DataBind()
    '    If GridViewlevel4.Rows.Count = 0 Then
    '        lblnolevel4.Visible = True
    '    End If

    'End Sub

    'Private Sub bindphase5()
    '    Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectlevelpositions(5)
    '    GridViewlevel5.DataSource = rec
    '    GridViewlevel5.DataBind()
    '    If GridViewlevel5.Rows.Count = 0 Then
    '        lblnolevel5.Visible = True
    '    End If

    'End Sub
End Class
