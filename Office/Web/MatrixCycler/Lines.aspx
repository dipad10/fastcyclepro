﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Lines.aspx.vb" Inherits="Office_Web_MatrixCycler_Lines" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Lines</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Matrix Cycler</a></li>

                <li class="breadcrumb-item active"><a href="#">Lines</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Lines </h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">
                            <div class="unseen">
                                <div class="table-responsive">
                          
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>

                                            <tr class="bg-primary">
                                                <th>Line</th>
                                                <th>Summary</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <tr>
                                                <td valign="middle"><img src="/assets/fire.png" width="28" height="30" /><b>FastCycle Blazer</b></td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.005<br>
                                                    Payout - ฿0.01<br>
                                                </td>
                                                <td align="center">
                                                    <a href="#Gridblazer" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a> <b>|</b> <a href="/office/web/MatrixCycler/buy_position" class="btn btn-secondary"><i class="fa fa-shopping-cart"></i>Buy Position</a></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle"><img src="/assets/rocket.png" width="28" height="30" /><b>Level 0</b></td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.015<br>
                                                    Payout - ฿0.005<br>
                                                </td>
                                                <td align="center">
                                                    <a href="#Grid0" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a> <b>|</b> <a href="/office/web/MatrixCycler/buy_position?type=0" class="btn btn-secondary"><i class="fa fa-shopping-cart"></i>Buy Position</a></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle"><img src="/assets/rocket.png" width="28" height="30" /><b>Level 1</b></td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.02<br>
                                                    Payout - ฿0.01<br>
                                                </td>
                                                <td align="center"><a href="#Grid1" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle"><img src="/assets/rocket.png" width="28" height="30" /><b>Level 2</b></td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.05<br>
                                                    Payout - ฿0.135<br>
                                                    1 new position(s) in Level 0 on cycling<br>
                                                </td>
                                                <td align="center"><a href="#Grid2" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a></td>
                                            </tr>
                                            <%--  <tr>
                                                <td valign="middle">Level 3</td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.10<br>
                                                    Payout - ฿0.0898<br>
                                                  1 new position(s) in Level 0 on cycling<br>
                                                </td>
                                                <td align="center"><a href="#Grid3" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Level 4</td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.20<br>
                                                    Payout - ฿0.1898<br>
                                                   1 new position(s) in Level 0 on cycling<br>
                                                </td>
                                                <td align="center"><a href="#Grid4" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a></td>
                                            </tr>

                                            <tr>
                                                <td valign="middle">Level 5</td>
                                                <td valign="middle">3 x 1 line.<br>
                                                    Position cost - ฿0.40<br>
                                                    Payout - ฿1.1898<br>
                                                 1 new position(s) in Level 0 on cycling<br>
                                                </td>
                                                <td align="center"><a href="#Grid5" class="btn btn-secondary" role="button" data-toggle="modal"><i class="fa fa-sort-amount-asc"></i>Next in Line</a></td>
                                            </tr>--%>
                                        </tbody>

                                    </table>
                            


                            </div>
                            </div>

                           <div id="Gridblazer" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                          <asp:Panel ID="Panel4" ScrollBars="Vertical" Height="550" runat="server">
                                                        <table class="table table-striped table-bordered table-middle table-hover">
                                                            <thead>
                                                                <tr class="bg-primary">

                                                                    <th class="font-xs">Username</th>
                                                                    <th class="font-xs">Position ID</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                          
                                                                  <asp:Repeater ID="RepeaterblazerB" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><img src='<%#Eval("state")%>' width="25" height="20" /> <span><img class="img-circle" src='<%#Eval("Profilepicpath")%>' width="25" height="20" /> </span> <span data-toggle="tooltip" data-placement="top" data-original-title="Position Cycled"  class="font-xs"><%#Eval("Username")%></span></td>
                                                                                <td> <span class="font-xs">#<%#Eval("PositionID")%> </span></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                 
                                                                <tr>
                                                                    <td><hr style="font-weight:bold; color:#3a526a" /></td>
                                                                     <td><hr style="font-weight:bold; color:#3a526a" /></td>
                                                                </tr>
                                                                   <asp:Repeater ID="Repeaterblazer" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><img src='<%#Eval("state")%>' width="25" height="20" /> <span><img class="img-circle" src='<%#Eval("Profilepicpath")%>' width="25" height="20" /> </span> <span data-toggle="tooltip" data-placement="top" data-original-title="Position not cycled Yet"  class="font-xs"><%#Eval("Username")%></span></td>
                                                                                <td> <span class="font-xs">#<%#Eval("PositionID")%> </span></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                               
                                                                    <asp:Label ID="lblnoblazer" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                             
                                                                
                                                            </tbody>
                                                        </table>
                                                        
                                                              </asp:Panel>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Grid0" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                          <asp:Panel ID="Panel1" ScrollBars="Vertical" Height="550" runat="server">
                                                        <table class="table table-striped table-bordered table-middle table-hover">
                                                            <thead>
                                                                <tr class="bg-primary">

                                                                    <th class="font-xs">Username</th>
                                                                    <th class="font-xs">Position ID</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                          

                                                                    <asp:Repeater ID="Repeaterlevel0" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><img src='<%#Eval("state")%>' width="25" height="20" /> <span><img class="img-circle" src='<%#Eval("Profilepicpath")%>' width="25" height="20" /> </span> <span class="font-xs"><%#Eval("Username")%></span></td>
                                                                                <td> <span class="font-xs">#<%#Eval("PositionID")%> </span></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:Label ID="lblnolevel0" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                             
                                                                
                                                            </tbody>
                                                        </table>
                                                        
                                                              </asp:Panel>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="Grid1" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <asp:Panel ID="Panel2" ScrollBars="Vertical" Height="550" runat="server">
                                                            <table class="table table-striped table-bordered table-middle table-hover">
                                                            <thead>
                                                                <tr class="bg-primary">

                                                                    <th class="font-xs">Username</th>
                                                                    <th class="font-xs">Position ID</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                          

                                                                    <asp:Repeater ID="Repeaterlevel1" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><img src='<%#Eval("state")%>' width="25" height="20" /> <span><img class="img-circle" src='<%#Eval("Profilepicpath")%>' width="25" height="20" /> </span> <span class="font-xs"><%#Eval("Username")%></span></td>
                                                                                <td> <span class="font-xs">#<%#Eval("PositionID")%> </span></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                             
                                                                
                                                            </tbody>
                                                        </table>
                                                            <asp:Label ID="lblnolevel1" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                        </asp:Panel>

                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Grid2" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <asp:Panel ID="Panel3" ScrollBars="Vertical" Height="550" runat="server">
                                                            <table class="table table-striped table-bordered table-middle table-hover">
                                                            <thead>
                                                                <tr class="bg-primary">

                                                                    <th class="font-xs">Username</th>
                                                                    <th class="font-xs">Position ID</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                          

                                                                    <asp:Repeater ID="Repeaterlevel2" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><img src='<%#Eval("state")%>' width="25" height="20" /> <span><img class="img-circle" src='<%#Eval("Profilepicpath")%>' width="25" height="20" /> </span> <span class="font-xs"><%#Eval("Username")%></span></td>
                                                                                <td> <span class="font-xs">#<%#Eval("PositionID")%> </span></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                             
                                                            </tbody>
                                                        </table>
                                                            <asp:Label ID="Lblnolevel2" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                        </asp:Panel>

                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--      <div id="Grid3" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                         <asp:Panel ID="Panel4" ScrollBars="Vertical" Height="550" runat="server">
                                                                <asp:GridView ID="GridViewlevel3" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Username">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs"><%#Eval("Username")%></span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Position ID">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs">#<%#Eval("PositionID")%> </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:Label ID="lblnolevel3" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                         </asp:Panel>
                                                     
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="Grid4" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                          <asp:Panel ID="Panel5" ScrollBars="Vertical" Height="550" runat="server">
                                                                  <asp:GridView ID="GridViewlevel4" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Username">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs"><%#Eval("Username")%></span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Position ID">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs">#<%#Eval("PositionID")%> </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:Label ID="lblnolevel4" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                          </asp:Panel>
                                                    
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="Grid5" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Next In Line</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GridViewlevel5" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" runat="server">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Username">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs"><%#Eval("Username")%></span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-CssClass="bg-primary font-xs" HeaderText="Position ID">
                                                                    <ItemTemplate>
                                                                        <span class="font-xs">#<%#Eval("PositionID")%> </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:Label ID="lblnolevel5" Visible="false" CssClass="text-warning font-xs" runat="server" Text="- no positions found -"></asp:Label>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline-inverse">Close</button>



                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</asp:Content>

