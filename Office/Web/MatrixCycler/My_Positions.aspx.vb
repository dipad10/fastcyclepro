﻿
Partial Class Office_Web_MatrixCycler_My_Positions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            bindphase(Session("username"), 0)
            lbllevel.Text = "Level 0"
        End If
    End Sub

    Protected Sub ddllevel_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case ddllevel.SelectedValue
            Case "null"
                'dont anything
            Case "A"
                Dim rec As List(Of fastcyclepro.Blazer) = (New cls_blazer).Selectuserposition(Session("username"), ddllevel.SelectedValue)
                GridView1.DataSource = rec
                GridView1.DataBind()
                lbllevel.Text = String.Format("{0}", "FastCycle Blaze")
                If GridView1.Rows.Count = 0 Then
                    lblnoposition.Visible = True
                Else
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            Case Else
                Dim rec As List(Of fastcyclepro.Position) = (New cls_position).Selectuserpositions(Session("username"), ddllevel.SelectedValue)
                GridView1.DataSource = rec
                GridView1.DataBind()
                lbllevel.Text = String.Format("Level {0}", ddllevel.SelectedValue)
                If GridView1.Rows.Count = 0 Then
                    lblnoposition.Visible = True
                Else
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
        End Select
      


    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)

    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim dr As System.Data.DataRow = DirectCast(e.Row.DataItem, System.Data.DataRowView).Row

        '    If dr("Status").ToString() = "0" Then
        '        DirectCast(e.Row.FindControl("lblstatus"), Label).Text = "NOT ACTIVE"
        '    ElseIf dr("Priority").ToString() = "1" Then
        '        DirectCast(e.Row.FindControl("lblstatus"), Label).Text = "ACTIVE"

        '    End If
        'End If
    End Sub
    Private Sub bindphase(ByVal username As String, ByVal levelid As Integer)
        Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectuserpositions(username, levelid)
        GridView1.DataSource = rec
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            lblnoposition.Visible = True
        End If

    End Sub
   
End Class
