﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="buy_position.aspx.vb" Inherits="Office_Web_MatrixCycler_buy_position" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Are you sure you want to Buy this Position?" TargetControlID="btnbuy" runat="server" />

        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Buy Position</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Matrix Cycler</a></li>

                <li class="breadcrumb-item active"><a href="#">Buy Position</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Buy Position</h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">
                             <div class="form-group">
                                  <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <p class="font-2x fw-semibold">Select Your Plan</p>
                                <div class="selectbox">
                                                            <asp:DropDownList ID="ddllevel" AutoPostBack="true" OnSelectedIndexChanged="ddllevel_SelectedIndexChanged" CssClass="form-control" runat="server">
                                                            
                                                                <asp:ListItem Selected="True" Value="fcpblazer">FastCycle Blaze</asp:ListItem>
                                                                <asp:ListItem Value="level0">FastCyclepro</asp:ListItem>
                                                           
                                                              <%--  <asp:ListItem Value="3">Level 3</asp:ListItem>
                                                                <asp:ListItem Value="4">Level 4</asp:ListItem>
                                                                <asp:ListItem Value="5">Level 5</asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                        </div>
                                </div>
                                      </div>
                                                        


                                                    </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-middle table-hover">
                                    <thead>
                                        <tr class="bg-primary">

                                            <th>Plan</th>
                                            <th>Summary</th>
                                            <th colspan="2">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <asp:Panel ID="pnlfcp" Visible="false" runat="server">
                                        <tbody>
                                        <tr>
                                            <td valign="middle" rowspan="2"><img src="/assets/rocket.png" width="28" height="30" /><b>FastCyclePro</b></td>
                                            <td valign="middle" rowspan="2">3 x 1 line.<br>
                                                Position cost - ฿0.015<br>
                                                Payout - ฿0.005</td>
                                            <td># of Positions:</td>
                                            <td>
                                                <asp:TextBox placeholder="Write 1" onkeypress="return numbersonly(this, event)" ID="txtnumber" data-toggle="tooltip" data-placement="top" data-original-title="Write '1' Here" CssClass="form-control" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Payment option(Wallet Balance <i class="fa fa-bitcoin"></i>)</td>
                                            <td>
                                                <asp:TextBox ID="txtbalance" data-toggle="tooltip" data-placement="right" data-original-title="Your Total Balance." CssClass="form-control" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td></td>
                                            <td>

                                                <asp:Button ID="btnbuy" OnClick="btnbuy_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Buy Now" />

                                            </td>
                                        </tr>
                                    </tbody>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlblazer" runat="server">
                                        <tbody>
                                        <tr>
                                            <td valign="middle" rowspan="2"><img src="/assets/fire.png" width="28" height="30" /><b>FastCycle BLAZER</b></td>
                                            <td valign="middle" rowspan="2">3 x 1 line.<br>
                                                Position cost - ฿0.005<br>
                                                1 new position in Fastcyclepro on cycling<br />
                                                Payout - ฿0.01
                                            </td>
                                            <td># of Positions:</td>
                                            <td>
                                                <asp:TextBox placeholder="Write 1" onkeypress="return numbersonly(this, event)" ID="txtnumberblazer" data-toggle="tooltip" data-placement="top" data-original-title="Write '1' Here" CssClass="form-control" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Payment option(Wallet Balance <i class="fa fa-bitcoin"></i>)</td>
                                            <td>
                                                <asp:TextBox ID="txtwalletbalanceblazer" data-toggle="tooltip" data-placement="right" data-original-title="Your Total Balance." CssClass="form-control" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td></td>
                                            <td>

                                                <asp:Button ID="btnbuyblazer" OnClick="btnbuyblazer_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Buy Now" />

                                            </td>
                                        </tr>
                                    </tbody>
                                    </asp:Panel>
                                    
                                </table>

                            </div>

                                       

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>

    <script>
        function numbersonly(myfield, e) {
            var key;
            var keychar;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;

            keychar = String.fromCharCode(key);

            // control keys
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                return true;

                // numbers
            else if ((("0123456789").indexOf(keychar) > -1))
                return true;

                // only one decimal point
            else if ((keychar == ".")) {
                if (myfield.value.indexOf(keychar) > -1)
                    return false;
            }
            else
                return false;
        }
    </script>

</asp:Content>

