﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments
Partial Class Office_Web_MatrixCycler_buy_position
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Session("username") = "" Then

                Dim OriginalUrl As String = HttpContext.Current.Request.RawUrl
                Dim LoginPageUrl As String = "/office/signin"
                HttpContext.Current.Response.Redirect([String].Format("{0}?goto={1}", LoginPageUrl, OriginalUrl))

            End If
            bindwalletbalance(Session("username"))
            'btnbuy.Enabled = False
            Dim type As String = Request.QueryString("type")
            If type <> "" Then
                pnlfcp.Visible = True
                pnlblazer.Visible = False
                ddllevel.SelectedValue = "level0"
            End If
        End If
    End Sub


    Protected Sub bindwalletbalance(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "GetSumWalletbalance"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("totalBAL") IsNot DBNull.Value Then
                        txtbalance.Text = dr("totalBAL")
                        txtbalance.Enabled = False
                        txtwalletbalanceblazer.Text = dr("totalBAL")
                        txtwalletbalanceblazer.Enabled = False
                    Else
                        txtbalance.Text = "0.0000"
                        txtwalletbalanceblazer.Text = "0.0000"

                    End If

                Else

                    txtbalance.Text = "0.0000"
                    txtwalletbalanceblazer.Text = "0.0000"
                End If

            End Using

        End Using
    End Sub

    Protected Sub btnbuy_Click(sender As Object, e As EventArgs)
       
        'Do checkings
        If txtnumber.Text = "" Then
            msgbox1.ShowError("Please write 1 in the number of positions")
            Exit Sub
        End If
        Try
            If txtnumber.Text <> 1 Then
                msgbox1.ShowError("Please buy 1 position at a time")
                Exit Sub

            End If
        Catch ex As Exception
            msgbox1.ShowError("Please Insert figures only")
        End Try
     

        If txtbalance.Text < 0.015 Then
            msgbox1.ShowError("Insufficient Balance to Purchase position")
            Exit Sub
        End If
        'check if the upline address is valid
        Dim refuser As Fastcyclepro.Referal = (New cls_referal).SelectThisreferee(Session("username"))
        Dim refreguser As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(refuser.Referer)
        If refreguser.BitcoinwalletAddress = "N/A" Then
            msgbox1.ShowError("Sorry, Your Referral is yet to update Bitcoin walletaddress. Please Contact your Referral, Thank you")
            Exit Sub
        End If

        'check if datetime is less than repurchase time
        Dim chkdate As fastcyclepro.Position = (New cls_position).SelectThisusername(Session("username"))
        If chkdate.RepurchaseTime Is Nothing Then
            'means its a fresh position purchase user is new
        Else
            If DateTime.Now < chkdate.RepurchaseTime Then
                msgbox1.ShowError("You can't purchase another position now. Please check back In 30 mins time")
                Exit Sub
            End If
        End If

        'get next value of phase 0 in autosettings
        Dim leveltype As String = "Level0"
        Dim count As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(leveltype)
        If count.Nextvalue = 3 Then
            'Reset level(0) autonumber back to 1
            Call mod_main.Resetautonumber("Level0")
            'First of save this new guy position and do normal thing
            'Insert position, get the referral of this person and give him 0.005btc bonus, send mail to refrral of bonus earned, send mail to this sessionuser about newly position. and now add +1 to levelone count
            Dim positionid As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            'save new position
            Call Savenewposition(Session("username"), positionid, 0, 0, "0.0000", "0.01", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'post withdrawal of position purchase
            Call mod_main.Savewallet("FASTCYLEPRO", 0, 0.01, 0.01, Session("username"), "WITHDRAWAL", Guid.NewGuid.ToString, "Position Purchase in Level(0)", "")
            'send mail to user abot newly acquired position
            Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = mod_main.PopulateBodynewposition(use.Name, "#" & positionid & "", 0, Date.Now.ToLongDateString)
            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "Your New Position Added", body)
            'get referer
            Try
                Dim ref As Fastcyclepro.Referal = (New cls_referal).SelectThisreferee(Session("username"))
                Dim refreg As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(ref.Referer)
                Dim Transid2 As String = POSTWITHDAWAL("create_withdrawal", 0.005, "1", "BTC", refreg.BitcoinwalletAddress, "1", "Referral Commissions", ConfigurationManager.AppSettings("publickey"))
                Call mod_main.Savewallet("COINPAYMENTS", 0, 0.005, 0.005, refreg.UserName, "COMMISSIONS", Transid2, "Straight Line Commisions from " & Session("username") & "'s Position Purchase", Session("username"))
                'send mail to referrer
                Dim body2 As String = mod_main.PopulateBodyreferralbonus(refreg.Name, Session("username"), 0.005 & "BTC", "Straight Line Commisions from " & Session("username") & "'s Position Purchase")
                Call mod_main.SendHtmlFormattedEmail(refreg.Email, "", "You have just earned referral commission", body2)
            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
            End Try
          
            'END OF POSITION PURCHASE -------


            '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            'START CYCLING PROCESS HERE ------


            'whent its equal to 3 means 3 people have joined for the top guy phase 0 member to cycle out to nextphase.
            Dim posi As Fastcyclepro.Position = (New cls_position).SelectThislevelid(0)
            'get the user details
            Dim use1 As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi.Username)
            'Give him 0.005btc on cycling, the code belwo withdraws 0.005 btc from central wallet and get transid

            Dim Transid As String = Guid.NewGuid.ToString
            'POSTWITHDAWAL("create_withdrawal", 0.005, "1", "BTC", use1.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.005, 0.005, posi.Username, "EARNINGS", Transid, "Profit on cycling from level(0) to Level(1)", "")
            Call mod_main.Savewallet("FASTCYCLEPRO", 0.005, 0, 0.005, posi.Username, "DEPOSIT", Transid, "Profit on cycling from level(0) to Level(1)", "")

            'update user position, set status=0 and update earned column
            Call mod_main.updateposition(posi.PositionID, 0.005, "CYCLED", 3)

            'buy new position for him in level1
            Dim positionid2 As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            Call Savenewposition(posi.Username, positionid2, 1, 0, "0.0000", "0.02", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase

            Dim body3 As String = mod_main.PopulateBodycycled(use1.Name, "#" & positionid2 & "", 1, Date.Now.ToLongDateString, 0.005, use1.BitcoinwalletAddress)
            Call mod_main.SendHtmlFormattedEmail(use1.Email, "", "Your New Position Added", body3)

            'check if level(1) autonumber is 3(means if 3 people have joined)
            Dim count2 As String = mod_main.Getautonumber("Level1")
            If count2 = 3 Then
                'start cycling process for level1 members

                'Reset level(0) autonumber back to 0
                Call mod_main.Resetautonumber("level1")
                'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
                Dim posi2 As Fastcyclepro.Position = (New cls_position).SelectThislevelid(1)
                'get the user details
                Dim use2 As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi2.Username)
                'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

                Dim Transid3 As String = Guid.NewGuid.ToString
                'POSTWITHDAWAL("create_withdrawal", 0.01, "1", "BTC", use2.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                Call mod_main.Savewallet("COINPAYMENTS", 0, 0.01, 0.01, posi2.Username, "EARNINGS", Transid3, "Profit on cycling from level(1) to Level(2)", "")
                Call mod_main.Savewallet("FASTCYCLEPRO", 0.01, 0, 0.01, posi2.Username, "DEPOSIT", Transid3, "Profit on cycling from level(1) to Level(2)", "")

                'update user position, set status=0 and update earned column
                Call mod_main.updateposition(posi2.PositionID, 0.01, "CYCLED", 3)

                'buy new position for him in level2
                Dim positionid3 As String = mod_main.Getautonumber("PositionID")
                Call mod_main.Updateautonumber("PositionID")
                Call Savenewposition(posi2.Username, positionid3, 2, 0, "0.0000", "0.05", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
                Dim body4 As String = mod_main.PopulateBodycycled(use2.Name, "#" & positionid3 & "", 2, Date.Now.ToLongDateString, 0.01, use2.BitcoinwalletAddress)
                Call mod_main.SendHtmlFormattedEmail(use2.Email, "", "Your New Position Added", body4)
                'continue cycling process to 2
                Dim count3 As String = mod_main.Getautonumber("Level2")
                If count3 = 3 Then
                    'start cycling process for level2
                    'Reset level(0) autonumber back to 0
                    Call mod_main.Resetautonumber("level2")
                    'whent its equal to 3 means 3 people have joined for the top guy phase 2 member to cycle out to nextphase.
                    Dim posi3 As Fastcyclepro.Position = (New cls_position).SelectThislevelid(2)
                    'get the user details
                    Dim use3 As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi3.Username)
                    'Give him 0.0398btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid
                    Select Case posi3.Username
                        Case "jasonodion", "dre"
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Guid.NewGuid.ToString, "Profit on cycling from level(2) to Level(3)", "")

                        Case "admin"
                            Dim Transid4 As String = POSTWITHDAWAL("create_withdrawal", 0.135, "1", "BTC", use3.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Transid4, "Profit on cycling from level(2) to Level(3)", "")
                        Case Else
                            Dim Transid4 As String = Guid.NewGuid.ToString
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Transid4, "Profit on cycling from level(2) to Level(3)", "")
                            Call mod_main.Savewallet("FASTCYCLEPRO", 0.135, 0, 0.135, posi3.Username, "DEPOSIT", Transid4, "Profit on cycling from level(2) to Level(3)", "")

                    End Select
   
                    'update user position, set status=0 and update earned column
                    Call mod_main.updateposition(posi3.PositionID, 0.135, "CYCLED", 3)

                    ''buy new position for him in level3 and also in level 0
                    'Dim positionid4 As String = mod_main.Getautonumber("PositionID")
                    'Call mod_main.Updateautonumber("PositionID")
                    'Call Savenewposition(posi3.Username, positionid4, 3, 0, "0.0000", "0.1", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    'buy position for him in phase 0
                    Dim po1 As String = mod_main.Getautonumber("PositionID")
                    Call mod_main.Updateautonumber("PositionID")
                    Call Savenewposition(posi3.Username, po1, 0, 0, "0.0000", "0.01", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    'add 1 to level0 autonumber
                    Call mod_main.Updateautonumber("Level0")
                    ''send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase 3
                    'Dim body5 As String = mod_main.PopulateBodynewposition(use3.Name, "#" & positionid4 & "", 3, Date.Now.ToLongDateString)
                    'Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", body5)

                    'send mail that user hve been cycled out, received btc, and upgraded nd display position number in phase 0
                    Select Case posi3.Username
                        Case "jasonodion", "dre"

                        Case Else
                            Dim bod1 As String = mod_main.PopulateBodycycled(use3.Name, "#" & po1 & "", 0, Date.Now.ToLongDateString, 0.135, use3.BitcoinwalletAddress)
                            Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", bod1)
                    End Select
                  
                    'buy position for him in blazer B
                    Dim posiblaze As String = mod_main.Getautonumber("PositionID")
                    Call mod_main.Updateautonumber("PositionID")
                    Call SavenewpositionBlazer(posi3.Username, posiblaze, "B", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    Dim bode As String = mod_main.PopulateBodynewposition(use3.Name, "#" & posiblaze & "", 0, Date.Now.ToLongDateString)
                    Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", bode)

                    Dim countblaze As String = mod_main.Getautonumber("BlazerB")
                    If countblaze = 3 Then
                        'give him 0.01 and create another position for him in blazer B again
                        'Reset BlazerB autonumber back
                        Call mod_main.Resetautonumber("BlazerB")
                        'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
                        Dim position4 As fastcyclepro.Blazer = (New cls_blazer).SelectThislevelid("B")
                        'get the user details
                        Dim use4 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(position4.Username)
                        'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

                        Dim Transid6 As String = Guid.NewGuid.ToString
                        'POSTWITHDAWAL("create_withdrawal", 0.01, "1", "BTC", use4.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                        Call mod_main.Savewallet("COINPAYMENTS", 0, 0.01, 0.01, use4.UserName, "EARNINGS", Transid6, "Profit on cycling from FastCycle Blaze", "")
                        'update user position, set status=0 and update earned column
                        Call mod_main.updatepositionblazer(position4.PositionID, 0.01, "CYCLED", 3)

                        'buy new position for him back in BlazerB
                        Dim positionid5 As String = mod_main.Getautonumber("PositionID")
                        Call mod_main.Updateautonumber("PositionID")
                        Call SavenewpositionBlazer(use4.UserName, positionid5, "B", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                        'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
                        Dim body6 As String = mod_main.PopulateBodycycled(use4.Name, "#" & positionid5 & "", "FastCycle Blaze", Date.Now.ToLongDateString, 0.01, use4.BitcoinwalletAddress)
                        Call mod_main.SendHtmlFormattedEmail(use4.Email, "", "Your New Position Added", body6)
                        Call mod_main.Updateautonumber("BlazerB")
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

                    Else
                        Call mod_main.Updateautonumber("BlazerB")
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

                    End If
                  
                Else
                    Call mod_main.Updateautonumber("Level2")
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

                End If
            Else
                Call mod_main.Updateautonumber("Level1")
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

            End If



        Else
            'First of save this new guy position and do normal thing
            'Insert position, get the referral of this person and give him 0.005btc bonus, send mail to refrral of bonus earned, send mail to this sessionuser about newly position. and now add +1 to levelone count
            Dim positionid As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            'save new position
            Call Savenewposition(Session("username"), positionid, 0, 0, "0.0000", "0.01", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'post withdrawal of position purchase
            Call mod_main.Savewallet("FASTCYLEPRO", 0, 0.01, 0.01, Session("username"), "WITHDRAWAL", Guid.NewGuid.ToString, "Position Purchase in Level(0)", "")
            'send mail to user abot newly acquired position
            Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = mod_main.PopulateBodynewposition(use.Name, "#" & positionid & "", 0, Date.Now.ToLongDateString)

            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "Your New Position Added", body)
            'get referer
            Try
                Dim ref As Fastcyclepro.Referal = (New cls_referal).SelectThisreferee(Session("username"))
                Dim refreg As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(ref.Referer)
                Dim Transid2 As String = POSTWITHDAWAL("create_withdrawal", 0.005, "1", "BTC", refreg.BitcoinwalletAddress, "1", "Referral Commissions", ConfigurationManager.AppSettings("publickey"))
                Call mod_main.Savewallet("COINPAYMENTS", 0, 0.005, 0.005, refreg.UserName, "COMMISSIONS", Transid2, "Straight Line Commisions from " & Session("username") & "'s Position Purchase", Session("username"))
                'send mail to referrer
                Dim body2 As String = mod_main.PopulateBodyreferralbonus(refreg.Name, Session("username"), 0.005 & "BTC", "Straight Line Commisions from " & Session("username") & "'s Position Purchase")

                Call mod_main.SendHtmlFormattedEmail(refreg.Email, "", "You have just earned referral commission", body2)

                'add 1 to level 0 autonumber
                Dim levelnumber As String = "Level0"
                Call mod_main.Updateautonumber(levelnumber)
                'finished here, show user its position details in the page below
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)



            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
            End Try

        End If
    End Sub

    Protected Sub btnbuyblazer_Click(sender As Object, e As EventArgs)
        'Do checkings
        If txtnumberblazer.Text = "" Then
            msgbox1.ShowError("Please write 1 in the number of positions")
            Exit Sub
        End If
        Try
            If txtnumberblazer.Text <> 1 Then
                msgbox1.ShowError("Please buy 1 position at a time")
                Exit Sub

            End If
        Catch ex As Exception
            msgbox1.ShowError("Please Insert figures only")
        End Try


        If txtwalletbalanceblazer.Text < 0.005 Then
            msgbox1.ShowError("Insufficient Balance to Purchase position, 0.005 Btc and above Required")
            Exit Sub
        End If
        ''check if the upline address is valid
        'Dim refuser As fastcyclepro.Referal = (New cls_referal).SelectThisreferee(Session("username"))
        'Dim refreguser As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(refuser.Referer)
        'If refreguser.BitcoinwalletAddress = "N/A" Then
        '    msgbox1.ShowError("Sorry, Your Referral is yet to update Bitcoin walletaddress. Please Contact your Referral, Thank you")
        '    Exit Sub
        'End If
        'check if datetime is less than repurchase time
        Dim chkdate As fastcyclepro.Blazer = (New cls_blazer).SelectThisusername(Session("username"))
        If chkdate.RepurchaseTime Is Nothing Then
            'means its a fresh position purchase user is new
        Else
            If DateTime.Now < chkdate.RepurchaseTime Then
                msgbox1.ShowError("You can't purchase another position now. Please check back In 10 mins time")
                Exit Sub
            End If
        End If



        'get next value of phase 0 in autosettings
        Dim leveltype As String = "BlazerA"
        Dim count As fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(leveltype)
        If count.Nextvalue = 3 Then
            'reset blazer a autonumber here
            Call mod_main.Resetautonumber("BlazerA")
            'First of save this new guy position in blazer and do normal thing
            Dim positionid As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            'save new position
            Call SavenewpositionBlazer(Session("username"), positionid, "A", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'post withdrawal of position purchase
            Call mod_main.Savewallet("FASTCYLEPRO", 0, 0.005, 0.005, Session("username"), "WITHDRAWAL", Guid.NewGuid.ToString, "Position Purchase in FastCycle Blaze(A)", "")
            'send mail to user abot newly acquired position
            Dim use As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = mod_main.PopulateBodynewposition(use.Name, "#" & positionid & "", "FastCycle Blaze", Date.Now.ToLongDateString)

            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "Your New Position Added", body)
            '--------CYCLING PROCESS STARTS HERE------------------
            'start cycling process for blazer A participant moving to phase 0 iin the main fcp
            Dim blazerguy As String = docycling()

            'get the user details
            Dim use1 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(blazerguy)

            'buy new position for him in BlazerB
            Dim positionid2 As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            Call SavenewpositionBlazer(use1.UserName, positionid2, "B", 0, "0.0000", 0.005, "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'send mail that user hve been cycled out...new position added in the main fcp
            Dim body3 As String = mod_main.PopulateBodynewposition(use1.Name, "#" & positionid2 & "", "FastCycle Blaze", Date.Now.ToLongDateString)

            Call mod_main.SendHtmlFormattedEmail(use1.Email, "", "Your New Position Added", body3)
            Dim count2 As String = mod_main.Getautonumber("BlazerB")
            If count2 = 3 Then
                'give him 0.01 and create another position for him in blazer B again
                'Reset BlazerB autonumber back
                Call mod_main.Resetautonumber("BlazerB")
                'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
                Dim posi2 As fastcyclepro.Blazer = (New cls_blazer).SelectThislevelid("B")
                'get the user details
                Dim use2 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi2.Username)
                'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

                Dim Transid3 As String = Guid.NewGuid.ToString
                'POSTWITHDAWAL("create_withdrawal", 0.01, "1", "BTC", use2.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                Call mod_main.Savewallet("COINPAYMENTS", 0, 0.01, 0.01, posi2.Username, "EARNINGS", Transid3, "Profit on cycling from FastCycle Blaze", "")
                Call mod_main.Savewallet("FASTCYCLEPRO", 0.01, 0, 0.01, posi2.Username, "DEPOSIT", Transid3, "Profit on cycling from FastCycle Blaze", "")

                'update user position, set status=0 and update earned column
                Call mod_main.updatepositionblazer(posi2.PositionID, 0.01, "CYCLED", 3)

                'buy new position for him back in BlazerB
                Dim positionid3 As String = mod_main.Getautonumber("PositionID")
                Call mod_main.Updateautonumber("PositionID")
                Call SavenewpositionBlazer(posi2.Username, positionid3, "B", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
                Dim body4 As String = mod_main.PopulateBodycycled(use2.Name, "#" & positionid3 & "", "FastCycle Blaze", Date.Now.ToLongDateString, 0.01, use2.BitcoinwalletAddress)
                Call mod_main.SendHtmlFormattedEmail(use2.Email, "", "Your New Position Added", body4)
                Call mod_main.Updateautonumber("BlazerB")
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

            Else
                Call mod_main.Updateautonumber("BlazerB")
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

            End If




        Else
            'First of save this new guy position in blazer and do normal thing
            Dim positionid As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            'save new position
            Call SavenewpositionBlazer(Session("username"), positionid, "A", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'post withdrawal of position purchase
            Call mod_main.Savewallet("FASTCYLEPRO", 0, 0.005, 0.005, Session("username"), "WITHDRAWAL", Guid.NewGuid.ToString, "Position Purchase in FastCycle Blaze(A)", "")
            'send mail to user abot newly acquired position
            Dim use As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = mod_main.PopulateBodynewposition(use.Name, "#" & positionid & "", "FastCycle Blaze", Date.Now.ToLongDateString)

            Call mod_main.SendHtmlFormattedEmail(use.Email, "", "Your New Position Added", body)
            'get referer
            Try

                'add 1 to level blazer A autonumber
                Dim levelnumber As String = "BlazerA"
                Call mod_main.Updateautonumber(levelnumber)
                'finished here, show user its position details in the page below
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)



            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Function docycling() As String
        'whent its equal to 3 means 3 people have joined for the top guy phase 0 member to cycle out to nextphase.
        Dim posi As fastcyclepro.Blazer = (New cls_blazer).SelectThislevelid("A")
        'get the user details
        Dim use1 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi.Username)

        'update user position, set status=0 and update earned column
        Call mod_main.updatepositionblazer(posi.PositionID, 0, "CYCLED", 3)
        'start position purchase in the main fcp and upgrade the top guy to blazer B wit 0.005 upgrade

        'buy new position for him in level0 The main fcp
        Dim positionid2 As String = mod_main.Getautonumber("PositionID")
        Call mod_main.Updateautonumber("PositionID")
        Call Savenewposition(posi.Username, positionid2, 0, 0, "0.0000", 0.01, "ACTIVE", Date.Now, Guid.NewGuid.ToString)
        'send mail that user hve been cycled out...new position added in the main fcp
        Dim body3 As String = mod_main.PopulateBodynewposition(use1.Name, "#" & positionid2 & "", 0, Date.Now.ToLongDateString)

        Call mod_main.SendHtmlFormattedEmail(use1.Email, "", "Your New Position Added", body3)

        'check if level(1) autonumber is 3(means if 3 people have joined)
        Dim count2 As String = mod_main.Getautonumber("Level0")
        If count2 = 3 Then
            'start cycling process for level0 members

            'Reset level(0) autonumber back to 0
            Call mod_main.Resetautonumber("level0")
            'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
            Dim posi2 As fastcyclepro.Position = (New cls_position).SelectThislevelid(0)
            'get the user details
            Dim use2 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi2.Username)
            'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

            Dim Transid3 As String = Guid.NewGuid.ToString
            'POSTWITHDAWAL("create_withdrawal", 0.005, "1", "BTC", use2.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.005, 0.005, posi2.Username, "EARNINGS", Transid3, "Profit on cycling from level(0) to Level(1)", "")
            Call mod_main.Savewallet("FASTCYCLEPRO", 0.005, 0, 0.005, posi2.Username, "EARNINGS", Transid3, "Profit on cycling from level(0) to Level(1)", "")

            'update user position, set status=0 and update earned column
            Call mod_main.updateposition(posi2.PositionID, 0.005, "CYCLED", 3)

            'buy new position for him in level1
            Dim positionid3 As String = mod_main.Getautonumber("PositionID")
            Call mod_main.Updateautonumber("PositionID")
            Call Savenewposition(posi2.Username, positionid3, 1, 0, "0.0000", "0.02", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
            'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
            Dim body4 As String = mod_main.PopulateBodycycled(use2.Name, "#" & positionid3 & "", 1, Date.Now.ToLongDateString, 0.005, use2.BitcoinwalletAddress)
            Call mod_main.SendHtmlFormattedEmail(use2.Email, "", "Your New Position Added", body4)

            'check if level(1) autonumber is 3(means if 3 people have joined)
            Dim counta As String = mod_main.Getautonumber("Level1")
            If counta = 3 Then
                'start cycling process for level1 members

                'Reset level(0) autonumber back to 0
                Call mod_main.Resetautonumber("level1")
                'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
                Dim posia As fastcyclepro.Position = (New cls_position).SelectThislevelid(1)
                'get the user details
                Dim usea As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posia.Username)
                'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

                Dim Transida As String = Guid.NewGuid.ToString
                'POSTWITHDAWAL("create_withdrawal", 0.01, "1", "BTC", usea.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                Call mod_main.Savewallet("COINPAYMENTS", 0, 0.01, 0.01, posia.Username, "EARNINGS", Transida, "Profit on cycling from level(1) to Level(2)", "")
                Call mod_main.Savewallet("DEPOSIT", 0, 0.01, 0.01, posia.Username, "DEPOSIT", Transida, "Profit on cycling from level(1) to Level(2)", "")

                'update user position, set status=0 and update earned column
                Call mod_main.updateposition(posia.PositionID, 0.01, "CYCLED", 3)

                'buy new position for him in level2
                Dim positionid4 As String = mod_main.Getautonumber("PositionID")
                Call mod_main.Updateautonumber("PositionID")
                Call Savenewposition(usea.UserName, positionid4, 2, 0, "0.0000", "0.05", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
                Dim body5 As String = mod_main.PopulateBodycycled(usea.Name, "#" & positionid4 & "", 2, Date.Now.ToLongDateString, 0.01, usea.BitcoinwalletAddress)
                Call mod_main.SendHtmlFormattedEmail(usea.Email, "", "Your New Position Added", body5)
                'continue cycling process to 2
                Dim count3 As String = mod_main.Getautonumber("Level2")
                If count3 = 3 Then
                    'start cycling process for level2
                    'Reset level(0) autonumber back to 0
                    Call mod_main.Resetautonumber("level2")
                    'whent its equal to 3 means 3 people have joined for the top guy phase 2 member to cycle out to nextphase.
                    Dim posi3 As fastcyclepro.Position = (New cls_position).SelectThislevelid(2)
                    'get the user details
                    Dim use3 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(posi3.Username)
                    'Give him 0.0398btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid
                    Select Case posi3.Username
                        Case "jasonodion", "dre"
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Guid.NewGuid.ToString(), "Profit on cycling from level(2) to Level(3)", "")
                        Case "admin"
                            Dim Transid4 As String = POSTWITHDAWAL("create_withdrawal", 0.135, "1", "BTC", use3.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Transid4, "Profit on cycling from level(2) to Level(3)", "")
                        Case Else
                            Dim transid4 As String = Guid.NewGuid.ToString
                            Call mod_main.Savewallet("COINPAYMENTS", 0, 0.135, 0.135, posi3.Username, "EARNINGS", Transid4, "Profit on cycling from level(2) to Level(3)", "")
                            Call mod_main.Savewallet("FASTCYCLEPRO", 0.135, 0, 0.135, posi3.Username, "DEPOSIT", transid4, "Profit on cycling from level(2) to Level(3)", "")

                    End Select
         
                    'update user position, set status=0 and update earned column
                    Call mod_main.updateposition(posi3.PositionID, 0.135, "CYCLED", 3)

                    ''buy new position for him in level3 and also in level 0
                    'Dim positionid4 As String = mod_main.Getautonumber("PositionID")
                    'Call mod_main.Updateautonumber("PositionID")
                    'Call Savenewposition(posi3.Username, positionid4, 3, 0, "0.0000", "0.1", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    'buy position for him in phase 0
                    Dim po1 As String = mod_main.Getautonumber("PositionID")
                    Call mod_main.Updateautonumber("PositionID")
                    Call Savenewposition(posi3.Username, po1, 0, 0, "0.0000", "0.010", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    'add 1 to level0 autonumber
                    Call mod_main.Updateautonumber("Level0")
                    ''send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase 3
                    'Dim body5 As String = mod_main.PopulateBodynewposition(use3.Name, "#" & positionid4 & "", 3, Date.Now.ToLongDateString)
                    'Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", body5)

                    'send mail that user hve been cycled out, received btc, and upgraded nd display position number in phase 0
                    Select Case posi3.Username
                        Case "jasonodion", "dre"
                      
                        Case Else
                           Dim bod1 As String = mod_main.PopulateBodycycled(use3.Name, "#" & po1 & "", 0, Date.Now.ToLongDateString, 0.135, use3.BitcoinwalletAddress)
                            Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", bod1)
                    End Select
                  
                    'buy position for him in blazer B
                    Dim posiblaze As String = mod_main.Getautonumber("PositionID")
                    Call mod_main.Updateautonumber("PositionID")
                    Call SavenewpositionBlazer(posi3.Username, posiblaze, "B", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                    Dim bode As String = mod_main.PopulateBodynewposition(use3.Name, "#" & posiblaze & "", "Fastcycle Blaze", Date.Now.ToLongDateString)
                    Call mod_main.SendHtmlFormattedEmail(use3.Email, "", "Your New Position Added", bode)

                    Dim countblaze As String = mod_main.Getautonumber("BlazerB")
                    If countblaze = 3 Then
                        'give him 0.01 and create another position for him in blazer B again
                        'Reset BlazerB autonumber back
                        Call mod_main.Resetautonumber("BlazerB")
                        'whent its equal to 3 means 3 people have joined for the top guy phase 1 member to cycle out to nextphase.
                        Dim position4 As fastcyclepro.Blazer = (New cls_blazer).SelectThislevelid("B")
                        'get the user details
                        Dim use4 As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(position4.Username)
                        'Give him 0.01btc on cycling, the code belwo withdraws 0.01 btc from central wallet and get transid

                        Dim Transid6 As String = Guid.NewGuid.ToString
                        'POSTWITHDAWAL("create_withdrawal", 0.01, "1", "BTC", use4.BitcoinwalletAddress, "1", "Profit On Cycling", ConfigurationManager.AppSettings("publickey"))
                        Call mod_main.Savewallet("COINPAYMENTS", 0, 0.01, 0.01, use4.UserName, "EARNINGS", Transid6, "Profit on cycling from FastCycle Blaze", "")
                        Call mod_main.Savewallet("FASTCYCLEPRO", 0, 0.01, 0.01, use4.UserName, "DEPOSIT", Transid6, "Profit on cycling from FastCycle Blaze", "")

                        'update user position, set status=0 and update earned column
                        Call mod_main.updatepositionblazer(position4.PositionID, 0.01, "CYCLED", 3)

                        'buy new position for him back in BlazerB
                        Dim positionid5 As String = mod_main.Getautonumber("PositionID")
                        Call mod_main.Updateautonumber("PositionID")
                        Call SavenewpositionBlazer(use4.UserName, positionid5, "B", 0, "0.0000", "0.005", "ACTIVE", Date.Now, Guid.NewGuid.ToString)
                        'send mail that user hve been cycled out, received btc, and upgraded nd display position number in new phase
                        Dim body6 As String = mod_main.PopulateBodycycled(use4.Name, "#" & positionid5 & "", "FastCycle Blaze", Date.Now.ToLongDateString, 0.01, use4.BitcoinwalletAddress)
                        Call mod_main.SendHtmlFormattedEmail(use4.Email, "", "Your New Position Added", body6)
                        Call mod_main.Updateautonumber("BlazerB")
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

                    Else
                        Call mod_main.Updateautonumber("BlazerB")
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Position Purchased Successfully!');window.location='My_positions.aspx';", True)

                    End If
                Else
                    Call mod_main.Updateautonumber("Level2")

                End If
            Else
                Call mod_main.Updateautonumber("Level1")

            End If
        Else
            Call mod_main.Updateautonumber("Level0")

        End If
        Return posi.Username
    End Function

    Protected Sub ddllevel_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case ddllevel.SelectedValue
            Case "fcpblazer"
                pnlblazer.Visible = True
                pnlfcp.Visible = False
            Case "level0"
                pnlblazer.Visible = False
                pnlfcp.Visible = True
        End Select
    End Sub
End Class
