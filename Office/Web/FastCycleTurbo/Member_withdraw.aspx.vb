﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments
Partial Class Office_Web_FastCycleTurbo_Member_withdraw
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Session("profit") Is Nothing Then
                Response.Redirect("/office/web/fastcycleTurbo/dashboard")
            Else
                lblbalance.Text = FormatNumber(CDbl(Session("profit")), 8)
                Dim rec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
                lblbitcoinaddress.Text = rec.BitcoinwalletAddress
            End If
           
        End If
    End Sub

    Protected Sub btnwithdraw_Click(sender As Object, e As EventArgs)
        Try

            Dim transid As String = POSTTURBOWITHDAWAL("create_withdrawal", lblbalance.Text, "1", "BTC", lblbitcoinaddress.Text, "1", "FastcycleTurbo Withdrawals", ConfigurationManager.AppSettings("turbopublickey"))
            Call mod_main.Saveturbowallet("COINPAYMENTS", 0, lblbalance.Text, 0, 0, lblbalance.Text, Session("username"), "WITHDRAWAL", transid, "FastcycleTurbo Withdrawal by " & Session("username") & "", "")

            Session("profit") = ""
            'send mail to user saying withdraw has been processed
            Dim userrec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim body As String = mod_main.PopulateBodyprocessed(userrec.Name, Date.Now.ToLongDateString, lblbalance.Text & " Btc", userrec.BitcoinwalletAddress)
            Call mod_main.SendHtmlFormattedEmail(userrec.Email, "", "Your Withdrawal is being Processed!", body)

            If checkcurbalance(Session("username")) = True Then
                'If true it means that the guy balance is less than 0.0005 and it should start fresh deposit set active deposit to 0(not active)
                Dim a As New cls_turboTurboWallet
                Dim rec As List(Of fastcyclepro.TurboWallet) = a.Selectuserdeposit(Session("username"))
                For Each record In rec
                    record.Active = 0
                    Dim res As ResponseInfo = a.Update(record)
                    If res.ErrorCode = 0 Then
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Withdrawal is being processed, you will get an email shortly!');window.location='Dashboard.aspx';", True)
                    Else
                        msgbox1.ShowError(res.ErrorMessage)
                    End If

                Next
            Else
                'continue count but set lastbalace count back to 0.0 and last updated date
                Dim a As New cls_turboTurboWallet
                Dim rec As List(Of fastcyclepro.TurboWallet) = a.Selectuser(Session("username"))
                For Each record In rec
                    record.LastUpdatedBalance = 0.0
                    record.Lastupdate = Date.Now
                    Dim res As ResponseInfo = a.Update(record)
                    If res.ErrorCode = 0 Then
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Withdrawal is being processed, you will get an email shortly');window.location='Dashboard.aspx';", True)
                    Else
                        msgbox1.ShowError(res.ErrorMessage)
                    End If

                Next
                'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Transaction Successfull!');window.location='Dashboard.aspx?A=.';", True)

            End If
        Catch ex As Exception
            msgbox1.ShowError(ex.Message)
            Exit Sub
        End Try
    
        'Check if this is the person final withdrawal so when next he deposit, it doesnt addup t should start fresh deposit.
        'check if current balance is < 0.001
        'make the profit empty incasea malicious user refreshes the page 
     

    End Sub

    Private Function checkcurbalance(ByVal username As String) As Boolean
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbocurrentbalance"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("totalBAL") IsNot DBNull.Value Then
                        Try
                            If dr2("totalBAL") < 0.0002 Then
                                Return True
                            Else
                                Return False
                            End If

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        msgbox1.ShowError("error occured")

                    End If


                Else

                    msgbox1.ShowError("error occured")
                End If


            End Using
        End Using
    End Function
End Class
