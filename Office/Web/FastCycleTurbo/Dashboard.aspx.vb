﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class Office_Web_CloudMinr_Dashboard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
 
        If Not Page.IsPostBack Then
            If Session("username") = "" Then

                Dim OriginalUrl As String = HttpContext.Current.Request.RawUrl
                Dim LoginPageUrl As String = "/office/signin"
                HttpContext.Current.Response.Redirect([String].Format("{0}?goto={1}", LoginPageUrl, OriginalUrl))

            End If

            Dim rec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            'If rec.UserName <> "admin" Then
            '    btnwithdraw.Enabled = False
            'End If

            Dim lastaccess As Date = rec.LastAccess
            Dim regdate As Date = rec.CreatedOn
            lblemail.Text = rec.Email
            lbljoindate.Text = regdate.ToLongDateString
            lbllastaccess.Text = lastaccess.ToLongDateString
            lblcurrentIP.Text = getIpAddressAddress()
            lblusername.Text = String.Format("{0}?ref={1}", ConfigurationManager.AppSettings("pathtorefredirect"), Session("username"))
            'get current btc rate
            Try

                Dim result = mod_main.converttobtc(1)
                lblbtcprice.Text = "$" & FormatNumber(1 / result, 2)
            Catch ex As Exception
                lblbtcprice.Text = 0
            End Try


            'start binding
            bindannouncement()

            Binddeposit(Session("username"))
            Bindcurbalance(Session("username"))
            Bindstep(Session("username"))
            Bindlastdeposit(Session("username"))
            Bindlastwithdrawal(Session("username"))
            Bindtotalwithdrawal(Session("username"))
            Bindtotaldeposit(Session("username"))
            Bindtotalcommissions(Session("username"))

            'calculate the time frame
            Dim deposit As Double = lbldeposit.Text
            If deposit > 0.0 Then
                'get time frame to continue count
                'update the balance to the new value then begin counter from there
                Dim B As New cls_turboTurboWallet
                Dim startvalue As String = gettimespan(Session("username"), deposit)
                Dim userec As List(Of fastcyclepro.TurboWallet) = B.Selectuser(Session("username"))
                For Each records In userec
                    records.LastUpdatedBalance = startvalue
                    records.Lastupdate = Date.Now
                    Dim res As ResponseInfo = B.Update(records)

                Next
                Dim curbal As Double = txtcurbalance.Text
                'incase if the counter has reach the current balance it should not surpass it
                If startvalue > curbal Then
                    startcounter.Value = curbal
                Else
                    startcounter.Value = FormatNumber(CDbl(startvalue), 8)
                End If

            Else
                startcounter.Value = 0.0
                txtcurbalance.Text = 0.0
            End If

        End If

    End Sub

    Public Shared Function getIpAddressAddress() As String
        Dim IpAddress As String = String.Empty
        IpAddress = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If IpAddress = "" Or IpAddress Is Nothing Then
            IpAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        End If

        Return IpAddress
    End Function

    Public Shared Function gettimespan(ByVal username As String, ByVal depositamt As String) As String
        Dim minutes As TimeSpan
        'get per minute of the guys deposit
        Dim depo As Double = depositamt
        Dim result As Double = 3.7 / 100 * depo / 1440

        Dim rec As fastcyclepro.TurboWallet = (New cls_turboTurboWallet).Selectuserlastupdated(username)
        Dim todaysminutes As Date = Date.Now
        Dim lastupdate As Date = rec.Lastupdate
        minutes = todaysminutes - lastupdate
        Dim lastbalance As Double = rec.LastUpdatedBalance
        Dim finalminutes As Double = minutes.TotalMinutes
        Dim result2 As Double = finalminutes * result
        Dim final As Double = lastbalance + result2

        Return final
    End Function

    Protected Sub btnwithdraw_Click(sender As Object, e As EventArgs)
        If Hidden1.Value < 0.0002 Then
            msgbox1.ShowError("Minimum Withdrawal is 0.0002 BTC!")
            Exit Sub
        Else
            Dim A As New fastcyclepro.FastcycleproDataContext
            Dim rec = A.checkturboreferer(Session("username")).ToList
            If rec.Count = 0 Then
                msgbox1.ShowError("This is your first withdrawal and you need at least a referral before you can withdraw. This rule is applied to First withdrawals Only!")
                Exit Sub
            End If
            Dim profit As Double = FormatNumber(CDbl(Hidden1.Value), 8)
            Session("profit") = profit

            Response.Redirect("/office/web/fastcycleturbo/member_withdraw.aspx")
        End If

    End Sub

    Private Sub Binddeposit(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbosumdeposit"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Totaldeposit") IsNot DBNull.Value Then
                        Try

                            lbldeposit.Text = dr2("Totaldeposit")
                            lblactivedeposit.Text = dr2("Totaldeposit")
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbldeposit.Text = "0.00000000"
                        lblactivedeposit.Text = "0.00000000"
                    End If


                Else
                    lblactivedeposit.Text = "0.00000000"
                    lbldeposit.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub

    Private Sub Bindcurbalance(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbocurrentbalance"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("totalBAL") IsNot DBNull.Value Then
                        Try

                            txtcurbalance.Text = dr2("totalBAL")
                            lbltotalbalance.Text = dr2("totalBAL")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        txtcurbalance.Text = "0.00000000"
                        lbltotalbalance.Text = "0.00000000"
                    End If


                Else

                    txtcurbalance.Text = "0.00000000"
                    lbltotalbalance.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub
    Private Sub Bindstep(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbosumstep"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Totalstep") IsNot DBNull.Value Then
                        Try

                            txtstep.Text = dr2("Totalstep")
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        txtstep.Text = "0.00000000"

                    End If


                Else

                    txtstep.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub

    Private Sub Bindlastdeposit(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbolastdeposit"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Deposit") IsNot DBNull.Value Then
                        Try

                            lbllastdeposit.Text = dr2("Deposit")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbllastdeposit.Text = "0.00000000"
                    End If


                Else

                    lbllastdeposit.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub

    Private Sub Bindlastwithdrawal(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbolastwithdrawal"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Withdrawal") IsNot DBNull.Value Then
                        Try

                            lbllastwithdrawal.Text = dr2("Withdrawal")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbllastwithdrawal.Text = "0.00000000"
                    End If


                Else

                    lbllastwithdrawal.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub

    Private Sub Bindtotalwithdrawal(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbosumwithdrawal"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Totalwithdrawal") IsNot DBNull.Value Then
                        Try

                            lbltotalwithdrawal.Text = dr2("Totalwithdrawal")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbltotalwithdrawal.Text = "0.00000000"
                    End If


                Else

                    lbltotalwithdrawal.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub
    Private Sub Bindtotalcommissions(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbosumcommissions"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Totalcommissions") IsNot DBNull.Value Then
                        Try

                            lbltotalaffiliate.Text = dr2("Totalcommissions")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbltotalaffiliate.Text = "0.00000000"
                    End If


                Else

                    lbltotalaffiliate.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub
    Private Sub Bindtotaldeposit(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Getturbosumdepositall"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Totaldeposit") IsNot DBNull.Value Then
                        Try

                            lbltotaldeposit.Text = dr2("Totaldeposit")

                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lbltotaldeposit.Text = "0.00000000"
                    End If


                Else

                    lbltotaldeposit.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub

    Private Sub bindannouncement()
        Dim A As New Fastcyclepro.FastcycleproDataContext
        Dim announcement = A.GetAnnouncement
        For Each record In announcement
            announce.InnerHtml = record.Announcement
        Next
    End Sub
End Class
