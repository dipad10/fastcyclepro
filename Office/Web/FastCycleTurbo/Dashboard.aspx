﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Office_Web_CloudMinr_Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <header class="page-header col-md-12">
            <h3>FCP Turbo Dashboard</h3>

        </header>
        <%-- announcement section --%>


        

        <section>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <h3 class="panel-title text-capitalize"><i class="fa fa-bell"></i>Latest Announcements</h3>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <a href="#panel-drop-5" class="pull-right" data-toggle="collapse"><i class="fa fa-angle-down fa-lg fa-inverse"></i></a>
                        </div>
                    </div>
                </div>
                <div id="panel-drop-5" class="collapse in">
                    <div class="panel-body">
                        <p runat="server" id="announce" class="mrgn-all-none fw-bold"></p>
                    </div>
                </div>
            </div>
        </section>
        <br />
        <br />
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <div style="box-shadow: 1px 3px 50px 3px gray;" class="panel panel-white">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
                          <h3 class="panel-title text-capitalize"><i class="fa fa-money"></i> Active Deposit:</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div style="background-color:#ddffff!important;" class="panel-body">
                        <h2 class="mrgn-all-md fw-bold text-center"><i class="fa fa-bitcoin"></i> <asp:Label ID="lbldeposit" runat="server" Text=""></asp:Label></h2>
                        <br />
                        <p class="text-center"><a style="width:165px;" class="btn btn-lg fw-semibold btn-primary" href="Member_deposit.aspx">DEPOSIT</a> </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <div style="box-shadow: 1px 3px 50px 3px gray;"" class="panel panel-white">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
                          <h3 class="panel-title text-capitalize"><i class="fa fa-credit-card"></i> Your Current Balance:</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div style="background-color:#ddffff!important;" class="panel-body">
                        <h2 class="mrgn-all-md fw-bold text-center"><i class="fa fa-bitcoin"></i><label class="profit" id="profitbalance" form="form1" runat="server" for="lblprofit"></label> </h2>
                        <br />
                       <asp:HiddenField ID="startcounter" runat="server" />
                   <asp:HiddenField ID="Hidden1" runat="server" />
                        <asp:TextBox ID="txtcurbalance" style="display:none;" runat="server"></asp:TextBox>
                       
                        <asp:TextBox ID="txtstep" style="display:none;" runat="server"></asp:TextBox>
                        <p class="text-center">
                            <asp:Button ID="btnwithdraw" OnClientClick ="passValue()" OnClick="btnwithdraw_Click" Width="165" CssClass="btn btn-lg fw-semibold btn-primary" runat="server" Text="WITHDRAW" />
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="w3-container w3-pale-blue w3-leftbar w3-border-blue">
                                <p style="margin-top:13px;" class="panel-title text-center font-sm text-capitalize"><i class="fa fa-user"></i> <asp:Label ID="lblusername" runat="server" Text="Label"></asp:Label></p>
                            </div>

                        </div>
                    </div>
                    <div class="panel-body">
                       <table>
                           <tbody>
                               <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Current Bitcoin Price:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">1 BTC = <asp:Label ID="lblbtcprice" runat="server" Text=""></asp:Label></span></td>
                               </tr>
                                <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Email:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lblemail" runat="server" Text="Label"></asp:Label></span></td>
                               </tr>
                                 <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Join Date:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lbljoindate" runat="server" Text="Label"></asp:Label></span></td>
                               </tr>
                                 <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">LastAccess:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lbllastaccess" runat="server" Text="Label"></asp:Label></span></td>
                               </tr>
                                
                                 <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Current IP:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lblcurrentIP" runat="server" Text=""></asp:Label></span></td>
                               </tr>
                              
                                 <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Active Deposit:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lblactivedeposit" runat="server" Text=""></asp:Label> BTC</span></td>
                               </tr>
                                <tr>
                                   <td><span style="color:#758494;" class="fw-bold font-xs">Total Balance:</span></td>
                                    <td><span style="color:#758494;" class="font-xs">
                                        <asp:Label ID="lbltotalbalance" runat="server" Text=""></asp:Label> BTC</span></td>
                               </tr>
                               <tr style="text-align:center;">
                                  
                                   <td>
                                       <a href="Member_deposit.aspx" class="btn btn-outline-primary" type="button"><i class="fa fa-arrow-circle-down"></i> Deposit</a>
                                   </td>
                                    <td>
                                     
                                       <button runat="server" OnClientClick ="passValue()" onserverclick="btnwithdraw_Click" class="btn btn-outline-primary" type="button"><i class="fa fa-arrow-circle-up"></i> Withdraw</button>
                                   </td>
                               </tr>
                           </tbody>
                       </table>
                    </div>
                </div>
            </div>
        </div>
         <%-- CARDS SECTION --%>
        <div class="row prtm-card-wrapper">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box graph bg-primary pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Last Deposit</span></div>
                            <%-- <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblindollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">

                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lbllastdeposit" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>


            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box order bg-success pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Deposit</span> </div>
                            <%--  <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblprofitdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lbltotaldeposit" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>


            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box invoices bg-facebook pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Last withdrawal</span> </div>
                            <%--   <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblrefdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lbllastwithdrawal" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box invoices bg-youtube pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total withdrawal</span> </div>
                            <%--   <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblrefdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lbltotalwithdrawal" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                   
                    </div>
                </div>

            </div>

              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 half-col-lg-md">
                <div class="prtm-card prtm-card-box invoices bg-youtube pad-all-md mrgn-b-lg">
                    <div class="prtm-card-info">
                        <div class="mrgn-b-lg clearfix">
                            <div class="pull-left"><span class="show">Total Affiliate Commissions</span> </div>
                            <%--   <div class="pull-right text-right">
                                <span class="font-counter">
                                    <i class="fa fa-dollar"></i>
                                    <asp:Label ID="lblrefdollars" runat="server" Text=""></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div class="prtm-card-progress">
                            <i class="fa fa-bitcoin"></i>
                            <asp:Label ID="lbltotalaffiliate" CssClass="fw-bold" runat="server" Text=""></asp:Label><b> BTC</b>

                        </div>
                   
                    </div>
                </div>

            </div>
        </div>
        
        <br />

    </form>
    
    <style>
        .w3-border-blue, .w3-hover-border-blue:hover {
            border-color: #2196F3 !important;
        }

        .w3-pale-blue, .w3-hover-pale-blue:hover {
            color: #000 !important;
            background-color: #ddffff !important;
        }

        .w3-leftbar {
            border-left: 6px solid #ccc !important;
        }

        .w3-container, .w3-panel {
            padding: 0.01em 16px;
            height:40px;
        }
    </style>
    <script type="text/javascript">
        $('#counter-block').ready(function () {
           
            $('.profit').animationCounter({
                start: parseFloat(document.getElementById("<%=startcounter.ClientID%>").value),
                step: parseFloat(document.getElementById("<%=txtstep.ClientID%>").value),
                delay: 1500,
                end: parseFloat(document.getElementById("<%=txtcurbalance.ClientID%>").value),
            });

        });
    </script>
     <script type ="text/javascript">
         function passValue() {
             var value1 = parseFloat(document.getElementById("<%=profitbalance.ClientID%>").innerText)
             document.getElementById('<%=Hidden1.ClientID%>').value = value1
                }
                window.onload = passValue;

            </script>
</asp:Content>

