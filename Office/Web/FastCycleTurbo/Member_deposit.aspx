﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Member_deposit.aspx.vb" Inherits="Office_Web_FastCycleTurbo_Member_deposit" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Make Deposit</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Fastcycleturbo</a></li>

                <li class="breadcrumb-item active"><a href="#">Make Deposit</a></li>
            </ul>
        </div>
        
        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">MAKE DEPOSIT</h3>
                                    <hr />
                                    <span class="font-xs"> <i class="fa fa-info-circle"></i> Please note all deposit transactions fees Inclusive(2%)</span>
                                </div>

                            </div>
                              <asp:Panel ID="Panel1" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Make Deposit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td style="color:#18ba60;" width="20%">Amount <i class="fa fa-bitcoin"></i></td>
                                                <td><span>
                                                    <asp:TextBox onchange="MultiplyByHidden()" onkeyup="MultiplyByHidden()" Height="50" placeholder="Ex: 0.005, 0.015, 0.030" data-toggle="tooltip" data-placement="top"  data-original-title="Enter the amount you wish to deposit E.g 0.005, 0.015, 0.030." onkeypress="MultiplyByHidden(); return numberOnly(this, event)" ID="txtamount" CssClass="form-control font-2x" runat="server"></asp:TextBox>
                                                    <br />
                                                      <span style="color:#758494;" class="font-xs fw-bold">Transaction Fees: </span> <span style="color:#758494;" class="font-xs fw-bold"> +<asp:Label ID="lbltransactionfees" runat="server" Text="0.00000000"></asp:Label></span>
                                                    <asp:TextBox ID="txttransfees" style="display:none;" runat="server"></asp:TextBox>
                                                </span></td>
                                            </tr>
                                            <tr>

                                                <td style="color:#18ba60;" class="font-xs fw-bold">Hourly Interest</td>
                                                <td style="color:#18ba60;" class="font-xs fw-bold"><span> <asp:Label ID="lblhourlyInterest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                                            </tr>
                                             <tr>

                                                <td style="color:#18ba60;" class="font-xs fw-bold">Daily Interest</td>
                                                <td style="color:#18ba60;" class="font-xs fw-bold"><span> <asp:Label ID="lbldailyInterest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                                            </tr>
                                             <tr>

                                                <td style="color:#18ba60;" class="font-xs fw-bold">Weekly Interest</td>
                                                <td style="color:#18ba60;" class="font-xs fw-bold"><span> <asp:Label ID="LblweeklyIntrest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                                            </tr>
                                              <tr>

                                              
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                  
                                </div>

                                    <div style="text-align:center;" class="form-group center">
                                           <p class=" font-xs fw-semibold">Please Beware: Every Deposit address is only valid for ONE Deposit</p>
                                        <asp:Button ID="btnfund" Width="200" OnClick="btnfund_Click" CssClass="btn bg-green text-center btn-primary" runat="server" Text="Make a Deposit" />
                                    </div>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" Visible="false" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Make Deposit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td class="fw-bold" style="color:#758494;" width="20%">Please Send: </td>
                                                <td class="fw-bold font-2x" style="color:#758494;"><span>
                                                    <asp:label ID="lblsendamt" style="color:#758494;" runat="server"></asp:label> BTC</span></td>
                                            </tr>
                                            <tr>

                                                <td style="color:#758494;" class="fw-bold"><span class="font-xs fw-bold"></span>To address:</td>
                                                <td style="color:#758494;" class="fw-bold"><span> <asp:label ID="lbladdress" CssClass="fw-bold font-2x" runat="server"></asp:label></span>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                     
                                </div>
                             
                                    <div style="text-align:center;" class="form-group center">
                                     
                                        <a href="Dashboard.aspx" class="btn btn-outline-primary btn-rounded">Mark Payment as complete</a>
                                    </div>
                            </asp:Panel>
                        </div>
                      
                          
                            
                         
                    </div>
                </div>

            </div>
        </div>
         </form>
    <%-- block letters on typing btc amount --%>

    <script type="text/javascript">
        function numberOnly(txt, e) {
            var arr = "0123456789.";
            var code;
            if (window.event)
                code = e.keyCode;
            else
                code = e.which;
            var char = keychar = String.fromCharCode(code);
            if (arr.indexOf(char) == -1)
                return false;
            else if (char == ".")
                if (txt.value.indexOf(".") > -1)
                    return false;
        }
    </script>

    <%-- calculate hourly daily weekly intrest --%>
    <script type="text/javascript">
        function MultiplyByHidden() {

            var value1 = parseFloat(document.getElementById("<%=txtamount.ClientID%>").value)
            var val1 = 3.7 / 100 * value1 / 24
            var val2 = 3.7 / 100 * value1
            var val3 = 3.7 / 100 * value1 * 7
            var val4 = 2 / 100 * value1
            document.getElementById("<%=lblhourlyInterest.ClientID%>").innerText = val1.toFixed(8)
            document.getElementById("<%=lbldailyInterest.ClientID%>").innerText = val2.toFixed(8)
            document.getElementById("<%=LblweeklyIntrest.ClientID%>").innerText = val3.toFixed(8)
            document.getElementById("<%=lbltransactionfees.ClientID%>").innerText = val4.toFixed(8)
            document.getElementById("<%=txttransfees.ClientID%>").value = val4.toFixed(8)

                    }
</script>
</asp:Content>

