﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports libCoinPaymentsNET
Imports libCoinPaymentsNET.CoinPayments
Partial Class Office_Web_FastCycleTurbo_Member_deposit
    Inherits System.Web.UI.Page

    Protected Sub btnfund_Click(sender As Object, e As EventArgs)

        Try
            If txtamount.Text < 0.005 Then
                msgbox1.ShowError("Mininimum Deposit for Fastcycle Turbo is 0.005 BTC")
                Exit Sub

            End If
        Catch ex As Exception
            msgbox1.ShowError("Please Insert Figures")
            Exit Sub
        End Try

        If txtamount.Text = "" Then
            msgbox1.ShowError("Please Enter an amount to Deposit")
            Exit Sub
        End If

        Dim bitrec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
        If bitrec.BitcoinwalletAddress = "N/A" Then
            msgbox1.ShowError("Please Update your Bitcoin Wallet address before you proceed")
            Exit Sub
        End If
        Try
            Dim amt As Double = Me.txtamount.Text
            Dim transfee As Double = Me.txttransfees.Text
            Dim total As Double = amt + transfee
            lblsendamt.Text = total
            Dim use As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim address As String = createtransaction("create_transaction", "1", total, "BTC", "BTC", use.Email, use.Name, "Fastcycle Turbo Deposit by " & use.UserName & "", Guid.NewGuid.ToString(), Me.txttransfees.Text, use.UserName, ConfigurationManager.AppSettings("turbopublickey"))
            lbladdress.Text = address
            Panel1.Visible = False
            Panel2.Visible = True
        Catch ex As Exception
            msgbox1.ShowError(ex.Message)
        End Try


    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Not Page.IsPostBack Then
        '    Dim rec As fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
        '    If rec.UserName <> "admin" Then
        '        btnfund.Enabled = False
        '    End If
        'End If
    End Sub
End Class
