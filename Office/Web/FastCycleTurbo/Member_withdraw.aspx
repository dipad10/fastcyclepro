﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Member_withdraw.aspx.vb" Inherits="Office_Web_FastCycleTurbo_Member_withdraw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Make Withdraw</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Fastcycleturbo</a></li>

                <li class="breadcrumb-item active"><a href="#">Make Withdraw</a></li>
            </ul>
        </div>
        
        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">MAKE WITHDRAW</h3>
                                    <hr />
                                </div>

                            </div>
                              <asp:Panel ID="Panel1" runat="server">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-middle table-hover">
                                        <thead>
                                            <tr class="bg-primary">

                                                <th style="text-align: center;" colspan="2">Make Withdraw</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                              <tr>

                                                <td class="fw-bold" style="color:#758494;" width="20%">Available Balance: </td>
                                                <td class=" font-2x" style="color:#758494;"><span>
                                                    <asp:label ID="lblbalance" style="color:#758494;" runat="server"></asp:label> BTC</span></td>
                                            </tr>
                                            <tr>

                                                <td style="color:#758494;" class="fw-bold"><span class="font-xs fw-bold"></span>Your Bitcoin address:</td>
                                                <td style="color:#758494;" class="font-2x"><span> <asp:label ID="lblbitcoinaddress" runat="server"></asp:label></span>
                                            </tr>
                          
                                        </tbody>
                                    </table>
                                  
                                </div>
                                    <div style="text-align:center;" class="form-group center">
                                        <asp:Button ID="btnwithdraw" Width="200" OnClick="btnwithdraw_Click" CssClass="btn bg-green text-center btn-primary" runat="server" Text="Withdrawal" />
                                    </div>
                            </asp:Panel>
                            
                        </div>
                      
                          
                            
                         
                    </div>
                </div>

            </div>
        </div>
         </form>
</asp:Content>

