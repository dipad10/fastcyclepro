﻿
Partial Class Office_Web_MemberAccount_Cashout_settings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If GridView1.Rows.Count = 0 Then
                GridView1.Visible = False
                btnaddnew.Visible = True

            Else
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                GridView1.Visible = True
                btnaddnew.Visible = False
            End If

        End If
       
    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)

    End Sub

    Protected Sub btnaddnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/office/web/MemberAccount/Cashout_Settings_edit")
    End Sub
End Class
