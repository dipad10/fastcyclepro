﻿
Partial Class Office_Web_MemberAccount_Testimonial_edit
    Inherits System.Web.UI.Page

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim A As New cls_testimonials
        Dim rec As New Fastcyclepro.Testimonial
        Const numbertype As String = "testimonialid"

        Dim auto As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype)
        Dim testid As String = String.Format("TESTI/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)
        rec.Testimony = txttestimony.Text
        rec.Testimonyid = testid
        rec.submittedon = Date.Now
        rec.Sponsorid = Session("username")
        rec.field1 = 0
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            msgbox1.Showsuccess("Thanks for your testimony. It has been added and awaiting admin approval")
            txttestimony.Text = ""
            'send notification to admin
            Dim mailbody As String = mod_main.PopulateBodymastermail("Administrator", "Testimony Awaiting Approval", String.Format("There's a Testimony awaiting Approval posted by {0}", rec.Sponsorid))
            Call mod_main.SendHtmlFormattedEmail(ConfigurationManager.AppSettings("AdminNotifications"), "", "Testimony Awaiting Approval", mailbody)
        Else
            msgbox1.ShowError(String.Format("{0} {1}", res.ErrorMessage, res.ExtraMessage))
        End If

    End Sub
End Class
