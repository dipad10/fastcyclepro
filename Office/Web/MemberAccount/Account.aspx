﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Account.aspx.vb" Inherits="Office_Web_Account" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Account</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>

                <li class="breadcrumb-item active"><a href="#">Account Overview</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">My Account</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="prtm-block">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-middle table-hover">
                                    <thead>
                                        <tr class="bg-primary">

                                            <th style="text-align: center;" colspan="2">Account Overview</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td width="20%">Username</td>
                                            <td><span>
                                                <asp:Label ID="lblusername" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                        <tr>

                                            <td>Registration Date</td>
                                            <td><span>
                                                <asp:Label ID="lblregdate" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                         <tr>

                                            <td>Last Access</td>
                                            <td><span>
                                                <asp:Label ID="lbllastaccess" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                        <tr>

                                            <td>Country</td>
                                            <td><span>
                                                <asp:Label ID="lblcountry" runat="server" Text=""></asp:Label>  <img runat="server" id="imgcountry" src="#" width="25" height="15" /></span></td>
                                        </tr>

                                      

                                         <tr>

                                            <td>Sponsor</td>
                                            <td><span>
                                                <asp:Label ID="lblsponsor" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                         <tr>

                                            <td>Current Server Time</td>
                                            <td><span>
                                                <asp:Label ID="lblservertime" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-middle table-hover">
                                    <thead>
                                        <tr class="bg-primary">

                                            <th style="text-align: center;" colspan="2">Straight Line Summary</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td width="20%">Total Positions</td>
                                            <td><span>
                                                <asp:Label ID="lbltotalpositions" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                        <tr>

                                            <td>Active Positions</td>
                                            <td><span>
                                                <asp:Label ID="lblactivepositions" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                         <tr>

                                            <td>Cycled Positions</td>
                                            <td><span>
                                                <asp:Label ID="lblcycledpositions" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                         <tr>

                                            <td>Total Purchase</td>
                                            <td><span>
                                               <i class="fa fa-bitcoin"></i><asp:Label ID="lbltotalpurchase" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                         <tr>

                                            <td>Total Earned</td>
                                            <td><span>
                                               <i class="fa fa-bitcoin"></i><asp:Label ID="lbltotalearned" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                         

                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-middle table-hover">
                                    <thead>
                                        <tr class="bg-primary">

                                            <th style="text-align: center;" colspan="2">Wallet Summary</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                         <tr>

                                            <td width="20%">Total Balance</td>
                                            <td><span>
                                                <i class="fa fa-bitcoin"></i><asp:Label ID="lbltotalbalance" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                       <%-- <tr>

                                            <td>Pending Withdrawal</td>
                                            <td><span>
                                                <asp:Label ID="lblpendingwithdrawal" runat="server" Text=""></asp:Label></span></td>
                                        </tr>--%>
                                        <tr>

                                            <td>Paid withdrawal</td>
                                            <td><span>
                                                <i class="fa fa-bitcoin"></i><asp:Label ID="lblpaidwithdrawal" runat="server" Text=""></asp:Label></span></td>
                                        </tr>
                                         <tr>

                                            <td>Total Withdrawal</td>
                                            <td><span>
                                                <i class="fa fa-bitcoin"></i><asp:Label ID="lbltotalwithdrawal" runat="server" Text=""></asp:Label></span></td>
                                        </tr>

                                    


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <script type="text/javascript">
        function display_c() {
            var refresh = 1000; // Refresh rate in milli seconds
            mytime = setTimeout('display_ct()', refresh)
        }

        function display_ct() {
            var strcount
            var x = new Date()
            var x1 = x.toUTCString();// changing the display to UTC string
            document.getElementById("<%=lblservertime.ClientID%>").innerText = x1;
            tt = display_c();
        }
</script>
</asp:Content>

