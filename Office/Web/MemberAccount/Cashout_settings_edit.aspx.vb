﻿
Partial Class Office_Web_MemberAccount_Cashout_settings_edit
    Inherits System.Web.UI.Page

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_Registration
        Dim rec As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
        rec.BitcoinwalletAddress = txtaddress.Text
        Dim res As ResponseInfo = A.Update(rec)
        If res.ErrorCode = 0 Then
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Bitcoin Wallet Address Updated Successfully');window.location='Cashout_settings.aspx';", True)
        Else
            msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            txtaddress.Text = rec.BitcoinwalletAddress

        End If
    End Sub
End Class
