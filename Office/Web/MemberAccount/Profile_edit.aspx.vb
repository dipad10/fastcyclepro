﻿Imports DevExpress.Web

Partial Class Office_Web_Profile_edit
    Inherits System.Web.UI.Page

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub



    Protected Sub btnsavedetails_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_Registration
        Dim rec As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
        rec.Description = txtaddress.Text
        rec.MobileNo1 = txtphonenumber.Text
        rec.Email = txtemail.Text
        If ASPxUploadControl1.PostedFile.FileName <> "" Then

            For Each file In ASPxUploadControl1.UploadedFiles
                file.SaveAs(Request.PhysicalApplicationPath + "/Office/assets/avatars/" + file.FileName)
                Dim bb As String = "/Office/assets/avatars/" + file.FileName
                rec.Profilepicpath = bb
                rec.Profilepic = file.FileName


            Next


        End If
        Dim res As ResponseInfo = A.Update(rec)
        If res.ErrorCode = 0 Then
            Dim b As New cls_matches
            Dim record As List(Of Fastcyclepro.Match) = b.Selectsponsorusername(rec.UserName)
            If record.Count <> 0 Then
                For Each sponsor In record
                    sponsor.Sponsorprofilepicpath = rec.Profilepicpath
                    'sponsor.Sponsorwalletaddress = rec.BitcoinwalletAddress
                    Dim res2 As ResponseInfo = b.Update(sponsor)
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Account Details Updated Successfully');window.location='profile_edit.aspx';", True)

                Next
            Else
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Account Details Updated Successfully');window.location='profile_edit.aspx';", True)

            End If
        Else
            msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
        End If
    End Sub

    'Protected Sub btnsavewallet_Click(sender As Object, e As EventArgs)
    '    Dim A As New Cls_Registration
    '    Dim rec As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
    '    rec.BitcoinwalletAddress = txtwalletaddress.Text
    '    Dim res As ResponseInfo = A.Update(rec)
    '    If res.ErrorCode = 0 Then
    '        Dim b As New cls_matches
    '        Dim record As List(Of Fastcyclepro.Match) = b.Selectsponsorusername(rec.UserName)
    '        If record.Count <> 0 Then
    '            For Each sponsor In record
    '                'sponsor.Sponsorprofilepicpath = rec.Profilepicpath
    '                sponsor.Sponsorwalletaddress = rec.BitcoinwalletAddress
    '                Dim res2 As ResponseInfo = b.Update(sponsor)
    '                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Bitcoin Wallet Address Updated');window.location='profile_edit.aspx';", True)

    '            Next
    '        Else
    '            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Bitcoin Wallet Address Updated');window.location='profile_edit.aspx';", True)

    '        End If

    '    Else
    '        msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
    '    End If
    'End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtusername.Enabled = False
            txtfullname.Enabled = False
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            profileimg.Src = rec.Profilepicpath
            lblfullname.Text = rec.Name
            lblprofileemail.Text = rec.Email
            lblprofilenationality.Text = rec.Country
            'lblprofilephone.Text = rec.MobileNo1
            lblprofileusername.Text = rec.UserName
            txtaddress.Text = rec.Description
            txtemail.Text = rec.Email
            txtfullname.Text = rec.Name
            txtphonenumber.Text = rec.MobileNo1
            txtusername.Text = rec.UserName
            'txtwalletaddress.Text = rec.BitcoinwalletAddress

            ddlcountry.SelectedItem.Text = rec.Country
            ddlcountry.Enabled = False
        End If
    End Sub

    Protected Sub btnsavepassword_Click(sender As Object, e As EventArgs)
        If txtrepeat.Text <> txtnewpass.Text Then
            msgbox1.ShowError("Passwords do not match!")
            Exit Sub
        End If
        If txtrepeat.Text = "" Or txtcurrentpassword.Text = "" Or txtnewpass.Text = "" Then
            msgbox1.ShowError("Passwords required!")
            Exit Sub
        End If

        Dim A As New Cls_Registration
        Dim rec As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
        If txtcurrentpassword.Text <> rec.Password Then
            msgbox1.ShowError("The current password is Incorrect, please check and try Again!")
            Exit Sub
        Else
            rec.Password = txtnewpass.Text
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Password has been changed Successfully');window.location='profile_edit.aspx';", True)

            Else
                msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        End If

    End Sub
End Class
