﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Testimonial_edit.aspx.vb" Inherits="Office_Web_MemberAccount_Testimonial_edit" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Testimony</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>

                <li class="breadcrumb-item active"><a href="#">Testimony</a></li>
            </ul>
        </div>

         <div class="prtm-block">
                    <div class="pad-b-md border-btm mrgn-b-lg">
                        <h3 class="mrgn-all-none">Add Testimony</h3>
                    </div>

                   <div class="form-group">

                            <asp:TextBox placeholder="Ex: I am so glad to inform you about this awesome opportunity Fastcycle professional is the best platform ever and forever. Please don't miss this opportunity....." ID="txttestimony" TextMode="MultiLine" Height="200" CssClass="form-control fw-bold" runat="server"></asp:TextBox>
                          
</div>

                   
                     <div class="form-group">
                                <asp:Button ID="btnsave" OnClick="btnsave_Click" CssClass="btn btn-primary btn-rounded" runat="server" Text="Save" />
                            </div>

                </div>
            
          </form>
</asp:Content>

