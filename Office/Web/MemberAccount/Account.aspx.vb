﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class Office_Web_Account
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
           
            Dim rec As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            Dim lastaccess As Date = rec.LastAccess
            Dim regdate As Date = rec.CreatedOn
            lbllastaccess.Text = lastaccess.ToLongDateString
            lblregdate.Text = regdate.ToLongDateString
            lblsponsor.Text = rec.Sponsor
            lblusername.Text = rec.UserName
            lblcountry.Text = rec.Country
            imgcountry.Src = rec.State

            bindcycledpositions(Session("username"))
            bindactivepositions(Session("username"))
            bindtotalpositions(Session("username"))
            bindwalletbalance(Session("username"))
            bindtotalpurchase(Session("username"))
            bindtotalearned(Session("username"))
            bindpaidwithdrawal(Session("username"))
            bindtotalwithdrawal(Session("username"))
        End If
    End Sub


    Protected Sub bindcycledpositions(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotalcycledpositions"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lblcycledpositions.Text = dr("Total")

                    Else
                        lblcycledpositions.Text = "0"


                    End If

                Else

                    lblcycledpositions.Text = "0"

                End If

            End Using

        End Using
    End Sub
    Protected Sub bindactivepositions(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotalactivepositions"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lblactivepositions.Text = dr("Total")

                    Else
                        lblactivepositions.Text = "0"


                    End If

                Else

                    lblactivepositions.Text = "0"

                End If

            End Using

        End Using
    End Sub
    Protected Sub bindtotalpositions(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotalpositions"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lbltotalpositions.Text = dr("Total")

                    Else
                        lbltotalpositions.Text = "0"


                    End If

                Else

                    lbltotalpositions.Text = "0"

                End If

            End Using

        End Using
    End Sub

    Protected Sub bindwalletbalance(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "GetSumWalletbalance"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("totalBAL") IsNot DBNull.Value Then
                        lbltotalbalance.Text = dr("totalBAL")
                       
                    Else
                        lbltotalbalance.Text = "0.0000"


                    End If

                Else

                    lbltotalbalance.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotalpurchase(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotalpurchase"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lbltotalpurchase.Text = dr("Total")
                       
                    Else
                        lbltotalpurchase.Text = "0.0000"


                    End If

                Else

                    lbltotalpurchase.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub
    Protected Sub bindtotalearned(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotalearned"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lbltotalearned.Text = dr("Total")


                    Else
                        lbltotalearned.Text = "0.0000"


                    End If

                Else

                    lbltotalearned.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub

    Protected Sub bindpaidwithdrawal(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Getpaidwithdrawal"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lblpaidwithdrawal.Text = dr("Total")


                    Else
                        lblpaidwithdrawal.Text = "0.0000"


                    End If

                Else

                    lblpaidwithdrawal.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotalwithdrawal(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Getpaidwithdrawal"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        lbltotalwithdrawal.Text = dr("Total")


                    Else
                        lbltotalwithdrawal.Text = "0.0000"


                    End If

                Else

                    lbltotalwithdrawal.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub
End Class
