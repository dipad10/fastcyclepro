﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Cashout_settings.aspx.vb" Inherits="Office_Web_MemberAccount_Cashout_settings" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Cashout</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>

                <li class="breadcrumb-item active"><a href="#">Cashout Settings</a></li>
            </ul>
        </div>

            <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">Cashout Settings</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="prtm-block">
                           
                            <asp:Button ID="btnaddnew" CssClass="btn btn-warning btn-rounded" OnClick="btnaddnew_Click" runat="server" Visible="false" Text="Add New Coinpayments Bitcoin Address" />
                                  <asp:Label ID="Label1" CssClass="text-small center fg-gray text-bold" runat="server" Text=""></asp:Label>
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" ShowHeaderWhenEmpty="true" Visible="false" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table table-striped table-bordered bg-primary table-middle table-hover" PageSize="50" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="Paymode" HeaderStyle-ForeColor="White" HeaderText="Payment Type" SortExpression="type" />

                                <asp:BoundField ItemStyle-HorizontalAlign="left" DataField="BitcoinwalletAddress" ItemStyle-Font-Bold="true" HeaderStyle-ForeColor="White" HeaderText="Bitcoin WalletAddress" SortExpression="Paymentmethod" />
                               
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href="Cashout_settings_edit.aspx" class="label label-success btn-rounded"><i class="fa fa-pencil-square"></i> Update</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                

                                <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                            </Columns>
                        </asp:GridView>
                                
                            </div>
                               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="GetUserreg" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="username" SessionField="username" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                         
                        </div>
                    </div>
                </div>

            </div>
        </div>
          </form>
</asp:Content>

