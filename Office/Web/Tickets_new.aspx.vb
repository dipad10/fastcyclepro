﻿Imports DevExpress.Web

Partial Class Office_Web_Tickets_new
    Inherits System.Web.UI.Page
    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub
    Protected Sub btnsumit_Click(sender As Object, e As EventArgs)
        If ddltitle.SelectedValue = "null" Then
            msgbox1.ShowError("Please select ticket title")
            Exit Sub
        End If
        If editor.Text = "" Then
            msgbox1.ShowError("Please Insert a description")
            Exit Sub

        End If



        Dim A As New Cls_Tickets
        Dim sn As String = Request.QueryString("D")
        If sn <> "" Then
            'update
            Dim rec2 As Fastcyclepro.Ticket = A.SelectThisID(sn)
            rec2.Message = editor.Text
            rec2.TicketTitle = ddltitle.SelectedItem.Text
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(Request.PhysicalApplicationPath + "/Office/assets/ticketimg/" + file.FileName)
                    Dim bb As String = "/Office/assets/ticketimg/" + file.FileName
                    rec2.Picpath = bb
                    rec2.A1 = file.FileName


                Next

                Dim res As ResponseInfo = A.Update(rec2)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Ticket has been Updated!.');window.location='tickets.aspx';", True)
                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                End If
            End If

        Else
            'create new instance
            Dim rec As New Fastcyclepro.Ticket
            rec.TicketTitle = ddltitle.SelectedItem.Text
            rec.UserName = Session("username")
            Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(rec.UserName)
            rec.UserID = use.UserID
            rec.Status = "PENDING"
            rec.Submittedon = DateTime.Now.ToString
            rec.Message = editor.Text
            If ASPxUploadControl1.PostedFile.FileName <> "" Then

                For Each file In ASPxUploadControl1.UploadedFiles
                    file.SaveAs(Request.PhysicalApplicationPath + "/Office/assets/ticketimg/" + file.FileName)
                    Dim bb As String = "/Office/assets/ticketimg/" + file.FileName
                    rec.Picpath = bb
                    rec.A1 = file.FileName


                Next

                Dim res As ResponseInfo = A.Insert(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Ticket has been Submitted!.');window.location='tickets.aspx';", True)
                Else
                    msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                End If
            End If
        End If


    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sn As String = Request.QueryString("D")
            If sn <> "" Then
                Dim rec As Fastcyclepro.Ticket = (New Cls_Tickets).SelectThisID(sn)
                ddltitle.SelectedItem.Text = rec.TicketTitle
                editor.Text = rec.Message
                Image1.ImageUrl = rec.Picpath
                Image1.Visible = True
            End If
        End If
    End Sub
End Class
