﻿Imports System.Net

Partial Class Office_Web_MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
       
        If Not Page.IsPostBack Then
            If Session("username") = "" Then

                Dim OriginalUrl As String = HttpContext.Current.Request.RawUrl
                Dim LoginPageUrl As String = "/office/signin"
                HttpContext.Current.Response.Redirect([String].Format("{0}?goto={1}", LoginPageUrl, OriginalUrl))
           
            End If
           
            'get the registration date for d navbar
            Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            datejoined.InnerHtml = Format(use.CreatedOn, "dd MMM yyyy")


            lblipaddress.InnerHtml = getIpAddressAddress()
            'get btc rate
            Try
             
                Dim result = mod_main.converttobtc(1)
                btctoday.InnerHtml = "$" & FormatNumber(1 / result, 2)
            Catch ex As Exception
                btctoday.InnerHtml = 0
            End Try

            Select Case use.Permission
                Case "ADMIN"

                    moduleadmin.Visible = True
                    welcomeimg.Src = use.Profilepicpath
                    welcomeimg.Alt = Session("username")

                Case Else
                    welcomeimg.Src = use.Profilepicpath
                    welcomeimg.Alt = Session("username")
                    moduleadmin.Visible = False

            End Select


            'get the user ipaddress

            If Date.Now.Hour < 12 Then
                welcome.InnerText = "Good Morning, " & Session("username").ToString() & ""

            ElseIf Date.Now.Hour < 16 Then
                welcome.InnerText = "Good Afternoon, " & Session("username").ToString() & ""
            Else
                welcome.InnerText = "Good Evening, " & Session("username").ToString() & ""


            End If

        End If
    End Sub

    Public Shared Function getIpAddressAddress() As String
        Dim IpAddress As String = String.Empty
        IpAddress = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If IpAddress = "" Or IpAddress Is Nothing Then
            IpAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        End If
       
        Return IpAddress
    End Function



    
End Class

