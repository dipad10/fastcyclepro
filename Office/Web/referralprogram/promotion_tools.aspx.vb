﻿
Partial Class Office_Web_referralprogram_promotion_tools
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtreflink.Text = String.Format("{0}?ref={1}", ConfigurationManager.AppSettings("pathtorefredirect"), Session("username"))
            lblsplash1.Text = String.Format("{0}1?ref={1}", ConfigurationManager.AppSettings("pathtosplash"), Session("username"))
        End If
    End Sub
End Class
