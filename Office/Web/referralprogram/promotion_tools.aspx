﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="promotion_tools.aspx.vb" Inherits="Office_Web_referralprogram_promotion_tools" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Promotion Tools</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Referral Program</a></li>

                <li class="breadcrumb-item active"><a href="#">Promotion Tools</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">


                    <div class="prtm-block">
                        <div class="caption">
                            <h3 class="text-capitalize">My Referrals</h3>
                            <hr />
                        </div>

                        <h4 class="text-primary">Give this link to your friends & contacts</h4>
                        <p></p>
                        <div class="form-group">
                            <label for="msg">Referral Link:</label>
                            <asp:TextBox ID="txtreflink" CssClass="form-control fw-bold" runat="server"></asp:TextBox>
                        </div>

                          <div class="form-group">
<label for="msg"><b>Splash Pages</b></label>
                            <div class="panel">
                                <div class="panel-body">
                                   <span>Use these splashpages to build your referrals. </span> <br />
                                    <img src="#" alt="#" width="100" height="100" />
                                    <br />
                                    <asp:TextBox ID="lblsplash1" CssClass="form-control fw-bold" runat="server"></asp:TextBox>
                               
                                </div>
                            </div></div>
                    </div>

                </div>

            </div>
        </div>


    </form>
</asp:Content>

