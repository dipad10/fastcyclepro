﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Office_Web_History_Transactions
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            bindwalletbalance(Session("username"))
            If GridView1.Visible = True Then
                If GridView1.Rows.Count = 0 Then
                    lblnoposition.Visible = True
                Else
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            End If

            If GridView2.Visible = True Then

                If GridView2.Rows.Count = 0 Then
                    Label1.Visible = True
                Else
                    GridView2.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            End If


        End If
    End Sub

    Protected Sub bindwalletbalance(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "GetSumWalletbalance"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("totalBAL") IsNot DBNull.Value Then
                        lbltotalbal.Text = dr("totalBAL")
                        lbltotalbal.Enabled = False

                    Else
                        lbltotalbal.Text = "0.0000"


                    End If

                Else

                    lbltotalbal.Text = "0.0000"

                End If

            End Using

        End Using
    End Sub

    Protected Sub ddlselect_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case ddlselect.SelectedValue
            Case "FCP"
                GridView1.Visible = True
                GridView2.Visible = False

            Case Else
                GridView2.Visible = True
                GridView1.Visible = False
                lbltotalbal.Visible = False
        End Select
    End Sub
End Class
