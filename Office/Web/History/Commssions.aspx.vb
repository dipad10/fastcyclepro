﻿
Partial Class Office_Web_History_Commssions
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If GridView1.Visible = True Then
                If GridView1.Rows.Count = 0 Then
                    lblnowithdrawal.Visible = True
                Else
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            End If

            If GridView2.Visible = True Then

                If GridView2.Rows.Count = 0 Then
                    Label1.Visible = True
                Else
                    GridView2.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            End If


        End If
    End Sub

    Protected Sub ddlselect_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case ddlselect.SelectedValue
            Case "FCP"
                GridView1.Visible = True
                GridView2.Visible = False
            Case Else
                GridView2.Visible = True
                GridView1.Visible = False
        End Select
    End Sub
End Class
