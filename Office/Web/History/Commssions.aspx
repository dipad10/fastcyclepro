﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Commssions.aspx.vb" Inherits="Office_Web_History_Commssions" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Referral Commissions</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">History</a></li>

                <li class="breadcrumb-item active"><a href="#">Referral Commisions</a></li>
            </ul>
        </div>

        <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">My Referral Commissions</h3>
                                </div>

                            </div>
                        </div>
                        <div class="prtm-block">
                                          <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                             <p class="font-2x fw-semibold">Select Plan</p>
                                        <div class="selectbox">
                                            <asp:DropDownList ID="ddlselect" AutoPostBack="true" OnSelectedIndexChanged="ddlselect_SelectedIndexChanged" CssClass="form-control" runat="server">
                           
                                                <asp:ListItem Selected="True" Value="FCP">FastCyclePro / Blazer</asp:ListItem>
                                                <asp:ListItem Value="FCT">Fastcycle Turbo</asp:ListItem>
                                          
                                                <%--  <asp:ListItem Value="3">Level 3</asp:ListItem>
                                                                <asp:ListItem Value="4">Level 4</asp:ListItem>
                                                                <asp:ListItem Value="5">Level 5</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="table-responsive">
                             
                                    <asp:GridView ID="GridView1" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" runat="server" AllowPaging="True" DataSourceID="SqlDataSource1">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Method">
                                                <ItemTemplate>
                                                    <span><%#Eval("AccountType")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Downline">
                                                <ItemTemplate>
                                                    <span><%#Eval("Field3")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Description">
                                                <ItemTemplate>
                                                    <span><%#Eval("item_name")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                           <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Amount">
                                                <ItemTemplate>
                                                    <span><i class="fa fa-bitcoin"></i><%#Eval("Withdrawal")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Submitted On">
                                                <ItemTemplate>
                                                    <span class=""><%#Eval("Createdon", "{0:d, MMM yyyy}")%> </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                                        </Columns>
                                    </asp:GridView>
                             

                                <div style="text-align: center;" class="center">
                                    <asp:Label ID="lblnowithdrawal" CssClass="text-center text-info" Visible="false" runat="server" Text="- no Commissions -"></asp:Label>

                                </div>
                            </div>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="Getallcommissions" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="username" SessionField="username" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                              <div class="table-responsive">
                             
                                    <asp:GridView ID="GridView2" Visible="false" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" runat="server" AllowPaging="True" DataSourceID="SqlDataSource2">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Method">
                                                <ItemTemplate>
                                                    <span><%#Eval("AccountType")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Downline">
                                                <ItemTemplate>
                                                    <span><%#Eval("Field3")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Description">
                                                <ItemTemplate>
                                                    <span><%#Eval("item_name")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                           <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Amount">
                                                <ItemTemplate>
                                                    <span><i class="fa fa-bitcoin"></i><%#Eval("CurrentBalance")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderStyle-CssClass="bg-primary font-sm" HeaderText="Submitted On">
                                                <ItemTemplate>
                                                    <span class=""><%#Eval("Createdon", "{0:d, MMM yyyy}")%> </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                                        </Columns>
                                    </asp:GridView>
                             

                                <div style="text-align: center;" class="center">
                                    <asp:Label ID="Label1" CssClass="text-center text-info" Visible="false" runat="server" Text="- no Commissions -"></asp:Label>

                                </div>
                            </div>

                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="Getallcommissionsturbo" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="username" SessionField="username" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>



                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</asp:Content>
