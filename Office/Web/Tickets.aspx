﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Office/Web/MasterPage.master" AutoEventWireup="false" CodeFile="Tickets.aspx.vb" Inherits="Office_Web_Tickets" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:msgbox1 ID="msgbox1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="prtm-page-bar">
            <ul class="breadcrumb">
                <li class="breadcrumb-item text-capitalize">
                    <h3>Tickets</h3>
                </li>
                <li class="breadcrumb-item"><a href="#">Support</a></li>

                <li class="breadcrumb-item active"><a href="#">Tickets</a></li>
            </ul>
        </div>

            <div class="table-style">
            <div class="row">
                <div class="col-md-12">
                    <div class="prtm-full-block">
                        <div class="prtm-block-title pad-all-md">
                            <div class="pos-relative">
                                <div class="caption">
                                    <h3 class="text-capitalize">My Tickets</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="prtm-block">
                              
                         
                        
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" DataSourceID="SqlDataSource1" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-Font-Bold="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-middle table-hover" PageSize="50" runat="server" AllowPaging="True">
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="bg-primary" DataField="SN" HeaderStyle-ForeColor="White" HeaderText="#ID" SortExpression="PositionID" />
                                  <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Creator">
                    <ItemTemplate>
                       <span><%#Eval("UserName")%></span> 
                    </ItemTemplate>
                </asp:TemplateField>
                                 <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Subject">
                    <ItemTemplate>
                       <span><%#Eval("TicketTitle")%></span> 
                    </ItemTemplate>
                </asp:TemplateField>
                              
                                  <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Status">
                    <ItemTemplate>
                       <span> <%#Eval("Status")%></span> 
                    </ItemTemplate>
                </asp:TemplateField>
                                  <asp:TemplateField HeaderStyle-CssClass="bg-primary" HeaderText="Created On">
                    <ItemTemplate>
                       <span> <%#Eval("Submittedon", "{0:d, MMM yyyy}")%></span> 
                    </ItemTemplate>
                </asp:TemplateField>
                             <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href="Tickets_new.aspx?D=<%#Eval("SN")%>" class="label label-success btn-rounded"><i class="fa fa-pencil-square"></i> Edit</a>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                                 

                                <%--                 <asp:HyperLinkField HeaderText="View" DataNavigateUrlFields="PolicyNo" Text="View" DataNavigateUrlFormatString="Viewer.aspx?PolicyNo={0}"/>--%>
                            </Columns>
                        </asp:GridView>
                                <div style="text-align:center;" class="center">
                                                                                            <asp:Label ID="lblnoposition" CssClass="text-center text-info" Visible="false" runat="server" Text="- no Tickets -"></asp:Label>

                            </div>
                            </div>
                               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Fastcyclepro_DBConnectionString %>" SelectCommand="GetUserTickets" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="username" SessionField="username" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                         
                            <asp:Button ID="btnnew" CssClass=" btn btn-secondary" runat="server" OnClick="btnnew_Click" Text="Create New Ticket" />

                        </div>
                    </div>
                </div>

            </div>
        </div>
          </form>
</asp:Content>

