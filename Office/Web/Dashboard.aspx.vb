﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class Office_Web_Dashboard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
      
        If Not Page.IsPostBack Then
            If Session("username") = "" Then

                Dim OriginalUrl As String = HttpContext.Current.Request.RawUrl
                Dim LoginPageUrl As String = "/office/signin"
                HttpContext.Current.Response.Redirect([String].Format("{0}?goto={1}", LoginPageUrl, OriginalUrl))

            End If
            'bind announcement
            bindannouncement()

            'show the total sum of donation
            bindGetpaidwithdrawal(Session("username"))
            BindProfit(Session("username"))
            bindrefbonus(Session("username"))
            bindreftotal(Session("username"))

            'bind repeaters
          
            'bind profile info
            Dim user As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("username"))
            profileuserpicture.Src = user.Profilepicpath
            profilefullname.InnerHtml = user.Name
            'profileuserphone.Text = user.MobileNo1
            profileemail.Text = user.Email
            profileusername.Text = Session("username")
            txtwalletaddress.Value = user.BitcoinwalletAddress

            bindphase(Session("username"))
            bindphase2(Session("username"))


        End If
    End Sub
#Region "BIND REPEATERS"
    Private Sub bindannouncement()
        Dim A As New Fastcyclepro.FastcycleproDataContext
        Dim announcement = A.GetAnnouncement
        For Each record In announcement
            announce.InnerHtml = record.Announcement
        Next
    End Sub
    
#End Region

#Region "BIND CARDS TOTALS"
    Protected Sub bindreftotal(ByVal referer As String)
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Gettotalreferal"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@referer", referer)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("total") IsNot DBNull.Value Then
                        Try
                            lbltotalref.Text = FormatNumber(dr2("total"), 0)
                            lblreflink.Text = String.Format("{0}{1}", "https://Fastcyclepro.com/?ref=", Session("username"))
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else
                        lbltotalref.Text = 0

                    End If


                Else
                    lbltotalref.Text = 0
                End If


            End Using
        End Using
    End Sub
    Protected Sub bindrefbonus(ByVal referer As String)
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "GetSumWalletbalance"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", referer)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("totalBAL") IsNot DBNull.Value Then
                        Try
                           
                            lblrefbitcoin.Text = dr2("totalBAL")
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lblrefbitcoin.Text = "0.00000000"
                    End If


                Else

                    lblrefbitcoin.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub
    Protected Sub bindGetpaidwithdrawal(ByVal username As String)
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Getpaidwithdrawal"
                mycommand.Parameters.AddWithValue("@username", username)
                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("Total") IsNot DBNull.Value Then
                        Try

                         
                            lblinbitcoin.Text = dr("Total")
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else
                        lblinbitcoin.Text = "0.00000000"


                    End If

                Else

                    lblinbitcoin.Text = "0.00000000"

                End If

            End Using

        End Using
    End Sub

    Private Sub BindProfit(ByVal username As String)
        'bind profit
        Using con As New SqlConnection(m_strconnString)
            con.Open()
            Using cmd As New SqlCommand()
                cmd.CommandText = "Gettotalpurchase"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@username", username)
                cmd.Connection = con

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(cmd)
                _da.Fill(_dt)

                Dim dr2 As SqlDataReader = Nothing
                dr2 = cmd.ExecuteReader

                If dr2.Read() Then
                    If dr2("Total") IsNot DBNull.Value Then
                        Try

                            lblprofitbitcoin.Text = dr2("Total")
                        Catch ex As Exception
                            msgbox1.ShowError(ex.Message)
                        End Try

                    Else

                        lblprofitbitcoin.Text = "0.00000000"
                    End If


                Else

                    lblprofitbitcoin.Text = "0.00000000"
                End If


            End Using
        End Using
    End Sub
#End Region

    Protected Sub btnrefredirect_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect(String.Format("{0}?ref={1}", ConfigurationManager.AppSettings("pathtorefredirect"), Session("username")))
    End Sub

    

    

    




    Protected Sub btnupdatewallet_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_Registration
        Dim rec As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
        rec.BitcoinwalletAddress = txtwalletaddress.Value.ToString
        Dim res As ResponseInfo = A.Update(rec)
        If res.ErrorCode = 0 Then
            Dim b As New cls_matches
            Dim record As List(Of Fastcyclepro.Match) = b.Selectsponsorusername(rec.UserName)
            If record.Count <> 0 Then
                For Each sponsor In record
                    'sponsor.Sponsorprofilepicpath = rec.Profilepicpath
                    sponsor.Sponsorwalletaddress = rec.BitcoinwalletAddress
                    Dim res2 As ResponseInfo = b.Update(sponsor)
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Bitcoin Wallet Address Updated Successfully');window.location='Dashboard.aspx';", True)

                Next
            Else
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Bitcoin Wallet Address Updated Successfully');window.location='Dashboard.aspx';", True)

            End If


        Else
            msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

        End If
    End Sub


    Private Sub bindphase(ByVal username As String)
        Dim rec As List(Of Fastcyclepro.Position) = (New cls_position).Selectuserpositions2(username)
        GridView1.DataSource = rec
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            lblnoposition.Visible = True
        End If

    End Sub

    Private Sub bindphase2(ByVal username As String)
        Dim rec As List(Of fastcyclepro.Blazer) = (New cls_blazer).Selectuserposition2(username)
        GridView2.DataSource = rec
        GridView2.DataBind()
        If GridView1.Rows.Count = 0 Then
            lblnoposition.Visible = True
        End If

    End Sub
End Class
