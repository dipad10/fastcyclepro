﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Data.SqlClient
Partial Class Office_Signup
    Inherits System.Web.UI.Page

    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("referral") = "" Then
            Session("referral") = "admin"
        End If

        lblref.Text = String.Format("Referred By: {0}", Session("referral"))

        If Not Page.IsPostBack Then
            bindcountry(ddlcountry)
        End If
    End Sub
    Protected Sub btnsignup_Click(sender As Object, e As EventArgs)
        'msgbox1.ShowError("Registration has been disabled for now..please wait till prelaunch")
        'Exit Sub

        'Start validattions
        If txtfullname.Text = "" Then
            msgbox1.ShowError("Please insert your fullname")
            Exit Sub

        End If

        Dim ref As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(Session("referral"))
        If ref.UserName Is Nothing Then
            'means there's no such refereal(user) in the system
            msgbox1.ShowError("This Referral ID does not Exist. Please check the spelling correctly.")
            Exit Sub

        End If


        If ddlgender.SelectedValue = "null" Then
            msgbox1.ShowError("Please select Your gender")
            Exit Sub
        End If
        If ddlcountry.SelectedValue = "null" Then
            msgbox1.ShowError("Please select Your Country")
            Exit Sub
        End If
        If txtphone.Text = "" Then
            msgbox1.ShowError("Please Insert Your Mobile number")
            Exit Sub
        End If
        If txtemail.Text = "" Then
            msgbox1.ShowError("Email is Required")
            Exit Sub
        End If
        If txtusername.Text = "" Then
            msgbox1.ShowError("Username is Required")
            Exit Sub
        End If
        'If txtwalletaddress.Text = "" Then
        '    msgbox1.ShowError("Please insert your wallet Address to start Earning")
        '    Exit Sub

        'End If
        If txtpassword.Text = "" Then
            msgbox1.ShowError("Password is Required")
            Exit Sub
        End If
        If Me.txtpassword.Text <> Me.txtconfirmpassword.Text Then
            msgbox1.ShowError("Passwords do not match!")
            Exit Sub

        End If

        Dim user As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(txtusername.Text)
        If user.UserName Is Nothing Then

        Else
            msgbox1.ShowError("This Username has already been taken. Please Use another.")
            Exit Sub

        End If
        Dim useremail As Fastcyclepro.Registration = (New Cls_Registration).SelectThisemail(txtemail.Text)
        If user.Email Is Nothing Then

        Else
            msgbox1.ShowError("This email already exists. Please Login.")
            Exit Sub

        End If


        If mod_main.IsValidEmail(txtemail.Text) = False Then
            msgbox1.ShowError("This is not a valid email!")
            Exit Sub
        End If

        Dim EncodedResponse As String = Request.Form("g-Recaptcha-Response")
        Dim iscaptchavalid As String = RecaptchaApiResponse.Validate(EncodedResponse)
        If iscaptchavalid <> "true" Then

            msgbox1.ShowError("invalid captcha")
            Exit Sub
        End If



        'START SIGNUP PROCESS
        'save user profile
        Dim P As New Cls_Registration

        Dim numbertype As String = "UserID"
        Dim auto As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype)
        Dim userid As String = String.Format("USER/{0}/{1}", Date.Now.Year, auto.Nextvalue)
        Call mod_main.Updateautonumber(numbertype)

        Dim Rec As New Fastcyclepro.Registration
        Rec.UserID = userid

        Rec.Name = Me.txtfullname.Text.ToUpper
        Rec.Gender = Me.ddlgender.SelectedValue
        Rec.Country = Me.ddlcountry.SelectedItem.Text
        Rec.MobileNo1 = Me.txtphone.Text
        Rec.UserName = Me.txtusername.Text
        Rec.Password = Me.txtpassword.Text
        Rec.Email = txtemail.Text

            Rec.BitcoinwalletAddress = "N/A"

        Rec.Sponsor = Session("referral")
        Rec.Paymode = "COINPAYMENTS"
        Rec.Permission = "USER"
        Rec.Active = 0
        Rec.CreatedOn = DateTime.Now
        Rec.Field9 = 1   'shows that account is still fresh not suspended. if its 0 it means its suspended
        If ddlgender.SelectedValue = "Male" Then
            Rec.Profilepicpath = "/office/assets/avatars/maleavatar.png"
            Rec.Profilepic = "maleavatar.png"
        Else
            Rec.Profilepicpath = "/office/assets/avatars/femaleavatar.png"
            Rec.Profilepic = "femaleavatar.png"
        End If
        Rec.State = String.Format("/countryflag/{0}.png", ddlcountry.SelectedValue)
        Rec.Activationcode = Guid.NewGuid.ToString
        Dim res As ResponseInfo = P.Insert(Rec)
        If res.ErrorCode = 0 Then

            Dim referal As String = Session("referral")

            Dim R As New cls_referal

            Dim numbertype2 As String = "refid"
            Dim auto2 As Fastcyclepro.Autosetting = (New Cls_Autosettings).Getautonumbervalue(numbertype2)
            Dim refid As String = String.Format("REF/{0}/{1}", Date.Now.Year, auto2.Nextvalue)
            Call mod_main.Updateautonumber(numbertype2)

            Dim refrec As New Fastcyclepro.Referal
            refrec.RefID = refid
            refrec.Referer = referal
            refrec.Referee = Rec.UserName
            refrec.Active = 1
            refrec.Status = "ACTIVE"
            refrec.Bitcoinamount = 0
            refrec.Bonus = 0
            refrec.submittedon = Rec.CreatedOn
            refrec.Field2 = Rec.Email
            Try
                R.Insert(refrec)
                'send mail to referrer that someone has registered under him
                Dim use As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(referal)
                Dim referaldate As Date = Rec.CreatedOn
                Dim body As String = mod_main.PopulateBodyreferrer(Rec.UserName, use.Name, referaldate.ToLongDateString)

                Call mod_main.SendHtmlFormattedEmail(use.Email, "", "You have a new Referral", body)
            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
            End Try


            Try
                'send activation email to user
                Dim url As String = String.Format("{0}?Acode={1}", ConfigurationManager.AppSettings("pathtouseractivation"), Rec.Activationcode)

                Dim body As String = mod_main.PopulateBodyactivation(Rec.Name, url)
                Call mod_main.SendHtmlFormattedEmail(Rec.Email, "", "Activate Your Fastcyclepro Account", body)
                Panelsignup.Visible = False
                txtconfirmail.InnerHtml = Rec.Email
                panelconfirmation.Visible = True

            Catch ex As Exception
                msgbox1.ShowError(ex.Message)
            End Try
        End If
    End Sub

    Private Sub bindcountry(ByVal Dropdown As DropDownList)
        Dim rec As List(Of fastcyclepro.Country) = (New cls_country).SelectAllcountries

        Dropdown.DataSource = rec
        Dropdown.DataTextField = "name"
        Dropdown.DataValueField = "iso"

        Dropdown.DataBind()
        Dropdown.Items.Insert(0, New ListItem("- select Country -", "NULL"))
    End Sub
   
    Protected Sub ddlcountry_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim isovalue As String = ddlcountry.SelectedValue
        Dim rec As fastcyclepro.Country = (New cls_country).SelectThisiso(isovalue)
        txtphone.Text = String.Format("+{0}", rec.phonecode)
    End Sub
End Class
