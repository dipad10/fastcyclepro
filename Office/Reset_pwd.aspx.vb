﻿
Partial Class Office_Reset_pwd
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs)
        If txtpassword.Text = "" Then
            msgbox1.ShowError("Please Insert your new password")
            Exit Sub
        End If
        If txtretype.Text = "" Then
            msgbox1.ShowError("Please Retype your password")
            Exit Sub
        End If
        If txtpassword.Text <> txtretype.Text Then
            msgbox1.ShowError("Please Enter Same Password")
            Exit Sub
        End If

        Dim useremail As String = Request.QueryString("M")
        Dim userresetcode As String = Request.QueryString("C")
        Dim A As New Cls_Registration
        Dim rec As Fastcyclepro.Registration = A.Selectresetpwd(useremail, userresetcode)
        If rec Is Nothing Then
            msgbox1.ShowError("This reset link is Invalid!")
            Exit Sub
        Else
            rec.Password = txtpassword.Text
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                msgbox1.Showsuccess("Your Password has been Changed successfully")
                txtpassword.Text = ""
                txtretype.Text = ""
            Else
                msgbox1.ShowError(res.ErrorMessage)
            End If
        End If
    End Sub
End Class
