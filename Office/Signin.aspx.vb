﻿Imports System.Runtime.Serialization
Imports System.Text
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports System.IO
Imports System.Web.Script.Serialization

Partial Class Office_Signin
    Inherits System.Web.UI.Page

    Protected Sub btnsignin_Click(sender As Object, e As EventArgs)
        If txtusername.Text = "" Then
            msgbox1.ShowError("Username is Required")
            Exit Sub
        End If

        If txtpassword.Text = "" Then
            msgbox1.ShowError("Password is Required")
            Exit Sub
        End If
        ''validate captcha
        'Dim EncodedResponse As String = Request.Form("g-Recaptcha-Response")
        'Dim iscaptchavalid As String = RecaptchaApiResponse.Validate(EncodedResponse)
        'If iscaptchavalid <> "true" Then

        '    msgbox1.ShowError("invalid captcha")
        '    Exit Sub
        'End If

        'check if goto query string is not empty. if it aint empty once login it should direct u straight to the path
        Dim path As String = Request.QueryString("goto")
        If path <> "" Then
            Dim rec As List(Of Fastcyclepro.Registration) = (New Cls_Registration).Authenticatelogin(txtusername.Text, txtpassword.Text)
            If rec.Count = 0 Then
                'invalid user
                msgbox1.ShowError("Invalid Username or Password")

                Exit Sub

            Else
                'chck wether account is suspended
                For Each record In rec
                    If record.Field9 = 0 Then
                        'means suspended
                        msgbox1.ShowError("You have Been Suspended from using Fastcyclepro. If you have any questions, you can contact our support staffs at support@Fastcyclepro.com")
                        Exit Sub

                    ElseIf record.Active = 0 Then
                        msgbox1.ShowError("Your Account has not been activated, Please contact us at support@Fastcyclepro.com or Use the Live chat")
                        Exit Sub

                    Else
                        'take him to the required page straight
                        Session("username") = txtusername.Text
                        Response.Redirect(path)
                    End If
                Next


            End If

        Else
            'get datetime for sponsors due gridview
            Session("datetime") = Format(Date.Now, "dd MMM yyyy")
            'normal login(goto is empty)
            Dim rec As List(Of Fastcyclepro.Registration) = (New Cls_Registration).Authenticatelogin(txtusername.Text, txtpassword.Text)
            If rec.Count = 0 Then
                'invalid user
                msgbox1.ShowError("Invalid Username or Password")

                Exit Sub

            Else
                Session("username") = txtusername.Text
                Dim A As New Cls_Registration
                Dim user As Fastcyclepro.Registration = A.SelectThisUsername(Session("username"))
                user.LastAccess = Date.Now
                Dim res As ResponseInfo = A.Update(user)
                If res.ErrorCode = 0 Then
                    Select Case user.Permission
                        Case "ADMIN"
                            Response.Redirect("~/Office/web/Admin/Home.aspx")
                        Case "USER"
                            If user.Field9 = 0 Then
                                msgbox1.ShowError("You have Been Suspended from using Fastcyclepro. Contact Our Support Team at support@fastcyclepro.com")
                                Exit Sub
                            ElseIf user.Active = 0 Then
                                msgbox1.ShowError("Your Account has not been activated, Please contact us at support@Fastcyclepro.com or check your mail for the activation link")
                                Exit Sub
                            Else
                                If RadioButton1.Checked = True Then
                                    Response.Redirect("~/Office/web/Dashboard.aspx")
                                Else
                                    Response.Redirect("~/Office/web/fastcycleturbo/Dashboard.aspx")
                                End If

                            End If

                    End Select
                End If






            End If
        End If


    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

End Class
