﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Reset_pwd.aspx.vb" Inherits="Office_Reset_pwd" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<!DOCTYPE html>

<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/vendor.min.css">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/plugins.min.css">
    <link rel="stylesheet" type="text/css" href="/office/assets/css/pratham.min.css">
    <title>Fastcyclepro SignIn</title>
</head>
<body style="background: url(/assets/business1.jpg) no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
    <form runat="server" id="form1">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="prtm-wrapper">
            <div class="prtm-main">
                <%--  <div class="login-banner">
                    <img src="/office/assets/img/login-banner.jpg" alt="login banner" class="img-responsive" width="1920" height="246">
                </div>--%>
                <div class="login-form-wrapper mrgn-b-lg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-5 center-block">
                                <div class="prtm-form-block prtm-full-block overflow-wrappper">
                                    <div class="login-bar">
                                        <img src="/office/assets/img/login-bars.png" class="img-responsive" alt="login bar" width="743" height="7">
                                    </div>
                                    <div class="prtm-block-title text-center">
                                        <div class="mrgn-b-lg">
                                            <a href="javascript:;">
                                                <img src="/assets/img/Fastcyclepro.png" alt="login logo" class="img-responsive display-ib" width="218" height="23">
                                            </a>
                                        </div>
                                        <div class="login-top mrgn-b-lg">
                                            <div class="mrgn-b-md">
                                                <h2 class="text-capitalize base-dark font-2x fw-normal">Reset Password</h2>
                                            </div>

                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <uc1:msgbox1 ID="msgbox1" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <asp:Panel ID="panelconfirmation" runat="server">
                                        <div class="prtm-block-content">
                                            <div class="panel panel-white">
                                                <div class="panel-heading panel-success-border">
                                                    <div class="row">
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
                                                            <h3 class="panel-title text-capitalize">Enter New password Here.</h3>
                                                        </div>
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                                            <div class="pull-right">
                                                                <a href="#panel5" data-toggle="collapse" class="" aria-expanded="true"><i class="fa fa-minus fa-lg gray" aria-hidden="true"></i></a>
                                                                <a href="javascript:;" class="mrgn-l-sm" data-toggle="collapse">
                                                                    <span aria-hidden="true">
                                                                        <i class="fa fa-times fa-lg gray" aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="sr-only">Close</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="panel5" class="collapse in" aria-expanded="true">
                                                    <div class="panel-body">
                                                        <div class="form-group has-feedback">
                                                            <asp:TextBox placeholder="Enter your New Password" TextMode="Password" CssClass="form-control" ID="txtpassword" runat="server"></asp:TextBox>
                                                                                                                    <span class="glyphicon glyphicon-lock form-control-feedback fa-lg" aria-hidden="true"></span>

                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <asp:TextBox placeholder="Retype Password" TextMode="Password" CssClass="form-control" ID="txtretype" runat="server"></asp:TextBox>
                                                                                                                    <span class="glyphicon glyphicon-lock form-control-feedback fa-lg" aria-hidden="true"></span>

                                                        </div>
                                                      
                                                        <p>
                                                            <asp:Button ID="btnsubmit" OnClick="btnsubmit_Click" CssClass="btn btn-primary btn-rounded btn-lg" runat="server" Text="Submit" />
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="/office/assets/js/vendor.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/plugins.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/pratham.min.js" type="text/javascript"></script>
</body>

</html>
