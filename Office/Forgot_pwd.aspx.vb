﻿
Partial Class Office_Forgot_pwd
    Inherits System.Web.UI.Page

    Protected Sub btnsend_Click(sender As Object, e As EventArgs)
        If txtemail.Text = "" Then
            msgbox1.ShowError("Please Enter your email address")
            Exit Sub

        End If
        If mod_main.IsValidEmail(txtemail.Text) = False Then
            msgbox1.ShowError("Please enter a valid email address")
            Exit Sub

        End If
        Dim rec As List(Of Fastcyclepro.Registration) = (New Cls_Registration).SelectThisemailist(txtemail.Text)
        If rec.Count = 0 Then
            'show error
            msgbox1.ShowError("This email address does not match our records.")
            Exit Sub
        Else
            Dim A As New Cls_Registration
            Dim use As Fastcyclepro.Registration = A.SelectThisemail(txtemail.Text)
            Dim code As String = Guid.NewGuid.ToString
            use.Field10 = code
            Dim res As ResponseInfo = A.Update(use)
            If res.ErrorCode = 0 Then
                Dim url As String = String.Format("{0}?M={1}&C={2}", ConfigurationManager.AppSettings("pathtoresetpwd"), use.Email, code)
                Dim body As String = mod_main.PopulateBodyforgotpwd(use.UserName, url)

                mod_main.SendHtmlFormattedEmail(use.Email, "", "Reset your Fastcyclepro Password", body)
                msgbox1.Showsuccess("A Password Reset link has been sent to your email address.")
                txtemail.Text = ""
            End If
            'send password to email


        End If
    End Sub
End Class
