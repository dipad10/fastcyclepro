﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Signup.aspx.vb" Inherits="Office_Signup" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox1" TagPrefix="uc1" %>

<!DOCTYPE html>

<html class="no-js" lang="en">

<!-- Mirrored from pratham.theironnetwork.org/demo/default/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Mar 2017 21:55:25 GMT -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/office/assets/css/vendor.min.css" />
    <link rel="stylesheet" type="text/css" href="/office/assets/css/plugins.min.css" />
    <link rel="stylesheet" type="text/css" href="/office/assets/css/pratham.min.css" />
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Fastcyclepro SignUp</title>
</head>
<body style="background: url(/assets/business1.jpg) no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
    <form runat="server" id="form1">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="prtm-wrapper">
            <div class="prtm-main">
                <%-- <div class="login-banner">
                    <img src="/office/assets/img/login-banner.jpg" alt="login banner" class="img-responsive" width="1920" height="246">
                </div>--%>
                <div class="login-form-wrapper mrgn-b-lg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-5 center-block">
                                <div class="prtm-form-block prtm-full-block overflow-wrappper">
                                    <div class="login-bar">
                                        <img src="/office/assets/img/login-bars.png" class="img-responsive" alt="login bar" width="743" height="7" />
                                    </div>
                                    <div class="prtm-block-title text-center">
                                        <div class="mrgn-b-lg">
                                            <a href="javascript:;">
                                                <img src="/assets/img/Fastcyclepro.png" alt="login logo" class="img-responsive display-ib" width="218" height="23" />
                                            </a>
                                        </div>
                                        <div class="login-top mrgn-b-lg">
                                            <div class="mrgn-b-md">
                                                <h2 class="text-capitalize base-dark font-2x fw-normal">Sign Up </h2>
                                                <asp:Label ID="lblref" CssClass="fw-bold font-sm" runat="server" Text=""></asp:Label>
                                            </div>
                                            <p>This process should not take more than one minute.</p>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <uc1:msgbox1 ID="msgbox1" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <asp:Panel ID="Panelsignup" runat="server">
                                        <div class="prtm-block-content">

                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Fullname" CssClass="form-control" ID="txtfullname" runat="server"></asp:TextBox>

                                                <span class="glyphicon glyphicon-user form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <div class="selectbox">
                                                    <asp:DropDownList ID="ddlgender" CssClass="form-control" runat="server">
                                                        <asp:ListItem Selected="true" Value="null">-Select Gender-</asp:ListItem>
                                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>


                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                     <div class="mrgn-b-md">
                                                <div class="form-group">
                                                    <div class="selectbox">
                                                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged" ID="ddlcountry" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox placeholder="Phone number" CssClass="form-control" ID="txtphone" runat="server"></asp:TextBox>
                                                </div>

                                            </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                           




                                            <asp:TextBox Visible="false" placeholder="Your Bitcoin Wallet Address(OPTIONAL)" CssClass="form-control" ID="txtwalletaddress" runat="server"></asp:TextBox>


                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Email Address" CssClass="form-control" ID="txtemail" runat="server"></asp:TextBox>

                                                <span class="glyphicon glyphicon-envelope form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Your Username" CssClass="form-control" ID="txtusername" runat="server"></asp:TextBox>

                                                <span class="glyphicon glyphicon-user form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Your Password" CssClass="form-control" TextMode="Password" ID="txtpassword" runat="server"></asp:TextBox>

                                                <span class="glyphicon glyphicon-lock form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <asp:TextBox placeholder="Confirm Password" CssClass="form-control" TextMode="Password" ID="txtconfirmpassword" runat="server"></asp:TextBox>

                                                <span class="glyphicon glyphicon-lock form-control-feedback fa-lg" aria-hidden="true"></span>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <div class="g-recaptcha" data-sitekey="6LcflCcUAAAAABU6bkwRlNIeZZiBpbTrVOJSdTKP"></div>
                                            </div>
                                            <div class="login-meta mrgn-b-lg">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"><span class="text-capitalize">I Agree with the <a class="fw-bold" href="/terms">Terms of Use</a> </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mrgn-b-lg">
                                                <asp:Button ID="btnsignup" OnClick="btnsignup_Click" runat="server" CssClass="btn btn-success btn-block font-2x" Text="Sign Up" />

                                            </div>
                                            <div class="text-center">
                                                <h5 class="base-dark">Already have an account? <a href="/office/signin.aspx" class="text-primary">Sign In</a></h5>
                                            </div>
                                            <div class="text-center"><a class="back-home-btn" href="/default.aspx"><i class="fa fa-long-arrow-left mrgn-r-xs"></i>Back To Home</a> </div>

                                        </div>
                                    </asp:Panel>
                                    <asp:Panel Visible="false" ID="panelconfirmation" runat="server">
                                        <div class="prtm-block-content">
                                            <div class="panel panel-white">
                                                <div class="panel-heading panel-success-border">
                                                    <div class="row">
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
                                                            <h3 class="panel-title text-capitalize">Welcome to Fastcyclepro</h3>
                                                        </div>
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                                            <div class="pull-right">
                                                                <a href="#panel5" data-toggle="collapse" class="" aria-expanded="true"><i class="fa fa-minus fa-lg gray" aria-hidden="true"></i></a>
                                                                <a href="javascript:;" class="mrgn-l-sm" data-toggle="collapse">
                                                                    <span aria-hidden="true">
                                                                        <i class="fa fa-times fa-lg gray" aria-hidden="true"></i>
                                                                    </span>
                                                                    <span class="sr-only">Close</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="panel5" class="collapse in" aria-expanded="true">
                                                    <div class="panel-body">
                                                        <p class="">Congratulations, and welcome to Fastcyclepro! The last step is to confirm your email address.</p>
                                                        <p class="">We've sent a confirmation email to <b runat="server" id="txtconfirmail"></b>. Please follow the link in that mail to confirm your email address and then you'll be able to sign in to your new Account</p>
                                                        <p class="">If you haven't received the email in five minutes, please check the following:</p>
                                                        <p class=""><b>-Check if you have used the correct mail.</b></p>
                                                        <p class=""><b>-Check your spam folder.</b></p>
                                                        <p class=""><b>-Contact us at support@Fastcyclepro.com</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="/office/assets/js/vendor.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/plugins.min.js" type="text/javascript"></script>
    <script src="/office/assets/js/pratham.min.js" type="text/javascript"></script>


</body>

</html>
