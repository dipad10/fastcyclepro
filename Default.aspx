﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Src="~/Controls/Fastcycleproheader.ascx" TagName="Fastcycleproheader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Fastcycleprofooter.ascx" TagName="Fastcycleprofooter" TagPrefix="uc2" %>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <title>Fastcyclepro.com | Earn With Pride!</title>
    <%-- google addsense --%>
  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Growing Your bitcoins? Think Fastcyclepro!" />
    <meta name="author" content="bitcoin, investment, grow your bitcoins, let's start Earning, Fastcyclepro.com, bit, flash" />
    <meta name="keywords" content="bitcoin, investment, grow your bitcoins, let's start Earning, Fastcyclepro.com, 2017, paying, not hyip, bitcoins, blockchain." />
    <meta name="theme-color" content="#3a526a" />
    <meta property="og:image" content="http://fastcyclepro.com/snip.png">
    <!-- Favicon Images -->
    <link rel="icon" href="assets/img/favicon/cropped-favicon-32x32.png" />
    <link rel="icon" href="assets/img/favicon/cropped-favicon-192x192.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="assets/img/favicon/cropped-favicon-270x270.png" />

    <!-- Google Font Import -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">

    <!-- Animation, Icon Fonts, Bootsrap styles -->
    <link rel='stylesheet' href='assets/stylesheets/vendor/aos.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/material-icons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/socicon.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/bootstrap.min.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/preloader.css' type='text/css' media='screen' />
    <link rel='stylesheet' href='assets/stylesheets/vendor/demo-navigation.css' type='text/css' media='screen' />

    <!-- *************************************************************************************** -->
    <!-- Vendor Styles Section -->

    <!-- Vendor Styles Section END-->
    <!-- *************************************************************************************** -->

    <!-- Main Theme Style -->
    <link rel='stylesheet' href='assets/stylesheets/theme.min.css' type='text/css' media='screen' />

    <!-- Modernizr Scripts -->
    <script type='text/javascript' src='assets/js/vendor/modernizr.custom.js'></script>

    <!-- Init For Page Prelodaer -->
    <script type="text/javascript">
        (function () {
            window.onload = function () {
                var body = document.querySelector("body");
                var header = body.querySelector(".site-header");
                var preloader = body.querySelector(".loading-screen");
                body.style.overflowY = "auto";
                preloader.classList.add("loading-done");
                header.classList.add("loading-done");
            };
        })();
		</script>

</head>

<body>

    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-K57FHH"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K57FHH');
		</script>
    <!-- End Google Tag Manager -->

    <!-- It Is Preloader Markup -->
    <div class="loading-screen">
        <div class="spinner-wrap">
            <div class="spinner" id="spinner_one"></div>
            <div class="spinner" id="spinner_two"></div>
            <div class="spinner" id="spinner_three"></div>
            <div class="spinner" id="spinner_four"></div>
        </div>
    </div>

    <%--<div class="customizer">
		  <div class="toggle-wrap">
		    <div class="toggle">
		      <i class="material-icons settings"></i> <span>Options</span>
		    </div>
		  </div>

		  <div class="customizer-wrap">
		    <a class="btn btn-block" href="https://themeforest.net/item/startapp-multipurpose-corporate-html5-template/19535166?ref=8guild" target="_blank">Buy Theme</a>
		    <h4>
		      Choose Demo
		    </h4>
		    <a href="http://the8guild.com/themes/html/startapp-html/default/index.html">
		    	<img alt="Default Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/01.jpg"> <span class="text-lg">Default Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/financial/index.html">
		    	<img alt="Financial Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/02.jpg"> <span class="text-lg">Financial Demo</span>
		    </a>
		    <a href="index-2.html">
		    	<img alt="Consulting Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/03.jpg"> <span class="text-lg">Consulting Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/saas/index.html">
		    	<img alt="SaaS Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/04.jpg"> <span class="text-lg">SaaS Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agency/index.html">
		    	<img alt="Agency Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/05.jpg"> <span class="text-lg">Agency Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/app-showcase/index.html">
		    	<img alt="App Showcase Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/06.jpg"> <span class="text-lg">App Showcase Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/event/index.html">
		    	<img alt="Event Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/07.jpg"> <span class="text-lg">Event Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/coworking/index.html">
		    	<img alt="Coworking Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/08.jpg"> <span class="text-lg">Coworking Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/photography/index.html">
		    	<img alt="Aerial Photography Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/09.jpg"> <span class="text-lg">Aerial Photography Demo</span>
		    </a>
		    <a href="http://the8guild.com/themes/html/startapp-html/agritech/index.html">
		    	<img alt="Agri-Tech Demo" src="../../../../../startapp.8guild.com/agritech/wp-content/plugins/startapp-customizer/assets/img/10.jpg"> <span class="text-lg">Agri-Tech Demo</span>
		    </a>
		  </div>
		</div>--%>

    <!-- Offcanvas Sidebar -->

    <!-- Offcanvas Sidebar Mobile Menu END -->

    <uc1:Fastcycleproheader ID="Fastcycleproheader" runat="server" />


    <main>
			<article class="page">
<%--                <section class="fw-section mobile-center with-overlay bg-parallax padding-top-8x padding-bottom-8x" data-parallax-speed="0.4" data-parallax-type="scale" id="home" style="background-image: url(assets/img/man-1718099.jpg); background-attachment: scroll; background-size: auto;" data-jarallax-original-styles="background-image: url(assets/img/man-1718099.jpg);">--%>
			  <section class="fw-section with-overlay bg-parallax padding-top-8x padding-bottom-8x" data-parallax-speed="0.4" data-parallax-type="scroll" style="background-image: url(assets/img/bitcoin-1813503.jpg);">
			    <span class="overlay" style="opacity: 0.2; background-color: #3a526a;"></span>

			    <div class="container">
	          <div class="text-block huge-text">
	            <h1 style="text-align: center;">
	              <span class="text-light text-semibold">Join the all new Fastcycle Turbo!</span>
	            </h1>
	          </div>

	        <%--  <div class="row">
	            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
	              <div class="text-block">
	                <h4 style="text-align: center;">
	                  <span class="text-light text-thin"></span>
	                </h4>
	              </div>
	            </div>
	          </div>--%>

                    <div class="row">
	            <div class="col-sm-6 col-md-offset-3 text-center header-content-feature">
	              <div class="text-block">
                         <span style="text-align: center; border-radius:10px;">
                        <iframe width="150" height="200" src="https://www.youtube.com/embed/L5MvCc6e1ts" frameborder="0" allowfullscreen></iframe>
	                </span>
                    <%--  <div class="counter">
        <span class="text-sm text-bold"><b>Get your Bitcoins Ready!!!</b></span>

<h3><b>FastCycle Turbo Will Prelaunch in</b> </h3>
<div id="countdown">
  
</div><!-- /#Countdown Div -->
</div>--%>

	             
	              </div>
	            </div>
	          </div>

	          <div class="row padding-top-1x">
	            <div class="col-sm-12 text-center">
	              <a class="btn btn-solid btn-rounded btn-warning btn-nl waves-effect waves-light" href="/office/signin"><i class="fa fa-lock"></i> Login</a>
	              <a class="btn btn-solid btn-rounded btn-primary btn-nl waves-effect waves-light" href="/office/signup"><i class="fa fa-child"></i> Join Now! with 0.005 Btc</a>
	            </div>
			      </div>
			    </div>
			  </section>

                <style>
                    .text {
  border-bottom: 1px solid #262626;
  margin-top: 40px;
  padding-bottom: 40px;
  text-align: center;
}

.text h2 {
  color: #E5E5E5;
  font-size: 30px;
  font-style: normal;
  font-variant: normal;
  font-weight: lighter;
  letter-spacing: 2px;
}
                    .counter {
  background: #18ba60;
  -moz-box-shadow:    inset 0 0 5px #000000;
  -webkit-box-shadow: inset 0 0 5px #000000;
  box-shadow:         inset 0 0 5px #000000;
  min-height: 150px;
  text-align: center;
  border-radius:10px;
}

.counter h3 {
  color: #E5E5E5;
  font-size: 14px;
  font-style: normal;
  font-variant: normal;
  font-weight: lighter;
  letter-spacing: 1px;
  padding-top: 20px;
  margin-bottom: 30px;
}
.counter span {
  color: #E5E5E5;
  font-size: 20px;
  font-style: normal;
  font-variant: normal;
  font-weight: lighter;
  letter-spacing: 1px;
  padding-top: 20px;
  margin-bottom: 30px;
}

#countdown {
  color: #FFFFFF;
}

#countdown span {
  color: #E5E5E5;
  font-size: 26px;
  font-weight: normal;
  margin-left: 20px;
  margin-right: 20px;
  text-align: center;
}

                </style>
                <%-- For rate table --%>
                <style>
                    .rate-table {
                        background-color: #fff;
                        border-top: 1px solid #dee7f2;
                        border-bottom: 1px solid #dee7f2;
                        margin-top: -1px;

                    }
                    .rate-counter-block {
    border-right: 1px solid #dee7f2;
    padding-top: 20px;
    padding-bottom: 20px;
}
                    .rate-icon {
    float: left;
    padding-top: 5px;
    padding-right: 20px;
    padding-left: 20px;
}
                    .loan-rate {
    font-size: 32px;
    color: #3a526a;
    margin-bottom: 0px;
    font-weight: 600;
    line-height: 1;
}
                    .rate-title {
    text-transform: uppercase;
    font-size: 14px;
    color: #778191;
}
                  
                </style>
                <div class="rate-table">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="rate-counter-block aos-init aos-animate" data-aos="flip-left">
                        <div class="icon rate-icon  "> <img src="assets/img/024-bitcoin-5.png" width="50" height="50" alt="Total Upgrades" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate"><b runat="server" id="totupgrade">0</b></h1>
                            <small class="rate-title">Total Upgrades</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="rate-counter-block aos-init aos-animate" data-aos="flip-left">
                        <div class="icon rate-icon  "> <img src="assets/img/032-bitcoin-1.png" width="50" height="50" alt="Total Cycled" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate"><b runat="server" id="totcycled">0</b></h1>
                            <small class="rate-title">Total Cycled</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="rate-counter-block aos-init aos-animate" data-aos="flip-left">
                        <div class="icon rate-icon  "> <img src="assets/img/004-bitcoin-17.png" width="50" height="50" alt="Total Deposit" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate"><i class="fa fa-bitcoin"></i><b runat="server" id="totdeposit">0</b></h1>
                            <small class="rate-title">Total Deposits</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="rate-counter-block aos-init aos-animate" data-aos="flip-left">
                        <div class="icon rate-icon"> <img src="assets/img/029-money-bag.png" width="50" height="50" alt="Total Payouts" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate"><i class="fa fa-bitcoin"></i><b runat="server" id="totpayout">0.0000</b></h1>
                            <small class="rate-title">Total Payouts</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
         
                <section style="background-color:#f4f6f8;" class="fw-section padding-top-3x padding-bottom-1x">
			    <div class="container">
	          <h2 class="block-title text-dark text-center">
	           Let us help you Grow Your Bitcoins<small class="h4">We will match you with our 3 basic program that meet your financial need.</small>
	          </h2>
                    <div class="row">
			        <div class="col-sm-4">
			          <div class="icon-box icon-box-vertical icon-box-dark icon-box-bg-enabled icon-box-type-image bg-hover hover-scale-down text-center" data-aos="zoom-in">
			            <span class="icon-box-backdrop" style="background-color: #fff;"></span>

			           <div class="vc-icon-type-icon text-primary text-center mobile-center">
			               <img src="assets/img/005-bitcoin-16.png" width="100" height="100" alt="Earn unlimited with cycler" class="icon-svg-1x">
			              </div>

			            <h3 style="color:#18ba60" class="icon-box-title text-bold text-info">
			             
                       Earn Unlimited with Cycler
			            </h3>

			            <div class="icon-box-description">
			              <p>
			                <span class="opacity-75">Earn amazingly from World's Fastest Full automated straight-line cycler where you can enter with a minimum of 0.005 Bitcoins and earn up to 0.15 Bitcoins. Get on board now with the people who are already making 0.1 to 1.2 Bitcoins every month!</span>
			              </p>
			            </div>
			          </div>
			        </div>
                       
			        <div class="col-sm-4">
			          <div class="icon-box icon-box-vertical icon-box-light icon-box-bg-enabled icon-box-type-image bg-hover hover-from-bottom text-center" data-aos="zoom-in">
			            <span class="icon-box-backdrop" style="background-color: #fff;"></span>

			           <div class="vc-icon-type-icon text-primary text-center mobile-center">
			              <img src="assets/img/026-bitcoin-3.png" width="100" height="100" alt="Cloud mining" class="icon-svg-1x">
			            </div>

			            <h3 style="color:#18ba60" class="icon-box-title text-bold text-info">
			            Cryptocurrency Cloud Mining
			            </h3>

			            <div class="icon-box-description">
			              <p>
			                <span class="opacity-75">We offer our Bitcoin Cloud mining Service(The Fastcycle Turbo) Which enables you to earn 3.7% Daily Profit, Interest accrued Hourly!.</span>
			              </p>
			            </div>
			          </div>
			        </div>

			        <div class="col-sm-4">
			          <div class="icon-box icon-box-vertical icon-box-light icon-box-bg-enabled icon-box-type-image bg-hover hover-from-right text-center" data-aos="zoom-in">
			            <span class="icon-box-backdrop" style="background-color: #fff;"></span>

			           <div class="vc-icon-type-icon text-primary text-center mobile-center">
			                <img src="assets/img/011-bitcoin-13.png" width="100" height="100" alt="secured" class="icon-svg-1x">
			              </div>

			            <h3 style="color:#18ba60" class="icon-box-title text-bold text-info">
			         Secured and anonymous
			            </h3>

			            <div class="icon-box-description">
			              <p>
			                <span class="opacity-75">Communications between you and our servers are always encrypted. All your actions are hidden from 3rd parties guaranteeing you full anonymity.</span>
			              </p>
			            </div>
			          </div>
			        </div>
			      </div>
	          

	          
			    </div>
			  </section>
                
			
                <section class="fw-section layout-full-equal section-no-gap">
                      <form runat="server" id="form4">
			    <div class="container-fluid">
			      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12 padding-top-3x padding-bottom-2x" style="background-color: #506982; padding-left: 96px !important; padding-right: 96px !important;">
	             <h2 class="block-title text-light text-center">
	         <i class="fa fa-bitcoin"></i> FastCycleTurbo Profit Calculator
	          </h2>
                          <p class="text-center text-light">Experience the difference in our Cryptocurrency cloud mining with our state of the art algorithm which allows you earn every second, minute, hour of the day with a minimum of 0.005 Bitcoins. </p>
                          
                          <h2 class="text-bold text-light">3.7% DAILY</h2>
			            <p class="lead">
			              <span class="text-light">0.1542% HOURLY</span>
			            </p>
                          <p>
                              <span class="text-light">Minimum Deposit: 0.005 BTC</span><br />
                              <span class="text-light">Instant Withdrawals</span><br />
                              <span class="text-light">Interest Calculation Every Minute</span>
                          </p>
			        
			        </div>

			        <div class="col-lg-6 col-md-6 col-sm-12 padding-top-3x padding-bottom-2x" style="background-color: #3a526a; padding-left: 96px !important; padding-right: 96px !important;">
                
                             <p class="text-light text-center text-bold">Amount(<i class="fa fa-bitcoin"></i>) <asp:TextBox onchange="MultiplyByHidden()" onkeyup="MultiplyByHidden()" onkeypress="MultiplyByHidden(); return numberOnly(this, event)" ID="txtbtcamount" Width="100%" style="border-radius:10px;" Text="0.01" runat="server"></asp:TextBox></p>
                    <table style="width:100%; border:none;">
                        <tr>
                            <td>
                                <span class="text-light text-left">Hourly Interest</span>
                            </td>
                              <td>
                               <span class="text-light text-right"><asp:Label ID="lblhourlyInterest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-light text-left">Daily Interest</span>
                            </td>
                              <td>
                               <span class="text-light text-right"><asp:Label ID="lbldailyInterest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-light text-left">Weekly Interest</span>
                            </td>
                              <td>
                               <span class="text-light text-right"><asp:Label ID="LblweeklyIntrest" runat="server" Text="0.00000000"></asp:Label> BTC</span>
                            </td>
                        </tr>
                    </table>
                      <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
			        </div>

			        
			      </div>
			    </div>
                           </form>
			  </section>



               
              
         
			
			  

                 <style>
                     @media(min-width: 992px) {
                         .col5 {
                             width: 20%;
                             float: left;
                             position: relative;
                             min-height: 1px;
                             padding-right: 15px;
                             padding-left: 15px;
                         }
                     }
                 </style>
			 <section class="fw-section padding-top-3x padding-bottom-4x">
                    
			    <div class="container">
	          <h2 class="block-title text-dark text-center">
	         <img src="/assets/rocket.png" width="28" height="30" /> FastCyclePro Matrix Plan
	          </h2>
                   
    
	        </div>
              <div class="container">
                  <div class="table-responsive">
                      <table class="table table-bordered table-hovered aos-init aos-animate table-striped" data-aos="fade-up">
                        <thead style="font-weight: bold; background-color: #18ba60; color: #fff;">
                          <tr>
                            <th>Level</th>
                            <th>Referrals</th>
                            <th>Donation (BTC)</th>
                            
                            <th>Total (BTC)</th>
                            <th>Profit On cycling (BTC) </th>
                                
                            <th>Commission Per Referral</th>
                            <th>New Position in Level 0/Blazer</th>
                            <th>Upgrade Cost</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><b>0</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.015</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.045</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.005</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.005</b></td>
                            <td><b>0</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.02</b></td>
                          </tr>
                          
                           <tr>
                            <td><b>1</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.02</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.06</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.01</b></td>
                            <td><b> -</b></td>
                            <td><b>0</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.05</b></td>
                          </tr>
                          
                           <tr>
                            <td><b>2</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.05</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.15</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.135</b></td>
                            <td><b>-</b></td>
                            <td><b>1</b></td>
                            <td><b>-</b></td>
                          </tr>

                   <%--       <tr>
                            <td><b>3</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.10</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.30</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.0898</b></td>
                            <td><b>-</b></td>
                            <td><b>1</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.20</b></td>
                          </tr>

                          <tr>
                            <td><b>4</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.20</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.60</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.1898</b></td>
                            <td><b>-</b></td>
                            <td><b>1</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.40</b></td>
                          </tr>

                          <tr>
                            <td><b>5</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.40</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 1.20</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 1.1898</b></td>
                            <td><b>-</b></td>
                            <td><b>1</b></td>
                            <td><b>-</b></td>
                          </tr>--%>

                     

                          <tr style="font-weight: bolder;">
                            <td style="background-color: #18ba60; color: #fff;">Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="background-color: #18ba60; color: #fff;">0.1548 BTC(Over & Over Again)</td>
                            <td></td>
                            <td style="background-color: #18ba60; color: #fff;">1</td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  
                  
              </div>
			  </section>
                <section style="background-color:#f5f5f5;" class="fw-section padding-top-3x padding-bottom-4x">
                     <div class="container">
	          <h2 class="block-title text-dark text-center">
	          <img src="/assets/fire.png" width="28" height="30" /> FastCycle Blazer Matrix
	          </h2>
                   
	        </div>
              <div class="container">
                  <div class="table-responsive">
                      <table class="table table-bordered table-hovered aos-init aos-animate table-striped" data-aos="fade-up">
                        <thead style="font-weight: bold; background-color: #18ba60; color: #fff;">
                          <tr>
                            <th>Level</th>
                            <th>Referrals</th>
                            <th>Donation (BTC)</th>
                            
                            <th>Total (BTC)</th>
                            <th>Profit On cycling (BTC) </th>
                                
                            <th>Commission Per Referral</th>
                            <th>New Position in Level 0</th>
                            <th>Upgrade Cost</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><b>Fastcycle Blazer</b></td>
                            <td><b>3</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.005</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.015</b></td>
                            <td><b><i class="fa fa-bitcoin"></i> 0.01</b></td>
                            <td><b>-</b></td>
                            <td><b>1</b></td>
                            <td><b><i class="fa fa-bitcoin"></i>0.01</b></td>
                          </tr>
                          
                     
                          <tr style="font-weight: bolder;">
                            <td style="background-color: #18ba60; color: #fff;">Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="background-color: #18ba60; color: #fff;">0.01 BTC(Over & Over Again)</td>
                            <td></td>
                            <td style="background-color: #18ba60; color: #fff;">1</td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  
                  
              </div>
                    </section>
			

			  <section class="fw-section with-overlay padding-top-3x padding-bottom-3x mobile-center" style="background-image: url(assets/img/demo-marketing-bg-01.jpg);">
			    <span class="overlay" style="opacity: 0.65; background-color: #3a526a;"></span>

			    <div class="container">
			      <div class="row">
			        <div class="col-sm-8">
			          <div class="text-block">
                          <h2 style="color:white;">Our Affiliate Program</h2>
                          <hr />
			            <h4>
			              <span class="text-light">Earn 3% of your partner's deposit in our cryptocurrency cloud mining and also upto 0.005 Referral commissions in our straightline Cycler. It's free!</span>
			            </h4>
			          </div>
			        </div>

			        <div class="col-sm-4">
	              <div class="padding-top-1x visible-lg visible-xs"></div>
	              <div class="padding-top-2x visible-md visible-sm"></div>

			          <div class="text-center">
			            <a class="btn btn-transparent btn-rounded btn-light btn-nl waves-effect waves-light margin-top-3x margin-bottom-3x" href="/office/signup.aspx">Sign Up Today</a>
			          </div>
			        </div>
			      </div>
			    </div>
			  </section>

			  
			</article>

			<!-- Main Footer -->
                <uc2:Fastcycleprofooter ID="Fastcycleprofooter" runat="server" />

			<!-- Main Footer END -->
		</main>

    <a class="scroll-to-top-btn" href="#"><i class="material-icons keyboard_arrow_up"></i></a>
    <div class="site-backdrop"></div>

    <!-- Scripts section -->
    <!-- **************************************************************************************************************************** -->
    <script type='text/javascript' src='assets/js/vendor/jquery.js'></script>

    <script type='text/javascript' src='assets/js/vendor/aos.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.waypoints.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/bootstrap.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jarallax.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/velocity.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/waves.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/scrollspy.js'></script>
    <script type='text/javascript' src='assets/js/vendor/slick.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/isotope.pkgd.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/counterup.min.js'></script>
    <script type='text/javascript' src='assets/js/vendor/jquery.magnific-popup.min.js'></script>
    <!-- *************************************************************************************** -->
    <!-- Vendor Script Section -->

    <!-- Vendor Script Section END-->
    <!-- *************************************************************************************** -->
    <script type='text/javascript' src='assets/js/startapp-theme.js'></script>
    <!-- **************************************************************************************************************************** -->
    <!-- Scripts section END -->
    <%--<script>
        // set the date we're counting down to
        var target_date = new Date('Oct, 14, 2017').getTime();

        // variables for time units
        var days, hours, minutes, seconds;

        // get tag element
        var countdown = document.getElementById('countdown');

        // update the tag with id "countdown" every 1 second
        setInterval(function () {

            // find the amount of "seconds" between now and target
            var current_date = new Date().getTime();
            var seconds_left = (target_date - current_date) / 1000;

            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;

            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;

            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);

            // format countdown string + set tag value
            countdown.innerHTML = '<span class="days">' + days + ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
            + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';

        }, 1000);
    </script>--%>

    <%-- block letters on typing btc amount --%>

    <script type="text/javascript">
        function numberOnly(txt, e) {
            var arr = "0123456789.";
            var code;
            if (window.event)
                code = e.keyCode;
            else
                code = e.which;
            var char = keychar = String.fromCharCode(code);
            if (arr.indexOf(char) == -1)
                return false;
            else if (char == ".")
                if (txt.value.indexOf(".") > -1)
                    return false;
        }
    </script>
      <%-- calculate hourly daily weekly intrest --%>
    <script type="text/javascript">
        function MultiplyByHidden() {

            var value1 = parseFloat(document.getElementById("<%=txtbtcamount.ClientID%>").value)
            var val1 = 3.7 / 100 * value1 / 24
            var val2 = 3.7 / 100 * value1
            var val3 = 3.7 / 100 * value1 * 7
            //var val4 = 2 / 100 * value1
            document.getElementById("<%=lblhourlyInterest.ClientID%>").innerText = val1.toFixed(8)
            document.getElementById("<%=lbldailyInterest.ClientID%>").innerText = val2.toFixed(8)
            document.getElementById("<%=LblweeklyIntrest.ClientID%>").innerText = val3.toFixed(8)
          <%--  document.getElementById("<%=lbltransactionfees.ClientID%>").innerText = val4.toFixed(8)
            document.getElementById("<%=txttransfees.ClientID%>").value = val4.toFixed(8)--%>

        }
</script>
</body>

</html>
