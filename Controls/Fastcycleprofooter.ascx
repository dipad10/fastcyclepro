﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Fastcycleprofooter.ascx.vb" Inherits="Controls_Fastcycleprofooter" %>
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1873832552939949";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<footer class="site-footer footer-dark bg-parallax" data-parallax-speed="0.4" data-parallax-type="scroll" style="background-image: url(assets/img/demo-marketing-bg-02.jpg);">
    <div class="footer-row">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <section class="widget widget_startapp_site_info">
                        <a href="#">
                            <img alt="" src="/assets/bitcoin-sign.jpg">
                        </a>
                        <a href="#">
                            <img alt="" src="/assets/img/Blockchain-Logo-Blue6.png">
                        </a>
                       <%-- <p>
                            Fastcyclepro is Powered By blockchain and accept bitcoins only. No Local currencies.
		             
                        </p>--%>

                        <div class="social-bar sb-solid-bg sb-square sb-dark-skin text-left">
                            <a class="social-btn waves-effect waves-light" data-placement="top" data-toggle="tooltip" href="https://www.facebook.com/fastcyclepro" title="Facebook"><i class="socicon-facebook"></i></a>
                            <a class="social-btn waves-effect waves-light" data-placement="top" data-toggle="tooltip" href="https://chat.whatsapp.com/7VmohiPJQUf9s9mKoArYwm" title="Whatsapp"><i class="socicon-whatsapp"></i></a>
                            <a class="social-btn waves-effect waves-light" data-placement="top" data-toggle="tooltip" href="https://t.me/joinchat/Fy70LgulsSWwt38YZOUtdw" title="Telegram"><i class="socicon-telegram"></i></a>
                        </div>
                    </section>

                    <section class="widget widget_startapp_contacts">
                        <h3 class="widget-title">Get In Touch
		              </h3>

                        <div class="contact-item">
                            <div class="contact-icon">
                                <i class="material-icons chat"></i>
                            </div>

                            <div class="contact-info">
                                <span><a href="/default">Live chat</a></span>
                            </div>
                        </div>



                        <div class="contact-item">
                            <div class="contact-icon">
                                <i class="socicon-mail"></i>
                            </div>

                            <div class="contact-info">
                                <span>support@Fastcyclepro.com</span>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-md-3 col-sm-6">
                    <section class="widget widget_nav_menu">
                        <h3 class="widget-title">Like Our page
		              </h3>

                        <div class="menu-footer-menu-1-container">
                            <div class="fb-page" data-height="360" data-href="https://www.facebook.com/FastcyclePro-1940840066194765/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/FastcyclePro-1940840066194765/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FastcyclePro-1940840066194765/">Fastcyclepro.com</a></blockquote>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="clearfix visible-sm">
                </div>

                <div class="col-md-3 col-sm-6">
                    <section class="widget widget_nav_menu">
                        <h3 class="widget-title">Quick Links
		              </h3>

                        <div class="menu-footer-menu-2-container">
                            <ul class="menu">
                                <li class="menu-item">
                                    <a href="/Aboutus.aspx">About Fastcyclepro</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/howitworks">How it works</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/proof">Payment Proof</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/faq">FAQs</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/getstarted">Getting Started</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/aboutbitcoin">About Bitcoin</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/terms">Terms & Conditions</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>

                <div class="col-md-3 col-sm-6">
                    <section class="widget widget_recent_entries">
                        <h3 class="widget-title">Testimonials
		              </h3>
                    <marquee SCROLLDELAY="300" height="285" direction="up">
                         <ul>
                              
                               <asp:Repeater ID="rptblog" runat="server">
                                   <ItemTemplate>
                                       
                                            <li style="color:#fff;">
                                <span><%#Eval("Testimony")%></span> <span class="post-date"><%#Eval("submittedon", "{0:MMMM d, yyyy}")%></span>
                            </li>
                                             <hr />
                                              
                                   </ItemTemplate>
                               </asp:Repeater>
                       
                                  
                        </ul>
                     </marquee>
                        
                        
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div style="background-color:#3a526a;" class="copyright text-light">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p class="copyright-text">
                        2017 All Rights Reserved.
			         
                    </p>
                </div>

                <div class="col-sm-6 text-right"></div>
            </div>
        </div>
    </div>
</footer>
