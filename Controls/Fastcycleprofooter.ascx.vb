﻿
Partial Class Controls_Fastcycleprofooter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim rec As List(Of Fastcyclepro.Testimonial) = (New cls_testimonials).SelectAlltestimonies
            rptblog.DataSource = rec
            rptblog.DataBind()
        End If
    End Sub
End Class
