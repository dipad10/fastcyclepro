﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="messagebox.ascx.vb" Inherits="msgbox" %>
<asp:Panel ID="PanelError" CssClass="" runat="server" Visible="False">
    <div class="alert bg-danger  base-reverse alert-dismissible fade in" role="alert"> <span><i class="fa fa-bell fa-lg" aria-hidden="true"></i></span>
<button type="button" class="close" onserverclick="btncloseerror_ServerClick" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> <strong>Oh Snap!</strong> <asp:Literal ID="Literal1" runat="server"></asp:Literal> </div>
     
</asp:Panel>

<asp:Panel ID="PanelHelp" CssClass="" runat="server" Visible="false">
      <div class="alert bg-info  base-reverse alert-dismissible fade in" role="alert"> <span><i class="fa fa-bell fa-lg" aria-hidden="true"></i></span>
<button type="button" class="close" onserverclick="btnclosehelp_ServerClick" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> <strong>Need any help!</strong> <asp:Literal ID="Literal2" runat="server"></asp:Literal> </div>
     
</asp:Panel>
<asp:Panel ID="panelsuccess" CssClass="" runat="server" Visible="false">
    <div class="alert bg-success  base-reverse alert-dismissible fade in" role="alert"> <span><i class="fa fa-bell fa-lg" aria-hidden="true"></i></span>
<button type="button" class="close" onserverclick="btnclosesuccess_ServerClick" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> <strong>Well done!</strong> <asp:Literal ID="Literal3" runat="server"></asp:Literal> </div>
     
   
</asp:Panel>

<asp:Panel ID="Panelmodal" Visible="false" runat="server">
    <div id="modalHeaderColorWarning" class="modal-block modal-header-color modal-block-warning mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">Warning!</h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-icon">
														<i class="fa fa-warning"></i>
													</div>
													<div class="modal-text">
														<h4>Warning</h4>
														<asp:Literal ID="Literal4" runat="server"></asp:Literal>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button runat="server" onserverclick="Unnamed_ServerClick" class="btn btn-warning modal-dismiss">OK</button>
													</div>
												</div>
											</footer>
										</section>
									</div>
</asp:Panel>
