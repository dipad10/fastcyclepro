﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Fastcycleproheader.ascx.vb" Inherits="Controls_Header" %>

    <script type="text/javascript">window.$crisp = []; window.CRISP_WEBSITE_ID = "b61eb7df-f806-4ff0-8a71-d5bd57c28605"; (function () { d = document; s = d.createElement("script"); s.src = "https://client.crisp.chat/l.js"; s.async = 1; d.getElementsByTagName("head")[0].appendChild(s); })();</script>
    <!-- Header -->
    <%--<aside class="off-canvas-sidebar right-positioned">
        <span class="close-btn"><i class="material-icons clear"></i></span>

        <!-- Widget Start -->

        <!-- Widget END -->

        <!-- Widget Start -->
        <section class="widget widget_startapp_subscribe">
            <h2 class="widget-title">Newsletter</h2>

            <form method="post" target="_blank" novalidate autocomplete="off">
                <p>Subscribe to our newspaper. Be informed about all latest news and Challenges.</p>

                <div class="input-group">
                    <i class="material-icons mail_outline"></i>
                    <input type="email" name="EMAIL" placeholder="Enter email">
                    <button type="submit"><i class="material-icons send"></i></button>
                </div>
            </form>
        </section>
        <!-- Widget END -->

        <!-- Widget Start -->
        <section class="widget widget_startapp_image_carousel">
            <h2 class="widget-title">Testimonials</h2>

            <div class="widget-inner" data-slick='{"slidesToShow":1,"arrows":false,"autoplay":true,"autoplaySpeed":4000}'>
                <div class="carousel-item">
                    <div class="testimonial testimonial-ava-square text-dark text-center">
                        <div class="testimonial-body">
                            <p>
                                Fastcyclepro is still the best peer to peer bitcoin donation platform because everything is automated. Payments are made instantly
                            </p>
                        </div>
                        <center>
                            <div class="testimonial-cite">
	               

	                  <div class="testimonial-author-info">
	                    Martin Dylan - <b>Canada</b>
	                  </div>
	                </div>
                        </center>

                    </div>
                </div>
                <div class="carousel-item">
                    <div class="testimonial testimonial-ava-square text-dark text-center">
                        <div class="testimonial-body">
                            <p>
                               Now i feel like an Enterpreneur, Fastcyclepro is paying like an atm machine. long live Fastcyclepro!
                            </p>
                        </div>
                        <center>
                            <div class="testimonial-cite">
	               

	                  <div class="testimonial-author-info">
	                  Deji Kolawole - <b>Nigeria</b>
	                  </div>
	                </div>
                        </center>

                    </div>
                </div>

            </div>
        </section>
        <!-- Widget END -->
    </aside>--%>
    <!-- Offcanvas Sidebar END -->

    <!-- Site Search Form -->
    <div class="site-search-form">
        <div class="inner">
            <span class="close-btn"><i class="material-icons clear"></i></span>

            <form method="get" class="search-box" autocomplete="off">
                <input type="text" name="s" placeholder="Search" value="">
                <button type="submit"><i class="material-icons search"></i></button>
            </form>
        </div>
    </div>
    <!-- Site Serch Form END -->

    <!-- Offcanvas Sidebar Mobile Menu -->
    <aside class="off-canvas-menu">
        <span class="close-btn"><i class="material-icons clear"></i></span>

        <!-- Main navigation -->
        <!-- Visible only on mobile -->
        <nav class="vertical-navigation">
            <ul class="menu">
                <li class="menu-item current-menu-item">
                    <a href="/default.aspx">Home<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                </li>

                <li class="menu-item  menu-item-has-children">
                    <a href="#">About<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                    <ul class="sub-menu">
                        <li class="menu-item">
                            <a href="/Aboutus.aspx">About FastcyclePro<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                        </li>

                        <li class="menu-item">
                            <a href="/privacypolicy.aspx">Privacy Policy<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                        </li>

                        <%--   <li class="menu-item">
                            <a href="/howitworks.aspx">How it works<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                        </li>--%>
                    </ul>
                </li>

                <li class="menu-item">
                    <a href="/getstarted">Get Started<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>
                <li class="menu-item">
                    <a href="/Howitworks">How it works<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>

                <li class="menu-item">
                    <a href="/proof">Recent Payouts<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>
                <li class="menu-item">
                    <a href="/blog/posts">News<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>

                <li class="menu-item">
                    <a href="/faq">FAQs<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>

                <li class="menu-item">
                    <a href="/contact">Support<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>


                </li>
                  <li class="menu-item">
                            <a href="/office/signup.aspx">Register<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                        </li>
                 <li class="menu-item">
                            <a href="/office/signin.aspx">Log In<span class="arrow"><i class="material-icons keyboard_arrow_down"></i></span></a>
                        </li>
        



                <script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({ pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE }, 'google_translate_element');
                    }
                </script>
                <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>



            </ul>
        </nav>
        <!-- Main navigation END -->
    </aside>
    <header class="site-header navbar-sticky">
        <div class="topbar topbar-primary text-light">
            <div class="container">
                <div class="inner">
                    <div class="column">
                        <p class="additional-info">
                            <span>Bitcoin Rate: </span><b runat="server" id="btctoday"></b><span id="google_translate_element"></span>
                        </p>
                    </div>

                    <div class="column">
                        <p class="additional-info">
                            <marquee scrolldelay="300" direction="left">
                               <span><i class="fa fa-user"></i> Total Upgrades: <b runat="server" id="totupgrade">0</b></span> <b>,</b> 
                                 <span><i class="fa fa-recycle"></i> Total Cycled: <b runat="server" id="totcycled">0</b></span> <b>,</b>
                                 <span> Total Payouts: <i class="fa fa-bitcoin"></i><b runat="server" id="totpayout">0.0000</b></span> <b>,</b>
                               <span><i class="fa fa-user"></i> Total Users: <b runat="server" id="totuser">0</b></span>
                               </marquee>
                        </p>
                    </div>

                    <div class="column text-right">
                        <nav class="topbar-menu">
                            <ul class="menu">
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-706">
                                    <a href="/aboutbitcoin.aspx">
                                       <i style="font-size:xx-large" class="fa fa-bitcoin fa-4x"></i>?</a>
                                </li>
                                          
                                                 <a class="btn btn-ghost btn-rounded btn-success btn-sm waves-effect waves-light" href="/office/signup"><i class="fa fa-user-plus"></i> Signup</a>
                                                                                         <a class="btn btn-ghost btn-rounded btn-success btn-sm waves-effect waves-light" href="/office/signin"><i class="fa fa-sign-in"></i> Login</a>
  
                             <%--   <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-707">
                                    <a href="#"><i class="fa fa-user"></i>My Account</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-708">
                                            <a href="/office/signup"><i class="fa fa-child"></i>Sign Up</a>
                                        </li>

                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-709">
                                            <a href="/office/signin"><i class="fa fa-lock"></i>Sign In</a>
                                        </li>


                                    </ul>
                                </li>--%>
                            </ul>
                        </nav>

                        <div class="social-bar sb-solid-bg sb-rounded sb-dark-skin text-left">
                            <a class="social-btn" href="https://www.facebook.com/groups/2339699896255343/"><i class="socicon-facebook"></i></a>
                            <a class="social-btn" href="#"><i class="socicon-telegram"></i></a>
                            <a class="social-btn" href="https://chat.whatsapp.com/7VmohiPJQUf9s9mKoArYwm"><i class="socicon-whatsapp"></i></a>
                        </div>

                        <div class="toolbar">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-regular menu-right">
            <div class="container">
                <div class="inner">

                    <!-- Main Logo Column -->
                    <div class="column">
                        <a style="max-width: 200px !Important;" class="site-logo" href="/default.aspx">
                            <img alt="" height="50" width="300" src="/assets/img/Fastcyclepro.png">
                        </a>
                    </div>
                    <!-- Main Logo Column END -->

                    <div class="column">

                        <!-- Main navigation -->
                        <!-- Visible only on desktop -->
                        <nav class="main-navigation">
                            <ul class="menu">
                                <li class="menu-item">
                                    <a href="/default.aspx">Home</a>
                                </li>



                                <li class="menu-item menu-item-has-children">
                                    <a href="#">About</a>

                                    <ul class="sub-menu">
                                        <li class="menu-item">
                                            <a href="/Aboutus.aspx">About Fastcyclepro</a>
                                        </li>

                                        <li class="menu-item">
                                            <a href="/privacypolicy.aspx">Privacy Policy</a>
                                        </li>


                                    </ul>
                                </li>

                                <li class="menu-item">
                                    <a href="/getstarted.aspx">Get Started</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/howitworks.aspx">How it works</a>
                                </li>



                                <li class="menu-item">
                                    <a href="/blog/posts.aspx">News</a>
                                </li>

                                <li class="menu-item">
                                    <a href="/faq.aspx">FAQ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="/contact.aspx">Support</a>
                                </li>







                            </ul>
                        </nav>
                        <!-- Main navigation END -->

                        <!-- Header Toolbar, search, offcanvas toggle etc -->
                        <div class="toolbar">
                            <!-- SIte Search Toggle -->
                            <a class="site-search-btn tool" href="#"><i class="material-icons search"></i></a>

                            <!-- Offcanvas Sidebar Toggle -->
                            <%--  <a class="sidebar-btn tool" href="#"><i class="material-icons more_vert"></i></a>--%>

                            <!-- Topbar Mobile View Toggle -->
                            <!-- Only Mobile view visible -->
                            <a class="topbar-btn tool" href="#"><i class="material-icons more_horiz"></i></a>

                            <!-- Mobile View Navigation Toggle -->
                            <!-- Only Mobile view visible -->
                            <a class="menu-btn tool" data-toggle="offcanvas" href="#"><i class="material-icons menu"></i></a>
                        </div>
                        <!-- Header Toolbar END -->

                        <div class="header-buttons">
                            <%--                      <img src="/assets/img/bitcoin-1294272_1280.png" class="btn btn-solid btn-rounded btn-primary btn-default btn-block waves-effect waves-light" width="90" height="20" />--%>
                            <a runat="server" id="btnproof" class="btn btn-solid btn-rounded btn-primary btn-default btn-block waves-effect waves-light" href="/proof"><i class="fa fa-bitcoin"></i> Latest Payouts</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header END -->


