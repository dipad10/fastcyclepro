﻿Imports System.Net
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class Controls_Header
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'get btc rate
            Try
                Dim uri = [String].Format("https://blockchain.info/tobtc?currency=USD&value={0}", "1")

                Dim client As New WebClient()
                client.UseDefaultCredentials = True
                Dim data = client.DownloadString(uri)

                Dim result = Convert.ToDouble(data)
                btctoday.InnerHtml = "$" & FormatNumber(1 / result, 2)
            Catch ex As Exception
                btctoday.InnerHtml = 0
            End Try

            bindtotpayout()
            bindtotupgrade()
            bindtotcycle()
            bindtotuser()
        End If
    End Sub

    Protected Sub bindtotpayout()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotpayout"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totpayout.InnerHtml = dr("total") + 1.2
                        Catch ex As Exception

                        End Try

                    Else
                        totpayout.InnerHtml = "0.0000"


                    End If

                Else

                    totpayout.InnerHtml = "0.0000"


                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotupgrade()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotupgrade"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totupgrade.InnerHtml = dr("total") + 2419
                        Catch ex As Exception

                        End Try

                    Else
                        totupgrade.InnerHtml = 0


                    End If

                Else

                    totpayout.InnerHtml = 0


                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotcycle()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotcycled"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totcycled.InnerHtml = dr("total") + 1130
                        Catch ex As Exception

                        End Try

                    Else
                        totcycled.InnerHtml = 0


                    End If

                Else

                    totcycled.InnerHtml = 0


                End If

            End Using

        End Using
    End Sub

    Protected Sub bindtotuser()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Fastcyclepro_DBConnectionString").ConnectionString)
            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.StoredProcedure
                mycommand.CommandText = "Gettotusers"

                mycommand.ExecuteNonQuery()

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)

                Dim dr As SqlDataReader = Nothing
                dr = mycommand.ExecuteReader

                If dr.Read() Then
                    If dr("total") IsNot DBNull.Value Then
                        Try


                            totuser.InnerHtml = dr("total") + 2710
                        Catch ex As Exception

                        End Try

                    Else
                        totuser.InnerHtml = 0


                    End If

                Else

                    totuser.InnerHtml = 0


                End If

            End Using

        End Using
    End Sub
End Class
