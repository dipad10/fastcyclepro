﻿
Partial Class msgbox
    Inherits System.Web.UI.UserControl
  


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    Public WriteOnly Property HelpText() As String
        Set(ByVal value As String)
            Me.Literal2.Text = value
            Me.PanelHelp.Visible = True
        End Set
    End Property

    Public Sub ShowHelp(ByVal Message As String)
        Me.PanelHelp.Visible = True

        Me.PanelError.Visible = False
        Me.panelsuccess.Visible = False

        Me.Literal2.Text = Message
    End Sub

    Public Sub ShowError(ByVal Message As String)
        Me.PanelHelp.Visible = False
        Me.panelsuccess.Visible = False
        Me.PanelError.Visible = True
        Me.Literal1.Text = Message

    End Sub
    Public Sub Showmodal(ByVal Message As String)
        Me.PanelHelp.Visible = False
        Me.panelsuccess.Visible = False
        Me.PanelError.Visible = False
        Me.Literal1.Text = Message
        Me.Panelmodal.Visible = True
    End Sub
    Public Sub Showsuccess(ByVal Message As String)
        Me.PanelHelp.Visible = False
        Me.PanelError.Visible = False
        Me.panelsuccess.Visible = True

        Me.Literal3.Text = Message
        Me.PanelError.CssClass = "alert alert-danger"
    End Sub
    Public Sub Hide()
        Me.PanelHelp.Visible = False
        Me.PanelError.Visible = False
        Me.panelsuccess.Visible = False
        Me.Panelmodal.Visible = False

    End Sub

    Protected Sub btnclosehelp_ServerClick(sender As Object, e As EventArgs)
        PanelHelp.Visible = False

    End Sub

    Protected Sub btnclosesuccess_ServerClick(sender As Object, e As EventArgs)
        panelsuccess.Visible = False

    End Sub

    Protected Sub btncloseerror_ServerClick(sender As Object, e As EventArgs)
        PanelError.Visible = False

    End Sub
    
    Protected Sub Unnamed_ServerClick(sender As Object, e As EventArgs)
        Panelmodal.Visible = False

    End Sub
End Class
