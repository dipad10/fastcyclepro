﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="blogsidebar.ascx.vb" Inherits="Controls_blogsidebar" %>
<div class="col-md-3">
							<aside class="sidebar">
							
								<form>
									<div class="input-group input-group-lg">
										<input class="form-control" placeholder="Search..." name="s" id="s" type="text">
										<span class="input-group-btn">
											<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</form>
							
								<hr>
							
								<h4 class="heading-primary">ABOUT Fastcyclepro</h4>
                                <hr class="lead" />
								<p>Fastcyclepro was founded by a team of enthusiastic humanitarian specialists who wanted to overcome the routine and create a platform that would act in the market not only for business success but for the sake of humanitarian and financial empowerment services.</p>
							
								<hr>
							
								<h4 class="heading-primary">QUICK LINKS</h4>
<ul class="nav nav-list mb-xlg">
    <li><a href="/default.aspx">HOME</a></li>
									<li><a href="/faq.aspx">FAQ</a></li>
									
									<li><a href="/blog/blogposts.aspx">BLOG</a></li>
									
									<li><a href="/contact.aspx">CONTACT</a></li>
								</ul>							
							</aside>
						</div>