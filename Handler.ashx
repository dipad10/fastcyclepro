﻿<%@ WebHandler Language="VB" Class="Handler" %>

Imports System
Imports System.Web

Public Class Handler : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
         context.Response.ContentType = "application/x-www-form-urlencoded"
        Dim cp_merchant_id As String = "f39ec1421ad91f82aade6898a3089c59"
        Dim cp_ipn_secret As String = "Mamush1dipad1234?"
        Dim cp_debug_email As String = "support@fastcyclepro.com"
        Dim order_total As String = "0.0102"
        Dim ipnmode = context.Request("ipn_mode")
        If ipnmode = "" Or ipnmode <> "hmac" Then
            context.Response.Write("Invalid HMAC")
            Exit Sub
            
        End If
        Dim merchant = context.Request("merchant")
        If merchant <> cp_merchant_id Then
            context.Response.Write("Invalid Merchantid")
            Exit Sub
        End If
        If merchant = "" Then
            context.Response.Write("No merchantid")
            Exit Sub
        End If
        Dim itemname = context.Request("item_name")
        Dim txnid = context.Request("txn_id")
        Dim item_number = context.Request("item_number")
        Dim amount1 = context.Request("amount1")
        Dim amount2 = context.Request("amount2")
        Dim currency1 = context.Request("currency1")
        Dim currency2 = context.Request("currency2")
        Dim status = context.Request("status")
        Dim status_text = context.Request("status_text")
        Dim email = context.Request("email")
        Dim custom = context.Request("custom")
        Dim commission As Double = 0.00015
        If status >= 100 Then
            'means payment is complete save data and send mail saying transaction successful
            Dim A As New cls_wallet
            Dim rec As New Fastcyclepro.Wallet
            rec.TransactionID = txnid
            rec.AccountType = "COINPAYMENTS"
            rec.Deposit = amount1
            rec.Withdrawal = 0
            rec.Total = amount1
            rec.Type = "DEPOSIT"
            rec.Username = custom
            rec.Item_name = itemname
            rec.Status = status
            rec.Statustext = "COMPLETED"
            rec.amount1 = amount1
            rec.amount2 = amount1
            rec.amount = amount1
            rec.TransGUID = item_number
            rec.Createdon = Date.Now
            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then
                'get username and email address
                Dim usedetail As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(rec.Username)

                Dim body As String = mod_main.PopulateBodydepositcomplete(usedetail.Name, Date.Now.ToLongDateString, amount1 & " BTC", "Coinpayments")
                Call mod_main.SendHtmlFormattedEmail(usedetail.Email, "", "Your Deposit has been Completed!", body)
                context.Response.Write("Successful!")
                'save commission into adminprofit table
                Dim P As New cls_adminprofit
                Dim Prec As New Fastcyclepro.AdminProfit
                Prec.commission = commission
                Prec.createdon = Date.Now
                Prec.Description = "Commission " & commission & "BTC" & " Received from Deposit made by " & rec.Username & ""
                Prec.Status = 1
                'status 1 means hasnt been withdrawed, 0 means it has been withdrawed
                Dim res2 As ResponseInfo = P.Insert(Prec)
                If res2.ErrorCode = 0 Then
                    'send mail to admin saying commission received
                    Dim mailbody As String = mod_main.PopulateBodymastermail("Administrator", "Commission Payment Received!", Prec.Description)
                    Call mod_main.SendHtmlFormattedEmail(ConfigurationManager.AppSettings("AdminNotifications"), "", "Commission Payment Received!", mailbody)
                End If
            Else
                context.Response.Write(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        ElseIf status < 0 Then
            'payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent 
            context.Response.Write("Payment Error")
            
         
        Else
            'payment is pending, you can optionally add a note to the order page 
            'Dim usedetail As Fastcyclepro.Registration = (New Cls_Registration).SelectThisUsername(custom)

            'Dim body As String = mod_main.PopulateBodydepositpending(usedetail.Name, Date.Now.ToLongDateString, amount1 & " BTC", "Coinpayments")
            'Call mod_main.SendHtmlFormattedEmail(usedetail.Email, "", "Your Deposit is awaiting confirmations!", body)
            context.Response.Write("Payment Pending")
        End If

        
        
       
       
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class